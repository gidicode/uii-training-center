-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 13, 2019 at 05:17 PM
-- Server version: 10.1.39-MariaDB
-- PHP Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trainingcenter`
--

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `level` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `logo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `name`, `description`, `email`, `phone`, `google_id`, `level`, `deskripsi`, `status`, `logo`, `created_at`) VALUES
(8, 'Gidi Training', NULL, '14523300@students.uii.ac.id', '0274123109', '101719349283792166674', 'Prodi', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 'https://lh5.googleusercontent.com/-3IC_Q27PfxU/AAAAAAAAAAI/AAAAAAAAAAs/nQojhqeRO1k/s96-c/photo.jpg', '2019-02-25 05:20:13'),
(9, 'Training Center UII', NULL, 'work.wildan@gmail.com', '080981252358', '109112128695924974258', 'admin', 'Tes user', 1, 'https://lh6.googleusercontent.com/-jKn11o2hYO4/AAAAAAAAAAI/AAAAAAAAAAs/QWNyFGenqrE/s96-c/photo.jpg', '2019-03-25 23:45:38'),
(10, 'Gidicode Project', NULL, 'hi@gidicode.com', '080981231231', '118296070394187610489', 'Universitas', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 'https://lh6.googleusercontent.com/-fpEZSL9f-EM/AAAAAAAAAAI/AAAAAAAAACM/cz8pMEeAXK8/s96-c/photo.jpg', '2019-04-13 14:55:28'),
(35, 'Hari Setiaji', NULL, '115230402@uii.ac.id', '', NULL, 'admin', '', 1, NULL, '2019-05-05 05:24:07');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `training_id` int(11) UNSIGNED NOT NULL,
  `joined_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `user_id`, `training_id`, `joined_at`, `is_paid`, `invoice_id`) VALUES
(1, 11, 80, '2019-05-15 00:06:12', 0, 19056550);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `payment_date` varchar(55) NOT NULL,
  `to_bank_name` varchar(55) NOT NULL,
  `from_name` varchar(55) NOT NULL,
  `cash_amount` int(12) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `skema_tuk`
--

CREATE TABLE `skema_tuk` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `skema` varchar(55) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(15, 'website'),
(16, 'laravel');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(11) UNSIGNED NOT NULL,
  `organization_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` text,
  `price` int(11) UNSIGNED DEFAULT NULL,
  `fasilitas` text,
  `materi` text NOT NULL,
  `infoPembayaran` text NOT NULL,
  `quota` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clicked` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `organization_id`, `name`, `description`, `location`, `price`, `fasilitas`, `materi`, `infoPembayaran`, `quota`, `start_time`, `finish_time`, `closed_at`, `status`, `created_at`, `updated_at`, `clicked`) VALUES
(80, 10, 'Pelatihan Laravel untuk Pemula', '<p>Laravel merupakan framework PHP yang dirancang untuk membangun situs web dengan teknik pengembangan yang mudah dipahami karena mengikuti pola MVC (Model View Controller).</p><p>Pada training ini diharapkan mampu membantu meningkatkan kemampuan dan kinerja peserta dalam memperbaharui dan membangun website yang lebih mudah dan cepat, serta memiliki kinerja akses yang cepat.</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 'Fakultas Teknologi Industri Universitas Islam Indonesia', 1250000, '<p>Kelas ber-AC</p><p>Modul pelatihan</p><p>source code</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>1. Introducing to Laravel</p><p>1.1. Installing Laravel<br>1.2. Creating the TODOParrot Application<br>1.3. Configuring Laravel Application<br>1.4. Useful Development and Debugging Tools<br>1.5. Testing Laravel Application with PHPUnit</p><p>2. Managing Project Controllers, Layout, Views, and Other Assets</p><p>2.1. Creating First View<br>2.2. Creating First Controller<br>2.3. Managing Application Routes<br>2.4. Introducing the Blade Template Engine<br>2.5. Integrating Images, CSS and JavaScript<br>2.6. Introducing Elixir<br>2.7. Testing Views</p><p>3. Introducing Laravel Models</p><p>3.1. Configuring Project Database<br>3.2. Introducing the Eloquent ORM<br>3.3. Creating First Model<br>3.4. Introducing Migrations<br>3.5. Defining Accessors, Mutators, and Methods<br>3.6. Validating Models<br>3.7. Creating a RESTful Controller<br>3.8. Seeding the Database<br>3.9. Finding Data<br>3.10. Inserting New Records<br>3.11. Updating Existing Records<br>3.12. Deleting Records<br>3.13. Introducing Query Builder<br>3.14. Creating Sluggable URLs<br>3.15. Testing Models</p><p>4. Model Relations, Scopes, and Other Advanced Features</p><p>4.1. Introducing Relations<br>4.2. Introducing One-to-One Relations<br>4.3. Introducing the Belongs To Relation<br>4.4. Introducing One-to-Many Relations<br>4.5. Introducing Many-to-Many Relations<br>4.6. Introducing Has Many Through Relations<br>4.7. Introducing Polymorphic Relations<br>4.8. Eager Loadin<br>4.9. Introducing Scopes</p><p>5. Integrating Web Forms</p><p>5.1. Web Form Fundamentals<br>5.2. Creating a User Feedback For<br>5.3. Creating New TODO Lists<br>5.4. Updating a TODO List<br>5.5. Deleting TODO Lists<br>5.6. Associating Tasks with Categories<br>5.7. Uploading Files</p><p>6. Introducing Middleware</p><p>6.1. Introducing Laravel’s Default Middleware<br>6.2. Creating Middleware Solution<br>6.3. Using Middleware Parameters</p><p>7. Authenticating and Managing Users</p><p>7.1. Configuration Laravel Authentication<br>7.2. Registering Users<br>7.3. Retrieving the Authenticated User<br>7.4. Restricting Access to Authenticated Users<br>7.5. Restricting Forms to Authenticated Users<br>7.6. Creating Route Aliases<br>7.7. Adding Custom Fields to the Registration Form<br>7.8. Restricting an Entire Application to Authenticated Users</p><p>8. Deploying, Optimizing and Maintaining Application</p><p>8.1. Introducing the Laravel 5 Command Scheduler<br>8.2. Optimizing Application<br>8.3. Deploying Application<br>8.4. Placing Application in Maintenance Mode</p><p>9. Creating a Restricted Administration Console</p><p>9.1. Identifying Administrators<br>9.2. Creating the Administration Controllers<br>9.3. Restricting Access to the Administration Console</p><p>10. Introducing Lumen</p><p>10.1. Creating First Lumen Application<br>10.2. Creating a Status API</p><p>11. Introducing Events</p><p>11.1. Creating an Event<br>11.2. Broadcasting Events</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>Pembayaran melalui rekning Mandiri Syariah No.rek 09809381297 An. Gidicode Project</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 25, '2019-07-05 00:00:00', '2019-07-25 00:00:00', '2019-06-20 00:00:00', 3, '2019-05-05 05:52:12', '2019-05-05 05:52:12', 25);

-- --------------------------------------------------------

--
-- Table structure for table `training_images`
--

CREATE TABLE `training_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_images`
--

INSERT INTO `training_images` (`id`, `training_id`, `filename`) VALUES
(1, 80, 'http://staging-utc.nkmd-uii.id/public/images/pelatihan/80/laravel.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `training_tags`
--

CREATE TABLE `training_tags` (
  `training_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_tags`
--

INSERT INTO `training_tags` (`training_id`, `tag_id`) VALUES
(80, 15),
(80, 16);

-- --------------------------------------------------------

--
-- Table structure for table `training_update`
--

CREATE TABLE `training_update` (
  `id` int(11) NOT NULL,
  `uniqID` varchar(155) NOT NULL,
  `training_id` int(11) NOT NULL,
  `title` varchar(155) NOT NULL,
  `note` text NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tuk`
--

CREATE TABLE `tuk` (
  `id` int(11) NOT NULL,
  `google_id` varchar(155) NOT NULL,
  `email` varchar(111) NOT NULL,
  `nama_tuk` varchar(55) NOT NULL,
  `nama_laboratorium` varchar(155) NOT NULL,
  `nama_kepala_laboratorium` varchar(111) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `validasi` text NOT NULL,
  `status` varchar(8) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk`
--

INSERT INTO `tuk` (`id`, `google_id`, `email`, `nama_tuk`, `nama_laboratorium`, `nama_kepala_laboratorium`, `alamat`, `deskripsi`, `phone`, `validasi`, `status`, `logo`) VALUES
(2, '103513587021182227139', 'wildan@gidicode.com', 'Wildan  Maulana', 'Tes 1', 'tes 2', 'tes 3', 'tes 4', '341', '', '1', 'https://lh5.googleusercontent.com/-ofLf5YDsA_I/AAAAAAAAAAI/AAAAAAAAAAc/OwdsAugLTRQ/s96-c/photo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_participants`
--

CREATE TABLE `tuk_participants` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `nama` varchar(55) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tuk_peralatan`
--

CREATE TABLE `tuk_peralatan` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `spesifikasi` varchar(155) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `kondisi` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk_peralatan`
--

INSERT INTO `tuk_peralatan` (`id`, `id_tuk`, `spesifikasi`, `jumlah`, `kondisi`) VALUES
(5, 2, 'spesifikasi 2.1', 1, 'oke bos'),
(6, 2, 'spesifikasi 2.2', 1, 'oke bos');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_skema`
--

CREATE TABLE `tuk_skema` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `skema` varchar(55) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk_skema`
--

INSERT INTO `tuk_skema` (`id`, `id_tuk`, `skema`, `keterangan`) VALUES
(1, 2, 'Skema 1', 'lorem ipsum');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_tempat`
--

CREATE TABLE `tuk_tempat` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `spesifikasi` varchar(155) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `kondisi` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk_tempat`
--

INSERT INTO `tuk_tempat` (`id`, `id_tuk`, `spesifikasi`, `jumlah`, `kondisi`) VALUES
(5, 2, 'spesifikasi 1.2', 1, 'good aja'),
(6, 2, 'spek 1.1', 1, 'fa');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` text,
  `institusi` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `google_id`, `name`, `avatar`, `phone`, `address`, `institusi`, `pekerjaan`, `created_at`) VALUES
(10, '14523300@students.uii.ac.id', '101719349283792166674', 'WILDAN MAULANA -', 'https://lh5.googleusercontent.com/-3IC_Q27PfxU/AAAAAAAAAAI/AAAAAAAAJ-s/Dpea4YyNgkk/s96-c/photo.jpg', NULL, NULL, NULL, NULL, '2019-05-14 15:46:23'),
(11, 'nizomsidiq@gmail.com', '106552100903921270486', 'Nizom Sidiq', 'https://lh4.googleusercontent.com/-EHFgR2GXh8Q/AAAAAAAAAAI/AAAAAAAABDs/_2RoPVNK2uI/s96-c/photo.jpg', '0895385351954', 'jalan jalan di jogja', 'Gidicode', 'Kuli', '2019-05-14 17:03:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `organizations_email_uindex` (`email`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_id` (`invoice_id`),
  ADD KEY `training_id` (`training_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skema_tuk`
--
ALTER TABLE `skema_tuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Indexes for table `training_images`
--
ALTER TABLE `training_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_id` (`training_id`);

--
-- Indexes for table `training_tags`
--
ALTER TABLE `training_tags`
  ADD PRIMARY KEY (`training_id`,`tag_id`),
  ADD KEY `training_tags_ibfk_1` (`tag_id`),
  ADD KEY `training_tags_ibfk_2` (`training_id`);

--
-- Indexes for table `training_update`
--
ALTER TABLE `training_update`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk`
--
ALTER TABLE `tuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_participants`
--
ALTER TABLE `tuk_participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_peralatan`
--
ALTER TABLE `tuk_peralatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_skema`
--
ALTER TABLE `tuk_skema`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_tempat`
--
ALTER TABLE `tuk_tempat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_uindex` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `skema_tuk`
--
ALTER TABLE `skema_tuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=82;

--
-- AUTO_INCREMENT for table `training_images`
--
ALTER TABLE `training_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `training_update`
--
ALTER TABLE `training_update`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tuk`
--
ALTER TABLE `tuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tuk_participants`
--
ALTER TABLE `tuk_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tuk_peralatan`
--
ALTER TABLE `tuk_peralatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `tuk_skema`
--
ALTER TABLE `tuk_skema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tuk_tempat`
--
ALTER TABLE `tuk_tempat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `participants_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `training_images`
--
ALTER TABLE `training_images`
  ADD CONSTRAINT `training_images_ibfk_1` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_tags`
--
ALTER TABLE `training_tags`
  ADD CONSTRAINT `training_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `training_tags_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
