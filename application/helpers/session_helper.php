<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('session')) {
    function session($key = null, $default = null) {
        $CI =& get_instance();
        $CI->load->library('session');
        if (is_null($key)) {
            return $CI->session;
        }
        if (is_array($key)) {
            return $CI->session->set_userdata($key);
        }

        return $CI->session->userdata($key) ?? $default;
    }
}

if (! function_exists('auth')) {
    function auth($key = null) {
        $CI =& get_instance();

        if (is_null($key)) {
            return (object) $CI->session->userdata('user');
        }

        $user = (object) $CI->session->userdata('user');
        return $user->{$key} ?? null;
    }
}
