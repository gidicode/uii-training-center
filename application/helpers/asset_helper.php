<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('asset')) {
    function asset($uri = '') {
        return get_instance()->config->item('asset_url').$uri;
    }
}
