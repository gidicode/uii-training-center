<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('config')) {
    function config($path) {
        $names = explode('.', $path);
        foreach ($names as $i => $name) {
            if ($i == 0) {
                $config = get_instance()->config->item($name);
                continue;
            }

            $config = $config[$name];
        }
        return $config;
    }
}
