<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('rupiah_to_int')) {
    /**
     * Convert rupiah string to integer
     */
    function rupiah_to_int($rupiah): int {
        return (int) str_replace( // remove dots
            '.', '', 
            ltrim( // remove Rp
                rtrim($rupiah, ',-'), // remove ,-
                'Rp '
            )
        );
    }
}

if (! function_exists('int_to_rupiah')) {
    /**
     * Convert integer to string rupiah
     */
    function int_to_rupiah($int): string {
        return "Rp " . number_format($int,0,',','.') . ",-";
    }
}
