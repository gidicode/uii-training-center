<?php
defined('BASEPATH') OR exit('No direct script access allowed');

if (! function_exists('dashboard_url')) {
    function dashboard_url($uri = '') {
        return base_url('/admin/'.$uri);
    }
}
