<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Skema_model extends CI_Model
{
    protected $table = 'tuk_skema';

    public function __construct()
    {
        $this->load->database();
    }

    public function get_skema_by_tuk($id)
    {
        return $this->db->get_where('tuk_skema', array('id_tuk' => $id))->result();
    }
    public function get_skema_by_id($id)
    {
        return $this->db->get_where('tuk_skema', array('id' => $id))->result();
    }
    public function get_jumlah_sertifikasi($id)
    {
        $this->db->select(array(
            '*',
            'tuk_skema.id as id',
            'tuk_sertifikasi.id as sertifikasi_id',
            'count(tuk_sertifikasi.id) as jumlah'
        ));

        $this->db->join('tuk_sertifikasi','tuk_skema.id = tuk_sertifikasi.skema_id','left');
        $this->db->where("tuk_skema.id_tuk = $id");
        $this->db->group_by('tuk_skema.skema');
        return $this->db->get('tuk_skema');
    }
}