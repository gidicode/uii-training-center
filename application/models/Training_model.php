<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Training_model extends CI_Model
{
    protected $table = 'trainings';

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('date');
    }

    public function find($conditions = array())
    {   
        $this->db->select(array(
            '*',
            'organizations.name as org_name',
            'trainings.name as name',
            'trainings.status as status',
            'trainings.id as id',
            'trainings.description as description'
        ));

        $this->db->join('organizations','organizations.id = trainings.organization_id');
        $query = $this->db->get_where($this->table, $conditions);

        return $query->row_array();
    }

    public function findOther($conditions)
    {   
        $this->db->select(array(
            '*',
            'organizations.name as org_name',
            'trainings.name as training_name',
            'trainings.status as status',
            'trainings.id as id',
            'trainings.description as description'
        ));
        
        $this->db->join('training_images','trainings.id = training_images.training_id');
        $this->db->join('organizations','organizations.id = trainings.organization_id');
        $query = $this->db->get_where($this->table, $conditions);

        return $query;
    }

    public function get_published()
    {
        $this->db->where(['status' => My\Enums\TrainingStatus::PUBLISHED]);
        $this->db->where(['closed_at >' => now()]);
        $this->db->order_by('closed_at', 'DESC');
        return $this->db->get($this->table)->result();
    }

    public function get_by_organization($org_id)
    {
        $this->db->select(array('*','trainings.id as id_training','organizations.name as org_name','trainings.name as training_name','trainings.status as training_status'));
        $this->db->join('organizations','trainings.organization_id = organizations.id');
        $this->db->order_by('trainings.status');
        $this->db->order_by('trainings.id', 'DESC');
        return $this->db->get_where($this->table, ['organization_id' => $org_id])->result();
    }

    public function get_trainings_all()
    {
        $this->db->select(array('*','trainings.id as id_training','organizations.name as org_name','trainings.name as training_name','trainings.status as training_status'));
        $this->db->join('organizations','trainings.organization_id = organizations.id');
        $this->db->order_by('trainings.status');
        $this->db->order_by('trainings.id', 'DESC');
        return $this->db->get($this->table)->result();
    }

    public function load_images($ids)
    {
        if (is_int($ids)) {
            $ids = array($ids);
        }
        $this->db->where_in('training_id', $ids);

        return $this->db->get('training_images')->result_array();
    }

    public function load_tags($ids)
    {
        if (is_int($ids)) {
            $ids = array($ids);
        }
        $this->db->where_in('training_id', $ids);
        $this->db->from('training_tags');
        $this->db->join('tags', 'tags.id = training_tags.tag_id');
        return $this->db->get()->result_array();
    }

    public function load_organization($ids){
        if (is_int($ids)) {
            $ids = array($ids);
        }

        return $this->db->get_where('organizations',array('id' => $ids))->result_array();
    }

    public function create($data = array(), $returned = false)
    {
        $is_success = $this->db->insert($this->table, $data);

        return $returned ?
            $this->find(['trainings.id' => $this->db->insert_id()])
            : $is_success;
    }

    public function editTraining($id, $data){
        $this->db->where('id',$id);
        return $this->db->update('trainings',$data);
    }

    
    public function attach_images($id, $images = array())
    {   
        $upload_dir = PUBLICPATH.'images/pelatihan/'.$id;
        // Create folder if not exists
        if (! is_dir($upload_dir)) { mkdir($upload_dir, 0755, TRUE); }

        // initiate uploader
        $this->load->library('upload', [
            'upload_path' => $upload_dir.'/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '2048'
        ]);

        $insert_images = [];
        foreach ($images['name'] as $i => $name) {
            // set to $_FILES['upload_image'] each images for preparing to upload
            foreach ($images as $key => $values) {
                $_FILES['upload_image'][$key] = $values[$i];
            }
            // Upload
            if ($this->upload->do_upload('upload_image')) {
                $insert_images[] = [
                    'training_id' => $id,
                    'filename' => asset('images/pelatihan/'.$id.'/'.$this->upload->data('file_name'))
                ];

                $data_img = array(
                    'training_id' => $id,
                    'filename' => asset('images/pelatihan/'.$id.'/'.$this->upload->data('file_name'))
                );

                $this->db->insert('training_images',$data_img);
            }
            
            //$this->db->insert('training_images',$insert_images);
        }
        
        //$this->db->insert('training_images',$insert_images);
        

        //return $this->db->insert_batch('training_images', $insert_images);
    }

    public function deleteImages($id)
    {
        $this->db->where('training_id',$id);
        $this->db->delete('training_images');
        
        foreach($this->db->get_where('training_images',array('training_id' => $id))->result() as $img):
            $target = $img->filename;
            if (file_exists($target)){
                $deleteTarget = unlink($target);
            }
        endforeach;
        
    }

    public function sync_tags($id, $tags)
    {
        $this->load->model('tag_model');
        $insert_tags = [];
        foreach ($tags as $tag) {
            if (is_numeric($tag)) {
                $insert_tags[] = ['training_id' => $id, 'tag_id' => $tag];
            } else {
                $tag_id = $this->tag_model->create(['name' => $tag]);
                $insert_tags[] = ['training_id' => $id, 'tag_id' => $tag_id];
            }
        }

        $this->db->delete('training_tags', ['training_id' => $id]);
        return $this->db->insert_batch('training_tags', $insert_tags);
    }

    public function get_trainings()
    {
        return $this->db->query('select *,t.name as training_name, o.name as org_name, t.id as training_id from trainings t left join training_images i on(t.id = i.training_id) left join organizations o on(t.organization_id = o.id) where t.status = 3 group by t.id');
    }

    public function get_trainings_homepage()
    {
        return $this->db->query('select *,t.name as training_name, o.name as org_name, t.id as training_id from trainings t left join training_images i on(t.id = i.training_id) left join organizations o on(t.organization_id = o.id) where t.status = 3  group by t.id limit 6');
    }

    public function get_training_detail($id)
    {
        $this->db->select(array(
            '*',
            'trainings.name as training_name',
            'trainings.description as training_description',
            'organizations.name as org_name',
            'trainings.id as training_id',
            'trainings.status as status'
        ));
        $this->db->join('training_images','trainings.id = training_images.training_id');
        $this->db->join('organizations','trainings.organization_id = organizations.id');
        $this->db->where("trainings.id = $id AND trainings.status NOT IN(1,2)");
        return $this->db->get('trainings');
    }

    public function get_training_by($condition)
    {
        $this->db->select(array(
            '*',
            'trainings.name as training_name',
            'trainings.description as training_description',
            'organizations.name as org_name',
            'trainings.id as training_id',
            'trainings.status as status'
        ));
        $this->db->join('training_images','trainings.id = training_images.training_id');
        $this->db->join('organizations','trainings.organization_id = organizations.id');
        $this->db->where($condition);
        return $this->db->get('trainings');
    }

    public function get_participant($where)
    {
        $this->db->select(array('*','users.name as username','participants.id as id'));
        $this->db->join('users','participants.user_id = users.id');
        return $this->db->get_where('participants',$where);
    }

    public function get_participant_byOrg($where)
    {
        $this->db->select(array('participants.id as id','organizations.id as organization_id'));
        $this->db->join('trainings','participants.training_id = trainings.id');
        $this->db->join('organizations','organizations.id = trainings.organization_id');
        return $this->db->get_where('participants',$where);
    }

    public function get_joined_at($condition = array())
    {   
        return $this->db->get_where('participants',$condition)->row_array();
    }

    public function get_participantDetail($id)
    {
        $this->db->select(array(
            'users.name as participant',
            'trainings.name as training',
            'users.phone as phone',
            'users.email as email',
            'users.institusi as institusi',
            'users.address as address',
            'users.pekerjaan as pekerjaan'
        ));
        $this->db->join('users','users.id = participants.user_id');
        $this->db->join('trainings','trainings.id = participants.training_id');
        return $this->db->get_where('participants',array('participants.id' => $id));
    }

    public function get_paymentByIdUser($id)
    {
        $this->db->select(array(
            'users.name as participant',
            'trainings.name as training',
            'users.phone as phone',
            'users.email as email',
            'users.institusi as institusi',
            'users.address as address',
            'users.pekerjaan as pekerjaan',
            'payment.invoice_id as invoice_id',
            'payment.payment_date as payment_date',
            'payment.to_bank_name as to_bank_name',
            'payment.from_name as from_name',
            'payment.cash_amount as cash_amount'
        ));
        $this->db->join('payment','payment.invoice_id = participants.invoice_id');
        $this->db->join('users','users.id = participants.user_id');
        $this->db->join('trainings','trainings.id = participants.training_id');
        return $this->db->get_where('participants',array('participants.id' => $id));
    }

    public function get_trainingUpdate($id)
    {
        $this->db->select(array('*','TIMESTAMP(last_update) as last_update'));
        $this->db->order_by('last_update', 'DESC');
        return $this->db->get_where('training_update',array('training_id' => $id));
    }
}