<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tag_model extends CI_Model
{
    protected $table = 'tags';

    public function __construct()
    {
        $this->load->database();
    }

    public function find($conditions = array())
    {
        $query = $this->db->get_where($this->table, $conditions);

        return $query->row_array();
    }

    public function getByTraining($id)
    {
        $this->db->join('tags','tags.id = training_tags.tag_id');
        return $this->db->get_where('training_tags',array('training_id' => $id));
    }

    public function get_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function create($data = array())
    {
        if ($exist = $this->find($data)) {
            return $exist->id;
        }

        $this->db->insert($this->table, $data);
        return $this->db->insert_id();
    }
}