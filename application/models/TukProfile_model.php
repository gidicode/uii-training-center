<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class TukProfile_model extends CI_Model
{
    protected $table = 'tuk';

    public function find($conditions = array())
    {
        $query = $this->db->get_where($this->table, $conditions);

        return $query->row_array();
    }
}