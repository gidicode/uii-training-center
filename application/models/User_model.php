<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User_model extends CI_Model
{
    protected $table = 'users';

    public function __construct()
    {
        $this->load->database();
    }

    public function get_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function get_user($conditions = array())
    {
        $query = $this->db->get_where($this->table, $conditions);

        return $query->row_array();
    }

    public function create_user($data = array(), $returned = false)
    {
        $is_success = $this->db->insert($this->table, [
            'name' => $data['name'],
            'email' => $data['email'],
            'avatar' => $data['avatar'],
            'google_id' => $data['google_id'] ?? null,
        ]);

        if ($returned) {
            return $this->get_user(['id' => $this->db->insert_id()]);
        }

        return $is_success;
    }

    public function update_user($where,$data)
    {
        $this->db->where($where);
        $this->db->update($this->table,$data);
    }

    public function get_pelatihan($id)
    {
        $where = array('id' => $id);
        return $this->db->get_where('trainings',$where)->row_array();;
    }

    public function post_training_registrastion($participant)
    {
        return $this->db->insert('participants',$participant);
    }

    public function check_user_reg($id)
    {
        return $this->db->get_where('participants',array('training_id' => $id, 'user_id' => auth()->id))->num_rows();
    }

    public function get_participant_training()
    {
        $this->db->select(array('*','trainings.id as id_training'));
        $this->db->from('participants');
        $this->db->join('trainings','participants.training_id = trainings.id');
        $this->db->join('training_images','trainings.id = training_images.training_id');
        $this->db->where('user_id',auth()->id);
        $this->db->group_by('participants.training_id');
        return $this->db->get();
    }

    public function get_code_payment($code)
    {
        $this->db->select(array('*',
            'users.name as participant',
            'trainings.name as training'
        ));
        $this->db->join('users','users.id = participants.user_id');
        $this->db->join('trainings','trainings.id = participants.training_id');
        return $this->db->get_where('participants',array('participants.invoice_id' => $code));
    }

    public function get_payment_confirm($code)
    {
        return $this->db->get_where('payment',array('invoice_id' => $code));

    }
}