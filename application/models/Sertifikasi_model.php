<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sertifikasi_model extends CI_Model
{
    protected $table = 'tuk_sertifikasi';

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('date');
    }

    public function create($data = array(), $returned = false)
    {
        $is_success = $this->db->insert($this->table, $data);

        return $returned ?
            $this->find(['tuk_sertifikasi.id' => $this->db->insert_id()])
            : $is_success;   
    }

    public function editSertifikasi($id, $data){
        $this->db->where('id',$id);
        return $this->db->update('tuk_sertifikasi',$data);
    }


    public function attach_images($id, $images = array())
    {   
        $upload_dir = PUBLICPATH.'images/sertifikasi/'.$id;
        // Create folder if not exists
        if (! is_dir($upload_dir)) { mkdir($upload_dir, 0755, TRUE); }

        // initiate uploader
        $this->load->library('upload', [
            'upload_path' => $upload_dir.'/',
            'allowed_types' => 'gif|jpg|png|jpeg',
            'max_size' => '2048'
        ]);

        $insert_images = [];
        foreach ($images['name'] as $i => $name) {
            // set to $_FILES['upload_image'] each images for preparing to upload
            foreach ($images as $key => $values) {
                $_FILES['upload_image'][$key] = $values[$i];
            }
            // Upload
            if ($this->upload->do_upload('upload_image')) {
                $insert_images[] = [
                    'sertifikasi_id' => $id,
                    'filename' => asset('images/sertifikasi/'.$id.'/'.$this->upload->data('file_name'))
                ];

                $data_img = array(
                    'sertifikasi_id' => $id,
                    'filename' => asset('images/sertifikasi/'.$id.'/'.$this->upload->data('file_name'))
                );

                $this->db->insert('tuk_sertifikasi_image',$data_img);
            }
            
        }     
    
    }

    public function deleteImages($id)
    {
        $this->db->where('sertifikasi_id',$id);
        $this->db->delete('tuk_sertifikasi_image');
        
        foreach($this->db->get_where('tuk_sertifikasi_image',array('sertifikasi_id' => $id))->result() as $img):
            $target = $img->filename;
            if (file_exists($target)){
                $deleteTarget = unlink($target);
            }
        endforeach;
        
    }

    public function find($conditions = array())
    {   
        $this->db->select(array(
            '*',
            'tuk.nama_tuk as tuk_name',
            'tuk_sertifikasi.name as name',
            'tuk_sertifikasi.status as status',
            'tuk_sertifikasi.id as id',
            'tuk_sertifikasi.description as description'
        ));

        $this->db->join('tuk','tuk.id = tuk_sertifikasi.tuk_id');
        $query = $this->db->get_where($this->table, $conditions);

        return $query->row_array();
    }
    
    public function get_by_tuk($tuk_id)
    {  
        $this->db->select(array(
            '*',
            'tuk_sertifikasi.id as sertifikasi_id',
            'tuk_sertifikasi.name as sertifikasi_name',
            'tuk_sertifikasi.description as sertifikasi_description',
            'tuk.nama_tuk as tuk_name',
            'tuk_sertifikasi.status as status'
        ));
        $this->db->join('tuk','tuk_sertifikasi.tuk_id = tuk.id');
        $this->db->order_by('tuk_sertifikasi.status');
        $this->db->order_by('tuk_sertifikasi.status', 'DESC');
        return $this->db->get_where($this->table, ['tuk_id' => $tuk_id])->result();

    }

    //Find Certificate By Skema
    public function get_by_skema($skema_id)
    {  
        $this->db->select(array(
            '*',
            'tuk_sertifikasi.id as sertifikasi_id',
            'tuk_sertifikasi.name as sertifikasi_name',
            'tuk_sertifikasi.description as sertifikasi_description',
            'tuk.nama_tuk as tuk_name',
            'tuk_skema.skema as skema_name',
            'tuk_sertifikasi.status as status'
        ));
        $this->db->join('tuk','tuk_sertifikasi.tuk_id = tuk.id');
        $this->db->join('tuk_skema','tuk_sertifikasi.skema_id = tuk_skema.id');
        $this->db->order_by('tuk_sertifikasi.status');
        $this->db->order_by('tuk_sertifikasi.status', 'DESC');
        return $this->db->get_where($this->table, ['skema_id' => $skema_id])->result();

    }

    public function get_participant($where)
    {
        $this->db->select(array('*','users.name as username','tuk_sertifikasi_participants.id as id'));
        $this->db->join('users','tuk_sertifikasi_participants.user_id = users.id');
        return $this->db->get_where('tuk_sertifikasi_participants',$where);
    }

    public function get_participant_nilai($id)
    {
        $this->db->select(array('*','users.name as username','tuk_sertifikasi_participants.id as id','tuk_sertifikasi_nilai.id_user as nilai_user_id','tuk_sertifikasi_nilai.id_sertifikasi as nilai_sertifikasi_id'));
        $this->db->join('users','tuk_sertifikasi_participants.user_id = users.id','left');
        $this->db->join('tuk_sertifikasi_nilai','tuk_sertifikasi_participants.user_id = tuk_sertifikasi_nilai.id_user','left');
        $this->db->where("tuk_sertifikasi_participants.is_paid", 1);
        //$this->db->where('tuk_sertifikasi_participants.user_id = tuk_sertifikasi_nilai.id_user AND tuk_sertifikasi_participants.sertifikasi_id = tuk_sertifikasi_nilai.id_sertifikasi');
        return $this->db->get('tuk_sertifikasi_participants');
    }

    //Load image for view detail
    public function load_images($ids)
    {
        if (is_int($ids)) {
            $ids = array($ids);
        }
        $this->db->where_in('sertifikasi_id', $ids);

        return $this->db->get('tuk_sertifikasi_image')->result_array();
    }

    //Load image for view detail
    public function load_skema($ids)
    {
        if (is_int($ids)) {
            $ids = array($ids);
        }

        $this->db->where_in('id', $ids);
        //$this->db->from('tuk_skema');
        return $this->db->get('tuk_skema')->result_array();
    }
    
    public function get_sertifikasi_by_skema($id)
    {
        return $this->db->get_where($this->table, ['skema_id' => $id])->result();

    }

    public function get_certifications()
    {
        return $this->db->query('select *,t.name as sertifikasi_name, tuk.nama_tuk as tuk_name, t.id as sertifikasi_id from tuk_sertifikasi t left join tuk_sertifikasi_image i on(t.id = i.sertifikasi_id) left join tuk on(t.tuk_id = tuk.id) where t.status = 3 group by t.id');
    }

    public function get_certifications_homepage()
    {
        return $this->db->query('select *,t.name as sertifikasi_name, tuk.nama_tuk as tuk_name, t.id as sertifikasi_id from tuk_sertifikasi t left join tuk_sertifikasi_image i on(t.id = i.sertifikasi_id) left join tuk on(t.tuk_id = tuk.id) where t.status = 3 group by t.id limit 6');
    }

    public function get_sertifikasi_detail($id)
    {
        $this->db->select(array(
            '*',
            'tuk_sertifikasi.name as sertifikasi_name',
            'tuk_sertifikasi.description as sertifikasi_description',
            'tuk.nama_tuk as tuk_name',
            'tuk_sertifikasi.id as sertifikasi_id',
            'tuk_sertifikasi.status as status'
        ));
        $this->db->join('tuk_sertifikasi_image','tuk_sertifikasi.id = tuk_sertifikasi_image.sertifikasi_id');
        $this->db->join('tuk','tuk_sertifikasi.tuk_id = tuk.id');
        $this->db->where("tuk_sertifikasi.id = $id AND tuk_sertifikasi.status NOT IN(1,2)");
        return $this->db->get('tuk_sertifikasi');
    }

    public function get_sertifikasi($id)
    {
        $this->db->select(array( 
            '*', 
            'tuk_sertifikasi.name as sertifikasi_name',
            'tuk.nama_tuk as tuk_name',
            'tuk_sertifikasi.start_time as time',
            'tuk_sertifikasi.price as price',
            'tuk_sertifikasi.infoPembayaran as infoPembayaran'
        ));
        $this->db->join('tuk','tuk_sertifikasi.tuk_id = tuk.id');
        return $this->db->get_where('tuk_sertifikasi',array('tuk_sertifikasi.id' => $id))->row_array();
    }
    
    public function get_joined_at($condition = array())
    {   
        return $this->db->get_where('tuk_sertifikasi_participants',$condition)->row_array();
    }

    


    public function get_paymentByIdUser($id)
    {
        $this->db->select(array(
            'users.name as participant',
            'tuk_sertifikasi.name as sertifikasi',
            'users.phone as phone',
            'users.email as email',
            'tuk_sertifikasi_participants.id as id',
            'users.institusi as institusi',
            'users.address as address',
            'users.pekerjaan as pekerjaan',
            'payment.id as payment_id',
            'payment.invoice_id as invoice_id',
            'payment.payment_date as payment_date',
            'payment.to_bank_name as to_bank_name',
            'payment.from_name as from_name',
            'payment.cash_amount as cash_amount'
        ));
        $this->db->join('payment','payment.invoice_id = tuk_sertifikasi_participants.invoice_id');
        $this->db->join('users','users.id = tuk_sertifikasi_participants.user_id');
        $this->db->join('tuk_sertifikasi','tuk_sertifikasi.id = tuk_sertifikasi_participants.sertifikasi_id');
        return $this->db->get_where('tuk_sertifikasi_participants',array('tuk_sertifikasi_participants.id' => $id));
    }

    public function get_participantDetail($id)
    {
        $this->db->select(array(
            'users.name as participant',
            'tuk_sertifikasi.name as sertifikasi',
            'users.phone as phone',
            'users.email as email',
            'users.institusi as institusi',
            'users.address as address',
            'users.pekerjaan as pekerjaan'
        ));
        $this->db->join('users','users.id = tuk_sertifikasi_participants.user_id');
        $this->db->join('tuk_sertifikasi','tuk_sertifikasi.id = tuk_sertifikasi_participants.sertifikasi_id');
        return $this->db->get_where('tuk_sertifikasi_participants',array('tuk_sertifikasi_participants.id' => $id));
    }

}