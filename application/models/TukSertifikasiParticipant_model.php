<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class tukSertifikasiParticipant_model extends CI_Model
{
    protected $table = 'tuk_sertifikasi_participants';

    public function __construct()
    {
        $this->load->database();
        $this->load->helper('date');
    }

    function input_data($data,$table){
		  $this->db->insert($table,$data);
    }

    public function check_user_reg($id)
    {
        return $this->db->get_where('tuk_sertifikasi_participants',array('sertifikasi_id' => $id, 'user_id' => auth()->id))->num_rows();
    }

    public function get_participant_sertifikasi()
    {
        $this->db->select(array('*','tuk_sertifikasi.id as id_sertifikasi'));
        $this->db->from('tuk_sertifikasi_participants');
        $this->db->join('tuk_sertifikasi','tuk_sertifikasi_participants.sertifikasi_id = tuk_sertifikasi.id');
        $this->db->join('tuk_sertifikasi_image','tuk_sertifikasi.id = tuk_sertifikasi_image.sertifikasi_id');
        $this->db->where('tuk_sertifikasi_participants.user_id',auth()->id);
        $this->db->group_by('tuk_sertifikasi_participants.sertifikasi_id');
        return $this->db->get();
    }
    
    public function get_code_payment_sertifikasi($code)
    {
        $this->db->select(array('*',
            'users.name as participant',
            'tuk_sertifikasi.name as training'
        ));
        $this->db->join('users','users.id = tuk_sertifikasi_participants.user_id');
        $this->db->join('tuk_sertifikasi','tuk_sertifikasi.id = tuk_sertifikasi_participants.sertifikasi_id');
        return $this->db->get_where('tuk_sertifikasi_participants',array('tuk_sertifikasi_participants.invoice_id' => $code));
    }

    
}