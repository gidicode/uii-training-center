<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Payment_model extends CI_Model
{
    protected $table = 'payment';

    public function __construct()
    {
        $this->load->database();
    }

    public function get_all_confirm($condition)
    {
        $this->db->select(
            array(
                'users.name as participant',
                'trainings.name as training',
                'organizations.name as organization',
                'payment.cash_amount as cash_amount',
                'participants.is_paid as is_paid',
                'payment.invoice_id as invoice_id',
                'participants.id as id_participant'
            )
        );
        $this->db->join('participants','payment.invoice_id = participants.invoice_id');
        $this->db->join('trainings','participants.training_id = trainings.id');
        $this->db->join('users','participants.user_id = users.id');
        $this->db->join('organizations','trainings.organization_id = organizations.id');
        return $this->db->get_where('payment',$condition);
    }

    public function get_confirm_byOrg($condition)
    {
        $this->db->select(
            array(
                'users.name as participant',
                'trainings.name as training',
                'organizations.name as organization',
                'payment.cash_amount as cash_amount',
                'participants.is_paid as is_paid',
                'payment.invoice_id as invoice_id',
                'participants.id as id_participant'
            )
        );
        $this->db->join('participants','payment.invoice_id = participants.invoice_id');
        $this->db->join('trainings','participants.training_id = trainings.id');
        $this->db->join('users','participants.user_id = users.id');
        $this->db->join('organizations','trainings.organization_id = organizations.id');
        return $this->db->get_where('payment',$condition);
    }

    public function get_confirm_byID($id)
    {
        $this->db->select(
            array(
                'users.name as participant',
                'users.email as email',
                'trainings.name as training',
                'organizations.name as organization',
                'payment.cash_amount as cash_amount',
                'participants.is_paid as is_paid',
                'payment.invoice_id as invoice_id',
                'to_bank_name',
                'from_name',
                'participants.id as id_participant'
            )
        );
        $this->db->join('participants','payment.invoice_id = participants.invoice_id');
        $this->db->join('trainings','participants.training_id = trainings.id');
        $this->db->join('users','participants.user_id = users.id');
        $this->db->join('organizations','trainings.organization_id = organizations.id');
        return $this->db->get_where('payment',array('payment.invoice_id' => $id));
    }

    public function get_training_byPayment($id)
    {
        $this->db->select(
            array(
                'users.name as participant',
                'users.email as email',
                'trainings.name as training',
                'participants.invoice_id as invoice_id',
                'trainings.price as price'
            )
        );   
        $this->db->join('trainings','participants.training_id = trainings.id');
        $this->db->join('users','participants.user_id = users.id');
        return $this->db->get_where('participants',array('participants.id' => $id));
    }
}