<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Organization_model extends CI_Model
{
    protected $table = 'organizations';

    public function __construct()
    {
        $this->load->database();
    }

    public function get_all()
    {
        return $this->db->get($this->table)->result();
    }

    public function get_where($condition)
    {   
        return $this->db->get_where($this->table,$condition)->result();
    }

    public function find($conditions = array())
    {
        $query = $this->db->get_where($this->table, $conditions);

        return $query->row_array();
    }

    public function findTuk($conditions = array())
    {
        $query = $this->db->get_where('tuk', $conditions);

        return $query->row_array();
    }

    public function findOther($conditions = array())
    {
        $query = $this->db->get_where($this->table, $conditions);

        return $query;
    }

    public function find_email($table, $conditions = array())
    {
        return $this->db->get_where($table, $conditions)->num_rows();
    }

    public function create($data = array(), $returned = false)
    {
        $is_success = $this->db->insert($this->table, [
            'name' => $data['name'],
            'email' => $data['email'],
            'logo' => $data['logo'],
            'level' => $data['level'],
            'status' => $data['status'],
            'deskripsi' => $data['deskripsi'],
            'google_id' => $data['google_id'] ?? null,
            'phone' => $data['phone']
        ]);

        if ($returned) {
            return $this->find(['id' => $this->db->insert_id()]);
        }

        return $is_success;
    }

    public function create_tuk($data,$email)
    {
        if($this->db->get_where('tuk', array('email' => $email))->num_rows() == 0){
            $this->db->insert('tuk',$data);
        } else {
            $this->db->update('tuk',$data,array('email' => $email));
        }
    }


    public function order_by($key, $order = 'ASC')
    {
        $this->db->order_by($key, $order);
        return $this;
    }

    public function ubah_status($id, $status)
    {
        $this->db->where(['id' => $id]);
        return $this->db->update($this->table, ['status' => $status]);
    }

    public function delete($condition)
    {
        return $this->db->delete($this->table, $condition);
    }
}
