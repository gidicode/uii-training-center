<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_add_tuk_participantForms extends CI_Migration 
{
    protected function up()
    {
        $this->dbforge->add_field(array(
            'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE,
                'auto_increment' => TRUE
            ),
            'user_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE
            ),
            'sertification_id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => TRUE
            ),
            'form' => array(
                'type' => 'JSON',
                'null' => TRUE
            ),
            'created_at' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE
            ),
            'updated_at' => array(
                'type' => 'TIMESTAMP',
                'null' => TRUE
            )

        ));

        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('tuk_participantForms' , TRUE);
    }

    protected function down()
    {
        $this->dbforge->drop_table('tuk_participantForms');
    }

}