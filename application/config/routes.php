<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'homepage';
$route['404_override'] = 'homepage/notfound';
$route['translate_uri_dashes'] = FALSE;

$route['thank-you'] = 'homepage/thankyouTuk';

// dashboard
$route['admin'] = 'dashboard/dashboard';
$route['admin/login'] = 'homepage/loginAdmin';
$route['lembaga'] = 'homepage/loginLembaga';
$route['tuk/login'] = 'homepage/loginTuk';

// dashboard organization
$route['admin/organization'] = 'dashboard/organization';
$route['admin/organization/(:num)/activate'] = 'dashboard/organization/activate/$1';
$route['admin/organization/(:num)/nonactivate'] = 'dashboard/organization/nonactivate/$1';
$route['admin/organization/(:num)/decline'] = 'dashboard/organization/decline/$1';
$route['admin/organization/(:num)'] = 'dashboard/organization/showDetail/$1';

// dashboard TUK
$route['tuk'] = 'dashboard/tuk';
$route['tuk/pending'] = 'dashboard/tuk/statusPending';
$route['tuk/berkas-tambahan'] = 'dashboard/tuk/berkasTambahan';
$route['tuk/skema/(:num)/delete'] = 'dashboard/tuk/deleteSkema';
$route['tuk/sertifikasi'] = 'dashboard/tuk/sertifikasi';
$route['tuk/sertifikasi/create'] = 'dashboard/tuk/createSertifikasi';
$route['tuk/sertifikasi/(:num)/edit'] = 'dashboard/tuk/editSertifikasi/$1';
$route['tuk/sertifikasi/(:num)'] = 'dashboard/tuk/showTuk/$1';
$route['tuk/sertifikasi/(:num)/manage'] = 'dashboard/tuk/manageTuk/$1';
$route['tuk/sertifikasi/(:num)/delete'] = 'dashboard/tuk/delete/$1';
$route['tuk/sertifikasi/(:num)/cancel'] = 'dashboard/tuk/cancel/$1';
$route['tuk/sertifikasi/skema/(:num)'] = 'dashboard/tuk/showSkema/$1';
$route['tuk/sertifikasi/(:num)/publish'] = 'dashboard/tuk/publish/$1';
$route['tuk/sertifikasi/(:num)/unpublish'] = 'dashboard/tuk/unpublish/$1';
$route['tuk/sertifikasi/(:any)/participant-paid'] = 'dashboard/tuk/participantPaid/$1';
$route['tuk/sertifikasi/(:any)/participant-unreject'] = 'dashboard/tuk/participantUnreject/$1';
$route['tuk/sertifikasi/(:any)/delete-participant'] = 'dashboard/tuk/participantReject/$1';
$route['tuk/sertifikasi/(:any)/rejected-participant'] = 'dashboard/tuk/participantRejected/$1';
$route['tuk/sertifikasi/(:any)/all-participant'] = 'dashboard/tuk/participantAll/$1';

$route['tuk/profile'] = 'dashboard/tuk/profile';

// dashboard training
$route['admin/training'] = 'dashboard/training';
$route['admin/training/create'] = 'dashboard/training/create';
$route['admin/training/(:num)/edit'] = 'dashboard/training/edit/$1';
$route['admin/training/(:num)/publish'] = 'dashboard/training/publish/$1';
$route['admin/training/(:num)/unpublish'] = 'dashboard/training/unpublish/$1';
$route['admin/training/(:num)'] = 'dashboard/training/show/$1';
$route['admin/training/(:num)/manage'] = 'dashboard/training/manage/$1';
$route['admin/training/(:num)/cancel'] = 'dashboard/training/cancel/$1';
$route['admin/training/(:num)/delete'] = 'dashboard/training/delete/$1';
$route['admin/training/(:num)/participant'] = 'dashboard/training/showParticipant/$1';
$route['admin/training/(:any)/participant-paid'] = 'dashboard/training/participantPaid/$1';
$route['admin/training/(:any)/participant-unreject'] = 'dashboard/training/participantUnreject/$1';
$route['admin/training/(:any)/delete-participant'] = 'dashboard/training/participantReject/$1';
$route['admin/training/(:any)/rejected-participant'] = 'dashboard/training/participantRejected/$1';
$route['admin/training/(:any)/all-participant'] = 'dashboard/training/participantAll/$1';
$route['admin/training/(:any)/delete-update'] = 'dashboard/training/deleteUpdate/$1' ;

// Manajemen pembayaran (admin)
$route['admin/pembayaran'] = 'dashboard/pembayaran/index';
$route['admin/pembayaran/all'] = 'dashboard/pembayaran/allConfirm';
$route['pembayaran/detailconfirm'] = 'dashboard/pembayaran/detailconfirm';

// Profile
$route['profile'] = 'peserta';
$route['profile/edit'] = 'peserta/edit';
$route['profile/pelatihan/(:num)'] = 'peserta/pelatihan';
$route['profile/sertifikasi/(:num)'] = 'peserta/sertifikasi';
$route['registrasi-user'] = 'homepage/registrasiUser';


// Pelatihan
$route['pelatihan']  = 'homepage/pelatihanAll';
$route['pelatihan/(:any)']  = 'homepage/pelatihan/$1';
$route['lembaga/(:any)']    = 'homepage/lembaga/$1';

// Sertifikasi
$route['sertifikasi'] = 'homepage/sertifikasiAll';
$route['sertifikasi/(:any)'] = 'homepage/sertifikasi/$1';


// Daftar Pelatihan
$route['registrasi-pelatihan'] = "peserta/registrasiPelatihan";
$route['registration-confirmation'] = "peserta/konfirmasiRegistrasi";

// Daftar Sertifikasi
$route['form-registrasi-sertifikasi/(:any)'] = "peserta/formRegistrasiSertifikasi/$1";
$route['registrasi-sertifikasi'] = "peserta/registrasiSertifikasi";
$route['registrasi-confirmation'] = "peserta/konfirmasiRegistrasiSertifikasi";

//Konfirmasi dan INVOICE
$route['konfirmasi'] = "homepage/konfirmasi";

//Manajemen User 
$route['admin/users'] = "dashboard/organization/userManagement";
$route['admin/users/(:num)/delete'] = "dashboard/organization/deleteUser/$1";
$route['admin/users/(:num)/suspend'] = "dashboard/organization/suspendUser/$1";
$route['admin/users/(:num)/active'] = "dashboard/organization/activeUser/$1";