<?php
defined('BASEPATH') or exit('No direct script access allowed');

$config['google'] = [
    'client_id' => env('GOOGLE_CLIENT_ID', ''),
    'secret_key' => env('GOOGLE_CLIENT_SECRET', ''),
    'redirect_url' => env('GOOGLE_REDIRECT_URL', ''),
    'scopes' => [
        'email',
        'profile'
    ],
    'api_key' => env('GOOGLE_API_KEY', '')
];
