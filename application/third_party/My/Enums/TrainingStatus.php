<?php

namespace My\Enums;

use MyCLabs\Enum\Enum;

class TrainingStatus extends Enum
{
    const REJECTED = 0;
    const DRAFT = 1;
    const SUBMIT = 2;
    const PUBLISHED = 3;
    const ONGOING = 4;
    const CLOSED = 5;

    protected static $colors = [
        'REJECTED' => 'text-danger',
        'DRAFT' => '',
        'SUBMIT' => 'text-info',
        'PUBLISHED' => 'text-success',
        'ONGOING' => 'text-success',
        'CLOSED' => 'text-primary'
    ];

    /**
     * Return key for value
     *
     * @param $value
     *
     * @return mixed
     */
    public static function search($value)
    {
        return \array_search((int) $value, static::toArray(), true);
    }

    public static function render($value)
    {
        $name = static::search($value);

        return '<i class="fas fa-sm fa-circle '.static::$colors[$name].'"></i>'.
            ' <small>'. $name.'</small>';
    }
}