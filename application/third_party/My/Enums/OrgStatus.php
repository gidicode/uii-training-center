<?php

namespace My\Enums;

use MyCLabs\Enum\Enum;

class OrgStatus extends Enum
{
    const PENDING = 0;
    const ACTIVE = 1;
    const INACTIVE = 2;
}