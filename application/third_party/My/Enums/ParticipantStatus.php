<?php

namespace My\Enums;

use MyCLabs\Enum\Enum;

class ParticipantStatus extends Enum
{
    const PENDING = 0;
    const ACTIVE = 1;
    const FINISHED = 2;
    const REJECTED = 3;

    protected static $colors = [
        'REJECTED' => 'text-danger',
        'PENDING' => 'text-info',
        'ACTIVE' => 'text-success',
        'FINISHED' => 'text-primary'
    ];

    /**
     * Return key for value
     *
     * @param $value
     *
     * @return mixed
     */
    public static function search($value)
    {
        return \array_search((int) $value, static::toArray(), true);
    }

    public static function render($value)
    {
        $name = static::search($value);

        return '<i class="fas fa-sm fa-circle '.static::$colors[$name].'"></i>'.
            ' <small>'. $name.'</small>';
    }
}