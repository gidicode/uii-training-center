<?php

if (! function_exists('dd')) {
    function dd() {
        array_map(function($x) {
            dump($x);
        }, func_get_args());
        die;
    }
}

if (! function_exists('env')) {
    /**
     * Gets the value of an environment variable. Supports boolean, empty and null.
     *
     * @param  string  $key
     * @param  mixed   $default
     * @return mixed
     */
    function env($key, $default = null)
    {
        $value = getenv($key);
        if ($value === false) {
            return $default;
        }
        switch (strtolower($value)) {
            case 'true':
            case '(true)':
                return true;
            case 'false':
            case '(false)':
                return false;
            case 'empty':
            case '(empty)':
                return '';
            case 'null':
            case '(null)':
                return;
        }
        // if (Str::startsWith($value, '"') && Str::endsWith($value, '"')) {
        //     return substr($value, 1, -1);
        // }
        return $value;
    }
}

if (! function_exists('array_only')) {

    /**
     * Get a subset of the items from the given array.
     * 
     * @param array $array
     * @param array $keys
     * @return array
     */
    function array_only($array, $keys)
    {
        return array_intersect_key($array, array_flip((array) $keys));
    }
}

if (!function_exists('carbon')) {

    function carbon($date, $format = null) {
        if (is_null($date)) {
            return null;
        }
        $date = \Carbon\Carbon::parse($date);
        if (is_null($format)) {
            return $date;
        }

        return $date->format($format);
    }
}