<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Auth extends MY_Controller
{
    protected $after_login = '/admin';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('email');
    }

    public function logout()
    {
        if ($this->request_method('POST') && session('logged_in')) {
            //$this->session->unset_userdata(['logged_in', 'user', 'access_token']);
            $this->session->sess_destroy();
            redirect('/');
        }
        $this->session->sess_destroy();
        redirect('/');
        show_404();
    }

    public function login()
    {
        if ($code = $this->input->get('code')) {
            $this->load->library('google');
            $this->google->setRedirect(base_url('/auth/login'));
            $profile = $this->google->validate($code);

            $this->load->model('organization_model');
            $organization = $this->organization_model->find(['email' => $profile['email']]);
            if ($organization) {
                if ($organization['status'] == My\Enums\OrgStatus::ACTIVE) {
                    
                    // Succesfully Login, so update google data
                    $data_update = array(
                        'google_id' => $profile['id'],
                        'logo'      => $profile['avatar']
                    );

                    $this->db->where('email',$profile['email']);
                    $this->db->update('organizations',$data_update);
                    if($organization['logo'] == null){
                        $logo = $profile['avatar'];
                    }else{
                        $logo = $organization['logo'];
                    }


                    $organization['avatar'] = $logo;
                    session(['logged_in' => true, 'user' => $organization,'dasbor' => 1]);
                    $this->redirect($this->after_login, 'Successfully Login', 'success');
                }
                // Account is not active
                $this->redirect('/', 'Akun anda belum aktif!', 'danger');
            }
            // Account is not found
            $this->redirect('/', 'Email is not associated with site.', 'warning');
        }

        redirect(base_url('/sso/google?redirect='.base_url('/auth/login')));
    }

    public function register()
    {
        if ($this->request_method('POST')) {
            return $this->post_register();
        } 

        if ($code = $this->input->get('code')) {
            $this->load->library('google');
            $this->google->setRedirect(base_url('/auth/register'));
            $profile = $this->google->validate($code);

            $this->load->model('organization_model');
            $exEmail = $this->organization_model->find(['email' => $profile['email']]);
            // Email has been used.
            if($exEmail != null){
                $this->redirect('lembaga', 'Email sudah digunakan, silahkan login untuk masuk', 'warning');
            }

            session()->set_tempdata('register_data', $profile);
            redirect('/auth/register');
        }
        if ($profile = session()->tempdata('register_data')) {
            // TODO: create view registration form
            $this->load->helper(array('form','url'));
            $this->load->view('pages/organisasi/registrasi', compact('profile'));
        } else {
            redirect(base_url('/sso/google?redirect='.base_url('/auth/register')));
        }
    }

    protected function post_register()
    {
        // TODO: Validation
        // 1. email must be unique with organizations table
        // 2. level value only of (prodi,fakultas,universitas)
        // 3. logo Image JPEG/PNG only, max: 2mb

        $name = $this->input->post('name');
        $email = $this->input->post('email');
        $level = $this->input->post('level');
        $google_id = $this->input->post('google_id');
        $deskripsi = $this->input->post('deskripsi');
        $phone = $this->input->post('phone');

        $config['upload_path'] = PUBLICPATH.'logo/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';

        $this->load->library('upload', $config);

        if($this->upload->do_upload('avatar')){
            $logo = asset('logo/').$this->upload->data('file_name');
        }else{
            $logo = $this->input->post('avatar_google');
        }

        $this->load->model('organization_model');
        $this->organization_model->create([
            'name' => $name,
            'email' => $email,
            'level' => $level,
            'logo' => $logo,
            'google_id' => $google_id,
            'deskripsi' => $deskripsi,
            'phone'     => $phone,
            'status' => 0
        ]);

        // [OPTIONAL]
        // send mail to admin, new organization request
        
        $data['user'] = $name;
        $content_email = $this->load->view('email/konfirmasiPendaftaranLembaga',$data,true);

        $this->load->config('email');
        //$this->email->initialize($config);
        
        $this->email->from('no-reply@utc.nkmd-uii.id', 'Admin UII Training Center');
        $this->email->to($email);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        $this->email->subject('Pendaftaran Berhasil');
        $this->email->message($content_email);
        $this->email->send();

        redirect('/auth/thankyou');
    }

    // TUK

    public function loginTuk()
    {
        if ($code = $this->input->get('code')) {
            $this->load->library('google');
            $this->google->setRedirect(base_url('/auth/loginTuk'));
            $profile = $this->google->validate($code);

            $this->load->model('organization_model');
            $tuk = $this->organization_model->findTuk(['email' => $profile['email']]);

            if ($tuk) {
                $ifTempatTuk = $this->db->get_where('tuk_tempat',array('id_tuk' => $tuk['id']))->num_rows();
                $ifPeralatanTuk = $this->db->get_where('tuk_peralatan',array('id_tuk' => $tuk['id']))->num_rows();
                session(['logged_in' => true, 'user' => $tuk,'dasbor' => 2]);
                if ($tuk['status'] == 1) {
                    
                    // Succesfully Login, so update google data
                    $data_update = array(
                        'google_id' => $profile['id'],
                        'logo'      => $profile['avatar']
                    );

                    $this->db->where('email',$profile['email']);
                    $this->db->update('organizations',$data_update);
                    if($tuk['logo'] == null){
                        $logo = $profile['avatar'];
                    }else{
                        $logo = $tuk['logo'];
                    }

                    $tuk['avatar'] = $logo;
                    $this->redirect('tuk', 'Successfully Login', 'success');

                } elseif($ifTempatTuk == 0 OR $ifPeralatanTuk == 0) {
                    redirect(base_url('tuk/berkas-tambahan'));
                } 
                
                // Account is not active
                $this->redirect('/tuk', 'Akun anda belum aktif!', 'danger');
            }
            // Account is not found
            $this->redirect('/tuk/login', 'Akun tidak ditemukan', 'warning');
        }

        redirect(base_url('/sso/google?redirect='.base_url('/auth/loginTuk')));
    }
    
    public function registerTuk()
    {
        if ($this->request_method('POST')) {
            return $this->post_register_tuk();
        } 

        if ($code = $this->input->get('code')) {
            $this->load->library('google');
            $this->google->setRedirect(base_url('/auth/registerTuk'));
            $profile = $this->google->validate($code);

            $this->load->model('organization_model');
            $exEmail = $this->db->get_where('tuk',array('email' => $profile['email']))->num_rows();
            // Email has been used.
            if($exEmail > 0){
                $this->redirect('tuk', 'Email sudah digunakan, silahkan login untuk masuk', 'warning');
            }

            session()->set_tempdata('register_data', $profile);
            redirect('/auth/registerTuk');
        }
        if ($profile = session()->tempdata('register_data')) {
            // TODO: create view registration form
            $this->load->helper(array('form','url'));
            $this->load->view('pages/organisasi/registrasiTUK', compact('profile'));
        } else {
            redirect(base_url('/sso/google?redirect='.base_url('/auth/registerTuk')));
        }
    }

    protected function post_register_tuk()
    {
        // TODO: Validation
        // 1. email must be unique with organizations table
        // 2. level value only of (prodi,fakultas,universitas)
        // 3. logo Image JPEG/PNG only, max: 2mb

        $config['upload_path'] = PUBLICPATH.'logo/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size'] = '2048';

        $this->load->library('upload', $config);

        if($this->upload->do_upload('avatar')){
            $logo = asset('logo/').$this->upload->data('file_name');
        }else{
            $logo = $this->input->post('avatar_google');
        }

        $this->load->model('organization_model');
        $data_tuk = array(
            'nama_tuk' => $this->input->post('nama_tuk'),
            'nama_laboratorium' => $this->input->post('nama_laboratorium'),
            'nama_kepala_laboratorium' => $this->input->post('nama_kepala_laboratorium'),
            'alamat' => $this->input->post('alamat'),
            'email' => $this->input->post('email'),
            'logo' => $logo,
            'google_id' => $this->input->post('google_id'),
            'deskripsi' => $this->input->post('deskripsi'),
            'phone'     => $this->input->post('phone'),
            'status' => 0
        );

        // save to session
        $this->organization_model->create_tuk($data_tuk, $this->input->post('email'));
        session(['logged_in' => true, 'user' => $data_tuk,'dasbor' => 2]);

        // [OPTIONAL]
        // send mail to admin, new organization request
        
        //$data['user'] = $name;
        //$content_email = $this->load->view('email/konfirmasiPendaftaranLembaga',$data,true);

        $this->load->config('email');
        //$this->email->initialize($config);

        //$this->email->from('no-reply@utc.nkmd-uii.id', 'Admin UII Training Center');
        //$this->email->to($email);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        //$this->email->subject('Pendaftaran Berhasil');
        //$this->email->message($content_email);
        //$this->email->send();

        redirect(base_url('tuk/berkas-tambahan'));
    }

    public function thankyou()
    {
        $this->load->view('thankyou');
    }
}