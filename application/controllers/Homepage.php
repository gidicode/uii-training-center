<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Homepage extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
		$this->load->model('training_model');
		$this->load->model('Sertifikasi_model');
		$this->load->model('user_model');
		$this->load->model('TukSertifikasiParticipant_model');
	}
	
	public function index()
	{
		$data['trainings'] = $this->training_model->get_trainings_homepage();
		$data['is_training_active'] = $this->training_model->get_trainings_homepage()->num_rows();
		$data['certifications'] = $this->Sertifikasi_model->get_certifications_homepage();
		$data['is_certification_active'] = $this->Sertifikasi_model->get_certifications_homepage()->num_rows();
		$this->load->view('homepage',$data);
	}

	public function registrasi()
	{
		$this->load->view('pages/organisasi/registrasi');
	}

	public function pelatihan($id)
	{
		if(isset($_POST['daftar'])){
			$daftar = $this->input->post('daftar');
			$idPelatihanDaftar = $this->input->post('idPelatihan');

			switch ($daftar) {

				case 'logged_in':
					$regStatus = $this->user_model->check_user_reg($idPelatihanDaftar);
					if($regStatus > 0){
						$this->session->set_flashdata('danger','Anda sudah mendaftar pelatihan ini ');
						redirect(base_url('pelatihan/'.$idPelatihanDaftar));
					}
	
					$session_daftar = array(
						'idPelatihanDaftar' => $idPelatihanDaftar,
						'redirect'	=> base_url('registrasi-pelatihan')
					);
					$this->session->set_userdata($session_daftar);
					redirect(base_url('registrasi-pelatihan'));
					break;

				case 'google':
					$session_daftar = array(
						'idPelatihanDaftar' => $idPelatihanDaftar,
						'redirect'	=> base_url('registrasi-pelatihan')
					);
					$this->session->set_userdata($session_daftar);
					redirect(base_url('sso/google'));
					break;
				
				default:
					
					break;
			}
		}else{
			if($this->training_model->get_training_detail($id)->num_rows() == 0){
				redirect(base_url());
			}else{
				$data['trainings'] = $this->training_model->get_training_detail($id)->result();
				$data['trainings_all'] = $this->training_model->get_trainings()->result();

				if($this->session->userdata('logged_in') == true && $this->session->userdata('dasbor') == 0){
					$data['logged_in'] = true;
				}else{
					$data['logged_in'] = false;
				}

				//update clicked
				$this->db->where('id',$id);
				$this->db->set('clicked','clicked+1',FALSE);
				$this->db->update('trainings');

				$this->load->view('pelatihan',$data);
			}
		}
	}

	public function registrasiUser()
	{
		$this->load->model('user_model');
		if(isset($_POST['registrasiUser'])){
			$data_update = array(
				'name'		=> $this->input->post('name'),
				'phone'		=> $this->input->post('phone'),
				'address'	=> $this->input->post('address'),
				'institusi'	=> $this->input->post('institusi'),
				'pekerjaan'	=> $this->input->post('pekerjaan')
			);
			$where = array('email' => $this->input->post('email'));
			$this->user_model->update_user($where,$data_update);

			$this->session->unset_userdata(['registrasiUser']);
			if($redirect = $this->session->userdata('redirect')){
				redirect($redirect);
			}else{
				redirect(base_url('profile'));
			}
		}
		
		if($this->session->userdata('registrasiUser') ==  true){
			$email = explode('@',auth()->email);
			if(strpos($email[1],'uii.ac.id') == true){
				$data['institusi'] = "Universitas Islam Indonesia";
			}else{

				$data['institusi'] = null;
			}
			$this->load->view('dashboard/peserta/registrasiUser',$data);
		}else{
			redirect(base_url('profile'));
		}
		
	}

	public function loginAdmin()
    {
        $this->load->view('dashboard/organization/login');
	}
	
	public function loginLembaga()
    {
        $this->load->view('dashboard/organization/loginLembaga');
	}
	
	public function loginTuk()
	{
		$this->load->view('dashboard/organization/loginTuk');
	}
	
	public function konfirmasi()
	{
		if(isset($_POST['code'])){
			$code = $this->input->post('code');
			if($is_code = $this->user_model->get_code_payment($code)->num_rows() > 0){
				$this->session->userdata('code','true');
				$code = $this->user_model->get_code_payment($code)->row_array();

				$this->load->view('form_konfirmasi',compact('code'));
			}
			else if($is_code = $this->TukSertifikasiParticipant_model->get_code_payment_sertifikasi($code)->num_rows() > 0){
				$this->session->userdata('code','true');
				$code = $this->TukSertifikasiParticipant_model->get_code_payment_sertifikasi($code)->row_array();
				//print_r($code);
				$this->load->view('form_konfirmasi',compact('code'));
			}
			else {
				$this->session->set_flashdata('danger','Kode pembayaran anda tidak ditemukan!');
				redirect(base_url('konfirmasi'));
			}
		}

		else {
			$this->load->view('konfirmasi');
		}
	}

	public function post_payment_confirm()
	{
		//cek invoice_id
		$invoice_id = $this->input->post('invoice_id');
		if($this->user_model->get_code_payment($invoice_id)->num_rows() > 0){

			$data_payment = array(
				'invoice_id'	=> $invoice_id,
				'payment_date'	=> $this->input->post('payment_date'),
				'to_bank_name'	=> $this->input->post('to_bank_name'),
				'from_name'		=> $this->input->post('from_name'),
				'cash_amount'	=> rupiah_to_int($this->input->post('cash_amount'))
			);

			if($this->user_model->get_payment_confirm($invoice_id)->num_rows() > 0){
				$this->db->where('invoice_id',$invoice_id);
				$this->db->update('payment',$data_payment);
			} 

			else {
				$this->db->insert('payment',$data_payment);
			}
			

			$this->session->set_flashdata(
				'payment_success',
				'Konfirmasi pembayaran anda berhasil disimpan, silahkan tunggu beberapa saat selagi kami mengecek pembayaran anda.<br>Anda juga dapat mengecek status pembayaran anda pada dasbor profil anda<br><a href="'.base_url().'" class="btn btn-primary form-control mt-3">kembali</a>'
			);
			$this->load->view('form_konfirmasi');

		} 
		else if($this->TukSertifikasiParticipant_model->get_code_payment_sertifikasi($invoice_id)->num_rows() > 0){
			
			$data_payment = array(
				'invoice_id'	=> $invoice_id,
				'payment_date'	=> $this->input->post('payment_date'),
				'to_bank_name'	=> $this->input->post('to_bank_name'),
				'from_name'		=> $this->input->post('from_name'),
				'cash_amount'	=> rupiah_to_int($this->input->post('cash_amount'))
			);

			if($this->user_model->get_payment_confirm($invoice_id)->num_rows() > 0){
				$this->db->where('invoice_id',$invoice_id);
				$this->db->update('payment',$data_payment);
			} 

			else {
				$this->db->insert('payment',$data_payment);
			}
			

			$this->session->set_flashdata(
				'payment_success',
				'Konfirmasi pembayaran anda berhasil disimpan, silahkan tunggu beberapa saat selagi kami mengecek pembayaran anda.<br>Anda juga dapat mengecek status pembayaran anda pada dasbor profil anda<br><a href="'.base_url().'" class="btn btn-primary form-control mt-3">kembali</a>'
			);
			$this->load->view('form_konfirmasi');
			
		}
		else {
			$this->session->set_flashdata('danger','Kode pembayaran anda tidak ditemukan!');
			return redirect(base_url('konfirmasi'));
		}
	}

	public function notfound()
	{
		if($this->session->userdata('dasbor') == 1){
			$data['admin'] = true;
		}else{
			$data['admin'] = false;
		}
		$this->load->view('404',$data);
	}

	public function lembaga($id)
	{
		$this->load->model('organization_model');
		$data = $this->organization_model->find(array('id' => $id,'status' => 1, 'level !=' => 'admin'));
		
		if($data){
			$data['training'] = $this->training_model->findOther(array('trainings.organization_id' => $id, 'trainings.status' => 3));
			$this->load->view('profil_lembaga',compact('data'));
		} else {
			redirect(base_url());
		}

		
	}
	
	public function pelatihanAll()
    {
        $data['trainings'] = $this->training_model->get_trainings_homepage();
		$data['is_training_active'] = $this->training_model->get_trainings_homepage()->num_rows();
        $this->load->view('pelatihanAll',$data);    
	}
	
	public function sertifikasi($id)
	{
		if(isset($_POST['daftar'])){
			$daftar = $this->input->post('daftar');
			$idSertifikasiDaftar = $this->input->post('idSertifikasi');

			switch ($daftar) {

				case 'logged_in':
					$regStatus = $this->TukSertifikasiParticipant_model->check_user_reg($idSertifikasiDaftar);
					if($regStatus > 0){
						$this->session->set_flashdata('danger','Anda sudah mendaftar Sertifikasi ini ');
						redirect(base_url('sertifikasi/'.$idSertifikasiDaftar));
					}
	
					$session_daftar = array(
						'idSertifikasiDaftar' => $idSertifikasiDaftar,
						'redirect'	=> base_url('form-registrasi-sertifikasi/'.$idSertifikasiDaftar)
					);
					$this->session->set_userdata($session_daftar);
					redirect(base_url('form-registrasi-sertifikasi/'.$idSertifikasiDaftar));
					break;

				case 'google':
					$session_daftar = array(
						'idSertifikasiDaftar' => $idSertifikasiDaftar,
						'redirect'	=> base_url('form-registrasi-sertifikasi/'.$idSertifikasiDaftar)
					);
					$this->session->set_userdata($session_daftar);
					redirect(base_url('sso/google'));
					break;
				
				default:
					
					break;
			}
		}else{
			if($this->Sertifikasi_model->get_sertifikasi_detail($id)->num_rows() == 0){
				redirect(base_url());
			}else{
				$data['certifications'] = $this->Sertifikasi_model->get_sertifikasi_detail($id)->result();
				$data['certifications_all'] = $this->Sertifikasi_model->get_certifications()->result();

				if($this->session->userdata('logged_in') == true && $this->session->userdata('dasbor') == 0){
					$data['logged_in'] = true;
				}else{
					$data['logged_in'] = false;
				}

				//update clicked
				$this->db->where('id',$id);
				$this->db->set('clicked','clicked+1',FALSE);
				$this->db->update('tuk_sertifikasi');

				$this->load->view('sertifikasi',$data);
			}
		}
	}

	public function sertifikasiAll()
	{
		$data['certifications'] = $this->Sertifikasi_model->get_certifications_homepage();
		$data['is_certification_active'] = $this->Sertifikasi_model->get_certifications_homepage()->num_rows();
        $this->load->view('sertifikasiAll',$data); 

	}
    
	/*
	public function contohEmail()
	{
		$this->load->view('email/undanganAdmin');
	} */

}
