<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Peserta extends CI_Controller {

  public function __construct()
  {
      parent::__construct();

      if (!$this->session->userdata('logged_in')) {
          redirect('/');
      }

      if ($this->session->userdata('dasbor') == 1) {
        $this->session->set_flashdata('danger','Silahkan login sebagai peserta');
        redirect('/');
      }

      $this->load->model('TukSertifikasiParticipant_model');
      
  }

	public function index()
	{
    $this->load->model('user_model');
    $this->load->model('Sertifikasi_model');

    $data = $this->user_model->get_participant_training()->result();
    $data2 = $this->TukSertifikasiParticipant_model->get_participant_sertifikasi()->result();

    //print_r($data2);
		$this->load->view('dashboard/peserta/index', compact('data','data2'));
  }
    
  public function edit()
  {
    $this->load->model('user_model');
    if(isset($_POST['editUser'])){
      $data_update = array(
				'name'		=> $this->input->post('name'),
				'phone'		=> $this->input->post('phone'),
				'address'	=> $this->input->post('address'),
				'institusi'	=> $this->input->post('institusi'),
				'pekerjaan'	=> $this->input->post('pekerjaan')
			);
			$where = array('email' => $this->input->post('email'));
      $this->user_model->update_user($where,$data_update);
      $this->session->set_flashdata('info','Data berhasil disimpan');
      redirect(base_url('profile/edit'));
      
    }else{
      $this->load->model('user_model');
      $user = $this->user_model->get_user(['email' => auth()->email]);
      $this->load->view('dashboard/peserta/edit_profile', compact('user'));
    }
  }

  public function pelatihan()
  {
    $this->load->model('training_model');
    $id = $this->uri->segment(3);
    $data_pelatihan = $this->training_model->get_training_detail($id);

    if($data_pelatihan->num_rows() == 0){
      show_404();
    }else{
      if($this->training_model->get_participant(array('user_id' => auth()->id, 'training_id' => $id))->num_rows() == 0){
        redirect(base_url());
      }

      $training = $data_pelatihan->row_array();
      $joined_at = $this->training_model->get_joined_at(array('user_id' => auth()->id, 'training_id' => $id));
      $training['joined_at'] = $joined_at['joined_at'];
      $training['status_peserta'] = $joined_at['is_paid'];
      $training['participant'] = $this->training_model->get_participant(array('participants.training_id' => $id,'is_paid' => 1));
      $training['updates'] = $this->training_model->get_trainingUpdate($id);
      $this->load->view('dashboard/peserta/pelatihan',compact('training')); 
    }
  }

  // Registrasi 
  public function registrasiPelatihan()
  {
    $this->load->model('user_model');
    if(!$this->session->userdata('idPelatihanDaftar')){
      redirect(base_url());
    }

    $idPelatihanDaftar = $this->session->userdata('idPelatihanDaftar');

    $regStatus = $this->user_model->check_user_reg($idPelatihanDaftar);
    if($regStatus > 0){
      $this->session->set_flashdata('danger','Anda sudah mendaftar pelatihan ini ');
      redirect(base_url('pelatihan/'.$idPelatihanDaftar));
    }

    if(isset($_POST['konfirmasiRegistrasi'])){
      $this->postRegistrasi();
    } 

    else {
      $id = $this->session->userdata('idPelatihanDaftar');
      
      $this->load->model('user_model');
      $data = $this->user_model->get_pelatihan($id);
      $this->load->view('dashboard/peserta/konfirmasiRegistrasiPelatihan',compact('data'));
    }
    
  }

  protected function postRegistrasi()
  {

    // Generate InvoiceID
    $invoice_id = date('ym').rand(1000,9999);
    $participant = array(
      'user_id' => $this->input->post('user'),
      'training_id' => $this->input->post('training_id'),
      'is_paid' => 0,
      'invoice_id' => $invoice_id
    );

    //send email
    $this->load->library('email');
    $this->load->config('email');
    
    $inv = $this->db->get_where('trainings',array('id' => $this->input->post('training_id')))->row_array();
    if(auth()->name){
			$inv['user'] = auth()->name;
		} else {
			$inv['user'] = '';
		}
    $inv['id_inv'] = $invoice_id;
    $content_email = $this->load->view('email/konfirmasiPendaftaranPelatihan',compact('inv'),true);

    $this->load->config('email');
    //$this->email->initialize($config);
      
    $this->email->from('no-reply@utc.nkmd-uii.id', 'Admin UII Training Center');
    $this->email->to(auth()->email);
    //$this->email->cc('another@another-example.com');
    //$this->email->bcc('them@their-example.com');
    $this->email->subject('Pendaftaran Berhasil');
    $this->email->message($content_email);
    $this->email->send();



    $this->load->model('user_model');
    $this->user_model->post_training_registrastion($participant);
    $this->session->set_flashdata('confirm',$this->input->post('training_id'));
    redirect('registration-confirmation');
  }

  public function konfirmasiRegistrasi()
  {
    if($training_id = $this->session->flashdata('confirm')){
      $this->load->model('user_model');
      $data = $this->user_model->get_pelatihan($training_id);
      $this->session->unset_userdata(['idPelatihanDaftar', 'redirect']);
      $this->load->view('dashboard/peserta/finishRegistrasi',compact('data'));
    }else{
      redirect(base_url('profile'));
    }
  }

  public function formRegistrasiSertifikasi()
  {

    if(isset($_POST['konfirmasiRegistrasi'])){
      $this->storeFormSertifikasi();
    }
    else{
    $this->load->view('dashboard/peserta/formRegistrasiSertifikasi');
    }
  }
  
  protected function storeFormSertifikasi(){

    $myObj = array(
      "dataPribadi" => array(
        "Nama_Lengkap" => $this->input->post('namaLengkap'),
        "Tempat_Lahir" => $this->input->post('tempatLahir'),
        "Tanggal_Lahir" => $this->input->post('tglLahir'),
        "Jenis_Kelamin" => $this->input->post('jenisKelamin'),
        "Kebangsaan" => $this->input->post('kebangsaan'),
        "Alamat" => $this->input->post('alamatRumah'),
        "Kode_Pos" => $this->input->post('kodeposRumah'),
        "Email" => $this->input->post('emailRumah'),
        "Telepon" => $this->input->post('telponRumah'),
        "Pendidikan_Terakhir" => $this->input->post('pendidikanTerakhir')
      ),
      "dataPekerjaan" => array(
        "Nama_Instansi" => $this->input->post('namaInstansi'),
        "Jabatan" => $this->input->post('jabatan'),
        "Alamat" => $this->input->post('alamatInstansi'),
        "Kode_Pos" => $this->input->post('kodeposInstansi'),
        "Email" => $this->input->post('emailInstansi'),
        "Telepon" => $this->input->post('telponInstansi'),
      ),
    );

    $myJSON = json_encode($myObj);
    $invoice_id = date('ym').rand(1000,9999);
   
    $data = array(
			'user_id' => auth()->id,
      'sertification_id' => (int)substr($this->input->post('sertification_id'),7),
      'invoice_id' => $invoice_id,
      'form' => $myJSON
      );

      $this->registrasiSertifikasi($data); 

  }

  public function registrasiSertifikasi($data )
  {
    if(isset($_POST['konfirmasiRegistrasi2'])){
      $this->skonfirmasiRegistrasiSertifikasi($data);
      redirect('/coba');
    }
    else{
    $this->load->model('Sertifikasi_model');
    $id =  $data['sertification_id'];

    $myJSONdata = json_encode($data);
    
    $data2 = $this->Sertifikasi_model->get_sertifikasi($id);
    //print_r($data);
  
    $this->load->view('dashboard/peserta/konfirmasiRegistrasiSertifikasi',  compact('data','data2'));
    }
  }

  public function konfirmasiRegistrasiSertifikasi(){
    
    $insert = array(
			'user_id' => $this->input->post('userID'),
      'sertifikasi_id' => $this->input->post('sertID'),
      'invoice_id' => $this->input->post('invID'),
      'formulir' => html_entity_decode($this->input->post('form'))
      );

      //send email
      $this->load->library('email');
      $this->load->config('email');
      
      $inv = $this->db->get_where('tuk_sertifikasi',array('id' =>  $insert['sertifikasi_id']))->row_array();
      if(auth()->name){
        $inv['user'] = auth()->name;
      } else {
        $inv['user'] = '';
      }
      $inv['id_inv'] = $insert['invoice_id'];
      $content_email = $this->load->view('email/konfirmasiPendaftaranPelatihan',compact('inv'),true);
  
      $this->load->config('email');
      //$this->email->initialize($config);
        
      $this->email->from('no-reply@utc.nkmd-uii.id', 'Admin UII Training Center');
      $this->email->to(auth()->email);
      //$this->email->cc('another@another-example.com');
      //$this->email->bcc('them@their-example.com');
      $this->email->subject('Pendaftaran Berhasil');
      $this->email->message($content_email);
      $this->email->send();

      $sertifikasi_id = $insert['sertifikasi_id'];
      $this->TukSertifikasiParticipant_model->input_data($insert,'tuk_sertifikasi_participants'); 

      if($sertifikasi_id){
        $this->load->model('Sertifikasi_model');
        $data =  $this->Sertifikasi_model->get_sertifikasi($sertifikasi_id);
        //print_r($sertifikasi_id);
        $this->load->view('dashboard/peserta/finishRegistrasi',compact('data'));
      }else{
        redirect(base_url('profile'));
      }
      
  }

  public function sertifikasi()
  {
    $this->load->model('Sertifikasi_model');
    $id = $this->uri->segment(3);
    $data_pelatihan = $this->Sertifikasi_model->get_sertifikasi_detail($id);

    if($data_pelatihan->num_rows() == 0){
      show_404();
    }else{
      if($this->Sertifikasi_model->get_participant(array('user_id' => auth()->id, 'sertifikasi_id' => $id))->num_rows() == 0){
        redirect(base_url());
      }

      $training = $data_pelatihan->row_array();
      $joined_at = $this->Sertifikasi_model->get_joined_at(array('user_id' => auth()->id, 'sertifikasi_id' => $id));
      $training['joined_at'] = $joined_at['joined_at'];
      $training['status_peserta'] = $joined_at['is_paid'];
      $training['participant'] = $this->Sertifikasi_model->get_participant(array('tuk_sertifikasi_participants.sertifikasi_id' => $id,'is_paid' => 1));
      //$training['updates'] = $this->Sertifikasi_model->get_trainingUpdate($id);
      $this->load->view('dashboard/peserta/pelatihan',compact('training')); 
    }
  }



	
}
