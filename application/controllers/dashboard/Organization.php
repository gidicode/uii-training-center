<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use My\Middleware\OrganizationOnly;

class Organization extends MY_Controller implements OrganizationOnly
{
    public function __construct()
    {
        parent::__construct();

        if ($this->auth()->level != 'admin') {
            $this->redirect('/dashboard', 'Unauthorized', 'danger');
        }
        $this->load->model('organization_model');
        $this->load->model('training_model');
        $this->load->library('email');
    }

    public function index()
    {
        $organizations = $this->organization_model
            ->order_by('status')
            ->order_by('created_at', 'DESC')
            ->get_where(array('level !=' => 'admin'));

        $this->load->view('dashboard/organization/index', compact('organizations'));
    }

    public function activate($id)
    {
        if (! $this->request_method('POST')) {
            show_404();
        }

        // [OPTIONAL] Notifikasi akun telah diaktifkan
        $this->organization_model->ubah_status($id, 1);

        $org = $this->organization_model->find(array('id' => $id));

        // send mail to admin, new organization request
        $data['user'] = $org['name'];
        $content_email = $this->load->view('email/konfirmasiLembagaAktif',$data,true);
        $this->email->initialize(array(
            'protocol' => 'smtp',
            'smtp_host' => 'smtp.sendgrid.net',
            'smtp_user' => 'uiitrainingcenter',
            'smtp_pass' => 'GrowthHub87!',
            'smtp_port' => 587,
            'crlf' => "\r\n",
            'newline' => "\r\n",
            'mailtype' => 'html'
        ));
          
        $this->email->from('no-reply@gidicode.com', 'Admin UII Training Center');
        $this->email->to($org['email']);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        $this->email->subject('Pendaftaran Berhasil');
        $this->email->message($content_email);
        $this->email->send();

        $this->redirect('/dashboard/organization', 'Berhasil merubah status', 'success');
    }

    public function nonactivate($id)
    {
        if (! $this->request_method('POST')) {
            show_404();
        }

        $is_exist = $this->training_model->findOther("organizations.id = $id AND trainings.status IN(3,4)");
        if($is_exist->num_rows() > 0){
            $this->redirect('/dashboard/organization', 'Anda tidak dapat merubah status lembaga ini, karena masih memiliki pelatihan yang aktif', 'danger');
        }else{
            $this->organization_model->ubah_status($id, 2);
            $this->redirect('/dashboard/organization', 'Berhasil merubah status', 'warning');
        }
    }

    public function decline($id)
    {
        if (! $this->request_method('POST')) {
            show_404();
        }

        // [OPTIONAL] Notifikasi akun ditolak
        $this->organization_model->delete(['id' => $id]);

        $this->redirect('/dashboard/organization', 'Berhasil merubah status', 'success');
    }

    public function showDetail($id)
    {
        $org = $this->organization_model->find(array('id' => $id));
        $org['jmlPendaftarAktif'] = $this->training_model->get_participant_byOrg(array('organizations.id' => $id, 'participants.is_paid' => 1));
        $org['jmlPendaftarAll'] = $this->training_model->get_participant_byOrg(array('organizations.id' => $id));
        $org['TrainingAktif'] = $this->training_model->get_training_by(array('organization_id' => $id, 'organizations.status' => 1));
        $org['TrainingAll'] = $this->training_model->get_training_by(array('organization_id' => $id));
        if($org){
            $this->load->view('dashboard/organization/monitor',compact('org'));
        }else{
            redirect(dashboard_url());
        }
        
    }

    public function userManagement()
    {
        if ($this->request_method('POST')) {
            $this->storeUser();
        }
        $data['users'] = $this->db->get_where('organizations',array('level' => 'admin','id !=' => auth()->id));
        $this->load->view('dashboard/organization/managementUser',$data);
    }

    protected function storeUser()
    {
        $data = array(
            'email'     => $this->input->post('email'),
            'phone'     => $this->input->post('phone'),
            'name'      => $this->input->post('username'),
            'level'     => 'admin',
            'status'    => 1
        );

        // cek exist email 
        $is_exist = $this->db->get_where('organizations',array('email' => $this->input->post('email')))->num_rows();
        
        if($is_exist > 0){
            $this->session->set_flashdata('danger','Tidak dapat menambahkan email <b>'.$this->input->post('email').'</b>! <br>Silahkan gunakan email yang lain');
            redirect(dashboard_url('users'));
        }

        if(isset($_POST['id'])){
            $this->db->where('id',$this->input->post('id'));
            $this->db->update('organizations',$data);
            $this->session->set_flashdata('warning','Data pengguna berhasil diubah!');
        }else{

            //send email
            $usr['user'] = $this->input->post('username');
            $content_email = $this->load->view('email/undanganAdmin',$usr,true);

            $this->load->config('email');
            //$this->email->initialize($config);
            
            $this->email->from('no-reply@utc.nkmd-uii.id', 'Admin UII Training Center');
            $this->email->to($this->input->post('email'));
            //$this->email->cc('another@another-example.com');
            //$this->email->bcc('them@their-example.com');
            $this->email->subject('Undangan Admin UII Training Center');
            $this->email->message($content_email);
            $this->email->send();

            $this->db->insert('organizations',$data);
            $this->session->set_flashdata('warning','Pengguna berhasil ditambah!');
        }
        
        redirect(dashboard_url('users'));
    }

    public function deleteUser($id)
    {
        $this->db->where('id',$id);
        $this->db->delete('organizations');

        $this->session->set_flashdata('warning','Data berhasil dihapus!');
        redirect(dashboard_url('users'));
    }

    public function suspendUser($id)
    {
        $this->db->where('id',$id);
        $this->db->update('organizations',array('status' => 0));
        $this->session->set_flashdata('warning','Status user berhasil dinonaktifkan');
        redirect(dashboard_url('users'));
    }

    public function activeUser($id)
    {
        $this->db->where('id',$id);
        $this->db->update('organizations',array('status' => 1));
        $this->session->set_flashdata('warning','Status user berhasil diaktifkan!');
        redirect(dashboard_url('users'));
    }


    public function detailUser()
    {
        $id = $this->input->post('id');
        $data = $this->db->get_where('organizations',array('id' => $id))->row_array();
        $this->load->view('dashboard/organization/detailUser',compact('data'));
    }
}
