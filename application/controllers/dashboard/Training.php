<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use My\Enums\TrainingStatus;
use My\Middleware\OrganizationOnly;

class Training extends MY_Controller implements OrganizationOnly
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('training_model');
        $this->load->library('encryption');

        if($this->session->userdata('dasbor') == 0){
            redirect(base_url('admin/login'));
        }
    }

    public function index()
    {
        if(auth()->level == 'admin'){
            $trainings = $this->training_model->get_trainings_all();
        }else{
            $trainings = $this->training_model->get_by_organization($this->auth()->id);
        }
        $this->load->view('dashboard/training/index', compact('trainings'));
    }

    public function show($id)
    {
        if ($training = $this->training_model->find(['trainings.id' => $id])) {
            $training['tags'] = $this->training_model->load_tags($training['id']);
            $training['images'] = $this->training_model->load_images($training['id']);
            $this->load->view('dashboard/training/show', compact('training'));
        } else {
            show_404();
        }
    }

    public function manage($id)
    {
        if ($training = $this->training_model->find(array('trainings.id' => $id, 'trainings.status !=' => 0,'trainings.status !=' => 1,'trainings.status !=' => 2))) {
            $training['training_id'] = $id;
            $training['tags'] = $this->training_model->load_tags($training['id']);
            $training['images'] = $this->training_model->load_images($training['id']);
            $training['peserta'] = $this->training_model->get_participant(array('training_id' => $training['id'],'is_paid !=' => 3 ));
            $training['peserta_paid'] = $this->training_model->get_participant(array('training_id' => $training['id'],'is_paid' => 1 ));
            $training['peserta_reject'] = $this->training_model->get_participant(array('training_id' => $training['id'],'is_paid' => 3 ));
            $training['training_update'] = $this->training_model->get_trainingUpdate($id);
            $this->load->view('dashboard/training/manage', compact('training'));
        } else {
            redirect(dashboard_url('training'));
        }
    }

    public function create()
    {
        if ($this->request_method('POST')) {
            $this->store();
        }
        $this->load->model('tag_model');
        $tags = $this->tag_model->get_all();
        $this->load->view('dashboard/training/create', compact('tags'));
    }

    protected function store()
    {
        $data = array_only(
            $this->input->post(), 
            [
                'name', 'description', 'start_time', 'finish_time',
                'location', 'closed_at', 'quota', 'price','fasilitas','materi','infoPembayaran'
            ]
        );

        // transform and add data
        $data['price'] = rupiah_to_int($data['price']);

        if(auth()->level == 'admin'){
            $data['organization_id'] = $this->input->post('organization');
        }else{
            $data['organization_id'] = auth()->id;
        }
        $data['status'] = $this->input->post('submit') == 'SUBMIT' ? TrainingStatus::SUBMIT : TrainingStatus::DRAFT;

        /*$this->db->trans_begin(); */

        // Create Training
        if(isset($_POST['edit'])){
            $training_id = $this->input->post('training_id');
            $training['id'] = $training_id;
            $this->training_model->editTraining($training_id,$data);
            if ($_FILES['images']['error'][0] == 0)  {
                $this->training_model->deleteImages($training_id);
            }
        }else{
            $training = $this->training_model->create($data, true);
        }

        // Save Tags
        if ($tags = $this->input->post('tags')) {
            $this->training_model->sync_tags($training['id'], $tags);
        }

        // Upload Images
        if (isset($_FILES['images'])) {
            $this->training_model->attach_images($training['id'], $_FILES['images']);
        }
        
        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            show_error('gagal');
        } else {
            $this->db->trans_commit();
        }
        
        $this->redirect('/admin/training', 'Berhasil submit pelatihan', 'warning');

    }

    public function edit($id)
    {
        if ($this->request_method('POST')) {
            $this->store();
        }

        if ($training = $this->training_model->find("trainings.id = $id AND trainings.status IN(1,2,3)")) {
            $this->load->model('tag_model');
            $training['tags'] = $this->tag_model->getByTraining($id);
            $training['tagsAll'] = $this->tag_model->get_all();
            $training['images'] = $this->db->get_where('training_images',array('training_id' => $id));
            $this->load->view('dashboard/training/edit', compact('training'));
        } else {
            $this->session->set_flashdata('danger','Pelatihan ini sedang berjalan atau sudah selesai, anda tidak dapat merubahnya!');
            redirect(dashboard_url('training'));
        }
    }

    public function publish($id)
    {
        if($this->auth()->level != 'admin'){
            redirect(dashboard_url('training'));
        }
        
        $this->db->set(array('status' => 3));
        $this->db->where('id',$id);
        $this->db->update('trainings');
        $this->session->set_flashdata('warning','Pelatihan/Sertifikasi berhasil di terbitkan! ');
        redirect(dashboard_url('training'));
    }

    public function unpublish($id)
    {
        if ($training = $this->training_model->find("trainings.id = $id AND trainings.status IN(4,5)")) {
            $this->session->set_flashdata('danger','Pelatihan ini sedang berjalan atau sudah selesai, anda tidak dapat melakukan aksi ini!');
            redirect(dashboard_url('training'));
        }
        
        else{
            $this->db->set(array('status' => 2));
            $this->db->where('id',$id);
            $this->db->update('trainings');
            $this->session->set_flashdata('warning','Status berhasil diubah!');
            redirect(dashboard_url('training'));
        }
    }


    public function cancel($id)
    {
        if ($training = $this->training_model->find("trainings.id = $id AND trainings.status IN(3,4,5)")) {
            $this->session->set_flashdata('danger','Pelatihan ini sedang berjalan atau sudah selesai, anda tidak dapat menghapus pelatihan ini!');
            redirect(dashboard_url('training'));
        } 
        
        else {
            $this->db->delete('training_images',array('training_id' => $id));
            $this->db->delete('trainings',array('id' => $id));

            return redirect('/admin/training');
        }
    }

    public function delete($id)
    {
        $this->db->delete('training_images',array('training_id' => $id));
        $this->db->delete('trainings',array('id' => $id));

        return redirect('/admin/training');
    }

    public function participantPaid($id)
    {
        $idAll = explode('-',$id);
        if(count($idAll) < 2){
            redirect(dashboard_url());
        }else{
            
            $id_user = $idAll[0];
            $id_training = $idAll[1];
                    
            $this->db->set(array('is_paid' => 1));
            $this->db->where('id',$id_user);
            $this->db->update('participants');
            $this->session->set_flashdata('warning','Status berhasil diubah');

            $id = $id_user;
            $this->email_konfirmasi_pembayaran($id);

            if($id_training == 'none'){
                redirect(dashboard_url('pembayaran'));
            }else{
                redirect(dashboard_url('training/'.$id_training.'/manage'));
            }   
        }
    }

    public function participantPaidPost()
    {
        if(isset($_POST['idparticipant'])){

            $this->db->set(array('is_paid' => 1));
            $this->db->where('id',$this->input->post('idparticipant'));
            $this->db->update('participants');

            $id = $this->input->post('idparticipant');
            $this->email_konfirmasi_pembayaran($id);

            $this->session->set_flashdata('warning','Status berhasil diubah');
            redirect(dashboard_url('pembayaran'));
        } 

        else {
            redirect(dashboard_url());
        }
    }

    protected function email_konfirmasi_pembayaran($id)
    {
        $this->load->library('email');
        $this->load->config('email');
        $this->load->model('payment_model');

        // send email
        $inv = $this->payment_model->get_training_byPayment($id)->row_array();
        $content_email = $this->load->view('email/invoicePeserta',compact('inv'),true);

        //$this->email->initialize($config);
          
        $this->email->from('no-reply@utc.nkmd-uii.id', 'Admin UII Training Center');
        $this->email->to($inv['email']);
        //$this->email->cc('another@another-example.com');
        //$this->email->bcc('them@their-example.com');
        $this->email->subject('Pembayaran anda berhasil');
        $this->email->message($content_email);
        $this->email->send();

    }

    public function participantReject($id)
    {
        $idAll = explode('-',$id);
        $id_user = $idAll[0];
        $id_training = $idAll[1];        
        $this->db->set(array('is_paid' => 3));
        $this->db->where('id',$id_user);
        $this->db->update('participants');
        $this->session->set_flashdata('warning','Data berhasil di reject!');
        redirect(dashboard_url('training/'.$id_training.'/manage'));
    } 

    public function participantUnreject($id)
    {
        $idAll = explode('-',$id);
        $id_user = $idAll[0];
        $id_training = $idAll[1];        
        $this->db->set(array('is_paid' => 0));
        $this->db->where('id',$id_user);
        $this->db->update('participants');
        $this->session->set_flashdata('warning','Status berhasil diubah');
        redirect(dashboard_url('training/'.$id_training.'/manage'));
    }

    public function participantRejected($id)
    {
        $peserta = $this->training_model->get_participant(array('training_id' => $id,'is_paid' => 3 ));
        if($peserta->num_rows() == 0) {
            show_404();
        }

        else {
            $training = $this->training_model->find(array('trainings.id' => $id, 'trainings.status' => 3));
            $training['tags'] = $this->training_model->load_tags($training['id']);
            $training['images'] = $this->training_model->load_images($training['id']);
            $training['peserta'] = $peserta;
            $this->load->view('dashboard/training/rejectedParticipant',compact('training'));
        }
    }

    public function participantAll($id)
    {
        $peserta = $this->training_model->get_participant(array('training_id' => $id));
        if($peserta->num_rows() == 0) {
            $this->session->set_flashdata('danger','Data tidak ditemukan!');
            redirect(dashboard_url('training/'.$id.'/manage'));
        }

        else {
            $training = $this->training_model->find(array('trainings.id' => $id));
            $training['tags'] = $this->training_model->load_tags($training['id']);
            $training['images'] = $this->training_model->load_images($training['id']);
            $training['peserta'] = $peserta;
            $this->load->view('dashboard/training/rejectedParticipant',compact('training'));
        }
    }

    public function detailparticipant()
    {
        $id = $this->input->post('id');

        if($this->training_model->get_paymentByIdUser($id)->num_rows() == 0){
            $prt = $this->training_model->get_participantDetail($id)->row_array();
            $prt['payment'] = false;
        }else{
            $prt = $this->training_model->get_paymentByIdUser($id)->row_array();
            $prt['payment'] = true;
        }
        $this->load->view('dashboard/training/detailparticipant',compact('prt'));
    }

    public function changestatus()
    {
        $this->db->where('id',$this->input->post('training_id'));
        $this->db->update('trainings',array('status' => $this->input->post('status')));

        $this->session->set_flashdata('warning','Status pelatihan berhasil di perbarui!');
        redirect(dashboard_url('training/'.$this->input->post('training_id').'/manage'));
    }

    public function postUpdate()
    {
        if($this->input->post('ket') == "add"){
            $uniqID = $this->encryption->encrypt(rand(0000,9999));
            $this->db->insert('training_update', array(
                'uniqID'=> $uniqID,
                'title' => $this->input->post('title'),
                'note'  => $this->input->post('note'),
                'training_id' => $this->input->post('training_id')
            ));
        } 
        elseif($this->input->post('ket') == "edit"){
            $this->db->where('uniqID',$this->input->post('id'));
            $this->db->update('training_update', array(
                'title' => $this->input->post('title'),
                'note'  => $this->input->post('note'),
                'training_id' => $this->input->post('training_id')
            ));
        }

        $this->session->set_flashdata('warning','Kabar berhasil di terbitkan!');
        redirect(dashboard_url('training/'.$this->input->post('training_id').'/manage'));
    }

    public function deleteUpdate($id)
    {
        $key = explode('-',$id);
        $update_id = $key[0];
        $training_id = $key[1];

        $this->db->delete('training_update',array('training_id' => $training_id, 'uniqID' => $update_id));

        $this->session->set_flashdata('warning','Data berhasil di hapus!');
        redirect(dashboard_url('training/'.$training_id.'/manage'));
    }

    public function editUpdate()
    {
        $upt0 = $this->db->get_where('training_update',array('uniqID' => $this->input->post('id')));
        if($upt0->num_rows() == 0){
            $upt['title'] = "";
            $upt['note'] = "";
        }else{
            $upt = $upt0->row_array();
        }
        $upt['id'] = $this->input->post('id');
        $upt['training_id'] = $this->input->post('training');
        $upt['ket'] = $this->input->post('ket');
        $this->load->view('dashboard/training/formEditUpdate',compact('upt'));
    }
}