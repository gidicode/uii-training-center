<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use My\Enums\SertifikasiStatus;

class Tuk extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();

        if (!$this->session->userdata('user')) {
            redirect('tuk/login');
        }

        $this->load->model('organization_model');
        $this->load->model('Sertifikasi_model');
        $this->load->model('Skema_model');
        $this->load->model('training_model');
        $this->load->model('TukProfile_model');
        $this->load->library('email');

    }

    public function index()
    {  
        
        if (auth()->status ==  0) {
            redirect(base_url('tuk/pending'));
        }
        if($this->request_method('POST')){
            return $this->post_addSkema();
        }

        //auth()->id

        $skm = $this->db->get_where('tuk_skema',array('id_tuk' => auth()->id ));
        
        
        $data['ifskema'] = $skm->num_rows();
        $data['skema'] = $this->Skema_model->get_jumlah_sertifikasi(auth()->id);
        $this->load->view('dashboard/dashboard_tuk',$data);
    }

    public function statusPending()
    {
        if (auth()->status ==  1) {
            redirect(base_url('tuk'));
        }

        $this->load->view('pages/organisasi/thankyouTuk');
    }

    public function berkasTambahan()
    {
        if($this->request_method('POST')){
            return $this->post_berkasTambahan();
        }
        $id = auth()->id;
        
        $data['tempat'] = $this->db->get_where('tuk_tempat', array('id_tuk' => $id));
        $data['peralatan'] = $this->db->get_where('tuk_peralatan', array('id_tuk' => $id));
        $this->load->view('pages/organisasi/berkasTambahan',$data);
    }

    protected function post_berkasTambahan()
    {
        $id = auth()->id;
        
        $spesifikasiTempat = $this->input->post('spesifikasiTempat');
        $jumlahTempat = $this->input->post('jumlahTempat');
        $kondisiTempat = $this->input->post('kondisiTempat');

        $spesifikasiBahan = $this->input->post('spesifikasiBahan');
        $jumlahBahan = $this->input->post('jumlahBahan');
        $kondisiBahan = $this->input->post('kondisiBahan');

        $jmlTempat = count($spesifikasiTempat);
        $jmlBahan = count($spesifikasiBahan);

        $this->db->delete('tuk_tempat', array('id_tuk' => $id));
        $this->db->delete('tuk_peralatan', array('id_tuk' => $id));

        for($a = 0; $a < $jmlTempat; $a++)
        {
            $datatempat = array(
                'id_tuk'        => $id,
                'spesifikasi'   => $spesifikasiTempat[$a],
                'jumlah'        => $jumlahTempat[$a],
                'kondisi'       => $kondisiTempat[$a]
            );
            $this->db->insert('tuk_tempat',$datatempat);
        }

        for($b = 0; $b < $jmlBahan; $b++)
        {
            $databahan = array(
                'id_tuk'        => $id,
                'spesifikasi'   => $spesifikasiBahan[$b],
                'jumlah'        => $jumlahBahan[$b],
                'kondisi'       => $kondisiBahan[$b]
            );

            $this->db->insert('tuk_peralatan',$databahan);
        }

        redirect(base_url('thank-you'));
    }

    protected function post_addSkema()
    {
        $data_skema = array (
            'id_tuk' => auth()->id,
            'skema' => $this->input->post('skema'),
            'keterangan' => $this->input->post('keterangan')
        );
        if($this->input->post('submitType') == 'submit') {
            $this->db->insert('tuk_skema',$data_skema);
        } elseif ($this->input->post('submitType') == 'edit') {
            $this->db->update('tuk_skema',$data_skema, array('id' => $this->input->post('id')));
        }

        echo '
            <script>javascript:history.go(-1)</script>
        ';
    }

    public function deleteSkema($id)
    {
        $this->db->delete('tuk_skema', array('id' => $id));
        redirect('tuk');
        
    }

    // kelola sertifikasi

    public function sertifikasi()
    {
        $certifications = $this->Sertifikasi_model->get_by_tuk(auth()->id);
        $this->load->view('dashboard/sertifikasi/index', compact('certifications'));
    }

    public function createSertifikasi()
    {
        if ($this->request_method('POST')) {
            $this->storeSertifikasi();
        }
        
        $this->load->model('Skema_model');
        $skemas = $this->Skema_model->get_skema_by_tuk(auth()->id);
        $this->load->view('dashboard/sertifikasi/create' ,compact('skemas'));
    }

    protected function storeSertifikasi()
    {
        $data = array_only(
            $this->input->post(),
            [
                'name', 'description', 'location', 'price', 'infoPembayaran',
                'quota', 'start_time', 'closed_at', 'skema_id'
            ]
        );
        $data['price'] = rupiah_to_int($data['price']);
        $data['tuk_id'] = Auth()->id;
        $data['status'] = $this->input->post('submit') == 'SUBMIT' ? SertifikasiStatus::SUBMIT : SertifikasiStatus::DRAFT;

        //Create Certification
        
        if(isset($_POST['edit'])){
            $sertifikasi_id = $this->input->post('sertifikasi_id');
            $sertifikasi['id'] = $sertifikasi_id;
            $this->Sertifikasi_model->editSertifikasi($sertifikasi_id,$data);
            if ($_FILES['images']['error'][0] == 0)  {
                $this->Sertifikasi_model->deleteImages($sertifikasi_id);
            }
        }else{
            $sertifikasi = $this->Sertifikasi_model->create($data, true);
        }

       // Upload Images
        if (isset($_FILES['images'])) {
            $this->Sertifikasi_model->attach_images($sertifikasi['id'], $_FILES['images']);
        }

        if ($this->db->trans_status() === FALSE) {
            $this->db->trans_rollback();
            show_error('gagal');
        } else {
            $this->db->trans_commit();
        }
        
        $this->redirect('/tuk/sertifikasi', 'Berhasil submit sertifikasi', 'warning');
    }

    public function editSertifikasi($id)
    {
        if ($this->request_method('POST')) {
            $this->store();
        }

        if ($sertifikasi = $this->Sertifikasi_model->find("tuk_sertifikasi.id = $id AND tuk_sertifikasi.status IN(1,2,3)")) {
            $sertifikasi['images'] = $this->db->get_where('tuk_sertifikasi_image',array('sertifikasi_id' => $id));
            $sertifikasi['skemas'] = $this->Skema_model->get_skema_by_id($sertifikasi['skema_id']);
            $this->load->view('dashboard/sertifikasi/edit' ,compact('sertifikasi'));
        } else {
            $this->session->set_flashdata('danger','Sertifikasi ini sedang berjalan atau sudah selesai, anda tidak dapat merubahnya!');
            redirect(dashboard_url('training'));
        }
        
    }

    public function showTuk($id)
    {
        if ($sertifikasi = $this->Sertifikasi_model->find(['tuk_sertifikasi.id' => $id])) {
            $sertifikasi['skema'] = $this->Sertifikasi_model->load_skema($sertifikasi['skema_id']);
            $sertifikasi['images'] = $this->Sertifikasi_model->load_images($sertifikasi['id']);
            $this->load->view('dashboard/sertifikasi/show-tuk', compact('sertifikasi'));
        } else {
            show_404();
        }
    }

    //Show Certificate by Skema
    public function showSkema($id)
    {
            $certifications = $this->Sertifikasi_model->get_by_skema($id);
            $data = $this->Skema_model->get_skema_by_id($id);
            $this->load->view('dashboard/sertifikasi/skema-show', compact('certifications','data'));

    }

    public function manageTuk($id)
    {
        if ($sertifikasi = $this->Sertifikasi_model->find(array('tuk_sertifikasi.id' => $id, 'tuk_sertifikasi.status !=' => 0,'tuk_sertifikasi.status !=' => 1,'tuk_sertifikasi.status !=' => 2))) {
            $sertifikasi['sertifikasi_id'] = $id;
            $sertifikasi['skema'] = $this->Sertifikasi_model->load_skema($sertifikasi['skema_id']);
            $sertifikasi['images'] = $this->Sertifikasi_model->load_images($sertifikasi['id']);
            $sertifikasi['peserta'] = $this->Sertifikasi_model->get_participant(array('sertifikasi_id' => $id,'is_paid !=' => 3 ));
            $sertifikasi['peserta_paid'] = $this->Sertifikasi_model->get_participant(array('sertifikasi_id' => $sertifikasi['id'],'is_paid' => 1 ));
            $sertifikasi['peserta_nilai'] = $this->Sertifikasi_model->get_participant_nilai($id);
            $sertifikasi['peserta_reject'] = $this->Sertifikasi_model->get_participant(array('sertifikasi_id' => $sertifikasi['id'],'is_paid' => 3 ));
            $this->load->view('dashboard/sertifikasi/manage-tuk', compact('sertifikasi'));
        } else {
            redirect(base_url('tuk/sertifikasi'));
        }
    }

    public function publish($id)
    {
        $this->db->set(array('status' => 3));
        $this->db->where('id',$id);
        $this->db->update('tuk_sertifikasi');
        $this->session->set_flashdata('warning','Pelatihan/Sertifikasi berhasil di terbitkan! ');
        redirect(base_url('tuk/sertifikasi'));
    }

    public function unpublish($id)
    {
        if ($certifications = $this->Sertifikasi_model->find("tuk_sertifikasi.id = $id AND tuk_sertifikasi.status IN(4,5)")) {
            $this->session->set_flashdata('danger','Sertifikasi ini sedang berjalan atau sudah selesai, anda tidak dapat melakukan aksi ini!');
            redirect(base_url('tuk/sertifikasi'));
        }
        
        else{
            $this->db->set(array('status' => 2));
            $this->db->where('id',$id);
            $this->db->update('tuk_sertifikasi');
            $this->session->set_flashdata('warning','Status berhasil diubah!');
            redirect(base_url('tuk/sertifikasi'));
        }
    }

    public function cancel($id)
    {
        if ($certifications = $this->Sertifikasi_model->find("tuk_sertifikasi.id = $id AND tuk_sertifikasi.status IN(3,4,5)")) {
            $this->session->set_flashdata('danger','Sertifikasi ini sedang berjalan atau sudah selesai, anda tidak dapat menghapus sertifikasi ini!');
            redirect(base_url('tuk/sertifikasi'));
        } 
        
        else {
            $this->db->delete('tuk_sertifikasi_image',array('sertifikasi_id' => $id));
            $this->db->delete('tuk_sertifikasi',array('id' => $id));

            return redirect('/tuk/sertifikasi');
        }
    }

    public function delete($id)
    {
        $this->db->delete('tuk_sertifikasi_image',array('sertifikasi_id' => $id));
        $this->db->delete('tuk_sertifikasi',array('id' => $id));

        return redirect('/tuk/sertifikasi');
    }

    public function changestatus()
    {
        $this->db->where('id',$this->input->post('sertifikasi_id'));
        $this->db->update('tuk_sertifikasi',array('status' => $this->input->post('status')));

        $this->session->set_flashdata('warning','Status sertifikasi berhasil di perbarui!');
        redirect('tuk/sertifikasi/'.$this->input->post('sertifikasi_id').'/manage');
    }

    public function detailparticipant()
    {
        $id = $this->input->post('id');

        if($this->Sertifikasi_model->get_paymentByIdUser($id)->num_rows() == 0){
            $prt = $this->Sertifikasi_model->get_participantDetail($id)->row_array();
            $prt['payment'] = false;
        }else{
            $prt = $this->Sertifikasi_model->get_paymentByIdUser($id)->row_array();
            $prt['payment'] = true;
        }
        $this->load->view('dashboard/sertifikasi/detailparticipant',compact('prt'));
    }

    public function participantPaid($id)
    {
        $idAll = explode('-',$id);
        if(count($idAll) < 2){
            redirect(dashboard_url());
        }else{
            
            $id_user = $idAll[0];
            $id_sertifikasi = $idAll[1];
                    
            $this->db->set(array('is_paid' => 1));
            $this->db->where('id',$id_user);
            $this->db->update('tuk_sertifikasi_participants');
            $this->session->set_flashdata('warning','Status berhasil diubah');

            $id = $id_user;
            //$this->email_konfirmasi_pembayaran($id);

            if($id_sertifikasi == 'none'){
                redirect(base_url('tuk/pembayaran'));
            }else{
                redirect(base_url('tuk/sertifikasi/'.$id_sertifikasi.'/manage'));
            }   
        }
    }

    public function participantReject($id)
    {
        $idAll = explode('-',$id);
        $id_user = $idAll[0];
        $id_sertifikasi = $idAll[1];        
        $this->db->set(array('is_paid' => 3));
        $this->db->where('id',$id_user);
        $this->db->update('tuk_sertifikasi_participants');
        $this->session->set_flashdata('warning','Data berhasil di reject!');
        redirect(base_url('tuk/sertifikasi/'.$id_sertifikasi.'/manage'));
    } 

    public function participantUnreject($id)
    {
        $idAll = explode('-',$id);
        $id_user = $idAll[0];
        $id_sertifikasi = $idAll[1];        
        $this->db->set(array('is_paid' => 0));
        $this->db->where('id',$id_user);
        $this->db->update('tuk_sertifikasi_participants');
        $this->session->set_flashdata('warning','Status berhasil diubah');
        redirect(base_url('tuk/sertifikasi/'.$id_sertifikasi.'/manage'));
    }


    public function participantRejected($id)
    {
        $peserta = $this->Sertifikasi_model->get_participant(array('sertifikasi_id' => $id,'is_paid' => 3 ));
        if($peserta->num_rows() == 0) {
            show_404();
        }

        else {
            $sertifikasi = $this->Sertifikasi_model->find(array('tuk_sertifikasi.id' => $id, 'tuk_sertifikasi.status' => 3));
            //$sertifikasi['tags'] = $this->training_model->load_tags($training['id']);
            $sertifikasi['images'] = $this->Sertifikasi_model->load_images($sertifikasi['id']);
            $sertifikasi['peserta'] = $peserta;
            $this->load->view('dashboard/sertifikasi/rejectedParticipant',compact('sertifikasi'));
        }
    }

    public function participantAll($id)
    {
        $peserta = $this->Sertifikasi_model->get_participant(array('sertifikasi_id' => $id));
        if($peserta->num_rows() == 0) {
            $this->session->set_flashdata('danger','Data tidak ditemukan!');
            redirect(base_url('tuk/sertifikasi/'.$id.'/manage'));
        }

        else {
            $sertifikasi = $this->Sertifikasi_model->find(array('tuk_sertifikasi.id' => $id));
           // $training['tags'] = $this->training_model->load_tags($training['id']);
            $sertifikasi['images'] = $this->Sertifikasi_model->load_images($sertifikasi['id']);
            $sertifikasi['peserta'] = $peserta;
            $this->load->view('dashboard/sertifikasi/rejectedParticipant',compact('sertifikasi'));
        }
    }

    public function post_sertifikasi_nilai()
    {
        if($this->request_method('POST')){
            return $this->post_nilai();
        }
    }

    protected function post_nilai()
    {
        $data_nilai = array (
            'id_user' => $this->input->post('user_id'),
            'id_sertifikasi' => $this->input->post('sertifikasi_id'),
            'nilai' => $this->input->post('nilai'),
            'keterangan' => $this->input->post('keterangan')
        );
        if($this->input->post('submitNilai') == 'submit') {
            $this->db->insert('tuk_sertifikasi_nilai',$data_nilai);
        } elseif ($this->input->post('submitType') == 'edit') {
            $this->db->update('tuk_sertifikasi_nilai',$data_nilai, array('id_user' => $this->input->post('user_id')));
        }

        echo '
            <script>javascript:history.go(-1)</script>
        ';
    }

    // kelola profile
    public function profile()
    {
        $profile = $this->TukProfile_model->find(['id' => auth()->id]);
        //echo var_dump($profile);
        $this->load->view('dashboard/tuk_profile/index' , compact('profile'));
    }

    protected function post_profile()
    {
        $this->load->view('dashboard/tuk_profile/index');
    }


    
}
