<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use My\Enums\TrainingStatus;
use My\Middleware\OrganizationOnly;

class Dashboard extends MY_Controller //implements OrganizationOnly
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->session->userdata('user')) {
            redirect('admin/login');
        }
        if ($this->session->userdata('dasbor') == 0) {
            redirect('admin/login');
        }
    }

    public function index()
    {
        $this->load->model('training_model');
        $this->load->model('payment_model');
        $this->load->model('organization_model');

        if(auth()->level == 'admin'){
            $data['lembaga'] = $this->organization_model->findOther(array('organizations.status' => 0));
            $data['pelatihan'] = $this->training_model->findOther("trainings.status IN(3,4,5)");
            $data['lembagaAktif'] = $this->organization_model->findOther(array('organizations.status' => 1, 'organizations.level !=' => 'admin'));
            $data['lembagaAll'] = $this->organization_model->findOther(array('organizations.level !=' => 'admin'));
            $data['TrainingAktif'] = $this->training_model->get_training_by(array('organizations.status' => 1));
            $data['TrainingAll'] = $this->training_model->get_training_by(array('trainings.status !=' => 0));
            $this->load->view('dashboard/dashboard',compact('data'));
        } else {
            $id = auth()->id;
            $data['pembayaran'] = $this->payment_model->get_all_confirm(array('trainings.organization_id' => auth()->id,'participants.is_paid' => 0));
            $data['pelatihan'] = $this->training_model->findOther("trainings.organization_id = $id AND trainings.status IN(3,4,5)");
            $data['pendaftarAktif'] = $this->training_model->get_participant_byOrg(array('organizations.id' => $id, 'participants.is_paid' => 1));
            $data['pendaftarAll'] = $this->training_model->get_participant_byOrg(array('organizations.id' => $id));
            $data['TrainingAktif'] = $this->training_model->get_training_by(array('organization_id' => $id, 'organizations.status' => 1));
            $data['TrainingAll'] = $this->training_model->get_training_by(array('organization_id' => $id));
            $this->load->view('dashboard/dashboard_lembaga',compact('data'));
        }
        
    }
}
