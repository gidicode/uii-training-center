<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use My\Middleware\OrganizationOnly;

class Pembayaran extends MY_Controller implements OrganizationOnly
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('payment_model');

        if($this->session->userdata('dasbor') == 0){
            redirect(base_url('admin/login'));
        }
    }

    public function index()
    {
        if(auth()->level == 'admin'){
            $data['data'] = $this->payment_model->get_all_confirm($condition = array('participants.is_paid' => 0));
            $data['admin'] = true;
        }
        
        else{
            $data['data'] = $this->payment_model->get_confirm_byOrg($condition = array(
                                                                                    'participants.is_paid' => 0, 
                                                                                    'trainings.organization_id' => auth()->id)
                                                                                );
            $data['admin'] = false;
        }
        
        $this->load->view('dashboard/pembayaran/index',$data);
    }

    public function allConfirm()
    {
        if(auth()->level == 'admin'){
            $data['data'] = $this->payment_model->get_all_confirm($condition = array());
            $data['admin'] = true;
        }
        
        else{
            $data['data'] = $this->payment_model->get_confirm_byOrg($condition = array(
                                                                                    'trainings.organization_id' => auth()->id)
                                                                                );
            $data['admin'] = false;
        }
        
        $this->load->view('dashboard/pembayaran/index',$data);
    }

    public function detailconfirm()
    {
        $inv = $this->payment_model->get_confirm_byID($this->input->post('id'))->row_array();
        $this->load->view('dashboard/pembayaran/detail',compact('inv'));
    }
}