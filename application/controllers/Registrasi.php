<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Registrasi extends MY_Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        if ($this->request_method('POST')) {
            return $this->post_register();
        }
        
        else if (!$this->session->userdata('idPelatihanDaftar')) {
            redirect(base_url());
        }
        
        else{
            echo $this->session->userdata('idPelatihanDaftar');
        }
    }

    public function daftarUII()
    {
        if ($code = $this->input->get('code')) {
            $this->load->library('google');
            $this->google->setRedirect(base_url('registrasi'));
            $profile = $this->google->validate($code);

            // UII Email
            $mail = explode('@',$profile['email']);

            echo "email adalah ";
            echo $profile['email'];

            /*
            if(strpos($mail[0], 'uii.ac.id') === FALSE){
                $this->session->set_flashdata('alert','Ops, email ini tidak terhubung dengan akun UII');
            } else {
                redirect(base_url('registrasi'));
            }*/
        }

        else {
            redirect(base_url('/sso/google?redirect='.base_url('/registrasi/daftarUII')));
        }
    }

    public function daftarGoogle()
    {

    }

    public function daftarFB()
    {

    }

    public function post_registrasi()
    {

    }
    
}