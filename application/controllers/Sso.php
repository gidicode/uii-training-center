<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sso extends CI_Controller {

    protected $after_login = '/';
    protected $after_reg = '/registrasi';

    public function __construct()
    {
        parent::__construct();
        $this->load->library('google');

        if ($this->session->userdata('logged_in')) {
            redirect('/');
        }
    }

	public function google()
	{
        if ($code = $this->input->get('code')) {
            $profile = $this->google->validate($code);

            $this->load->model('user_model');
            $user = $this->user_model->get_user(['email' => $profile['email']]);
            if (! $user) {
                $user = $this->user_model->create_user([
                    'email' => $profile['email'],
                    'name' => $profile['name'],
                    'avatar' => $profile['avatar'],
                    'google_id' => $profile['id']
                ], true);
                
                $this->session->set_userdata([
                    'registrasiUser' => true,
                    'user' => $user
                ]);
                redirect(base_url('registrasi-user'));
            } 

            $this->session->set_userdata([
                'logged_in' => true,
                'user'      => $user,
                'dasbor'    => 0
            ]);
            
            if($redirect = $this->session->userdata('redirect')){
                redirect($redirect);
            }else{
                redirect($this->after_login);
            }
            
        }

        if ($redirect = $this->input->get('redirect')) {
            $this->google->setRedirect($redirect);
        }

		redirect($this->google->get_login_url());
    }

    public function registrasiUser($data)
	{
		$this->load->view('dashboard/peserta/registrasiUser',$data);
	}

}
