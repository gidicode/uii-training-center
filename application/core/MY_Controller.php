<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->checkMiddlewares();
    }

    private function checkMiddlewares()
    {
        if ($this instanceof \My\Middleware\OrganizationOnly) {
            if (! $this->is_organization()) {
                $this->redirect('/', 'Unauthorized', 'danger');
            }
        }
        if ($this instanceof \My\Middleware\PesertaOnly) {
            if (! $this->is_user()) {
                $this->redirect('/', 'Unauthorized', 'danger');
            }
        }
    }

    protected function request_method($method = null)
    {
        if ($method) {
            return $this->input->server('REQUEST_METHOD') == $method;
        }

        return $this->input->server('REQUEST_METHOD');
    }

    protected function logged_in()
    {
        return $this->session->userdata('logged_in') == true;
    }

    protected function auth()
    {
        return (object) $this->session->userdata('user');
    }

    protected function is_organization()
    {
        return $this->logged_in() && isset($this->auth()->level);
    }

    protected function is_user()
    {
        return $this->logged_in() && ! isset($this->auth()->level);
    }

    protected function redirect($redirect = '/', $message = '', $color = null)
    {
        if (is_null($color)) {
            if (is_array($message)) {
                foreach ($message as $key => $msg) {
                    $this->session->set_flashdata($key, $msg);
                }
            } else {
                $this->session->set_flashdata('info', $message);
            }
        } else {
            $this->session->set_flashdata($color, $message);
        }
        redirect($redirect);
    }
}
