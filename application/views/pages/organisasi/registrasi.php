<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
        <?php $this->load->view('parts/header'); ?>
        
        <div class="container mt-5 mb-5">
            <h3>Registrasi Organisasi Baru</h3><hr>
                <?php echo form_open_multipart('auth/register');?>
                <input type="hidden" name="google_id" value="<?= $profile['id'] ?>">
                <div class="row">
                    <div class="col-md-3">
                    <div style="height: 250px;">
                        <img id="output" style="width:100%;" src="<?= $profile['avatar'] ?>" />
                        <input type="hidden" name="avatar_google" value="<?= $profile['avatar'] ?>">
                    </div>
                    <hr>
                    <div class="fileUpload btn btn-primary ">
                        <span>Ganti Logo Organisasi</span>
                        <input type="file" accept="image/*" onchange="loadFile(event)" name="avatar" class="upload">
                    </div>
                    
                    </div>
                    <div class="col-md-9">
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                            <input type="text" name="email" readonly class="form-control-plaintext" id="staticEmail" value="<?= $profile['email'] ?>" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama Organisasi</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" id="name" value="<?= $profile['first_name'].' '.$profile['last_name'] ?>" name="name" placeholder="Organisasi" required>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="level" class="col-sm-3 col-form-label">Level</label>
                            <div class="col-sm-9">
                            <select name="level" id="level" class="form-control" required>
                                <option value="">Pilih Salah Satu</option>
                                <option value="Prodi">Prodi</option>
                                <option value="Fakultas">Fakultas</option>
                                <option value="Universitas">Universitas</option>
                            </select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 col-form-label">No Telephon</label>
                            <div class="col-sm-9">
                            <input type="number" name="phone" class="form-control" id="phone" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                            <textarea name="deskripsi" class="form-control" id="deskripsi" cols="30" rows="10" required></textarea>
                            </div>
                        </div>

                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">Lanjutkan Registrasi</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script>
            var loadFile = function(event) {
                var reader = new FileReader();
                reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
                };
                reader.readAsDataURL(event.target.files[0]);
            };
        </script>
    </body>
</html>