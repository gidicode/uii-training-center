<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
        <?php //$this->load->view('parts/header'); ?>
        
        <div class="container mb-5" style="margin-top: 10vh;">
            <div class="text-right">
                <p>Hi <?= auth()->nama_tuk ?>, 
                <a href="" onclick="
                    event.preventDefault(); document.getElementById('logout-form').submit();">
                    logout
                </a>
                <form id="logout-form" action="<?= base_url('auth/logout') ?>" class="d-none" method="POST"></form>
            </div>
            <h3>Data Tambahan untuk TUK</h3><hr>
            <h5>Tempat Uji Kompetensi</h5>
            <form action="" method="post">
            <table class="table" id="tb-1">
                <tbody>
                    <?php if($tempat->num_rows() != 0){ foreach($tempat->result() as $t): ?>
                    <tr>
                        <td><input type="checkbox" name="record-1"></td>
                        <td style="width: 70%"><input type="text" name="spesifikasiTempat[]" class="form-control" placeholder="Spesifikasi" value="<?= $t->spesifikasi ?>" required></td>
                        <td style="width: 10%"><input type="number" name="jumlahTempat[]" value="<?= $t->jumlah ?>" class="form-control" placeholder="Jumlah" required></td>
                        <td><input type="text" class="form-control" value="<?= $t->kondisi ?>" name="kondisiTempat[]" placeholder="Kondisi" required></td>
                    </tr>
                    <?php endforeach; } else { ?>
                    <tr>
                        <td><input type="checkbox" name="record-1"></td>
                        <td style="width: 70%"><input type="text" name="spesifikasiTempat[]" class="form-control" placeholder="Spesifikasi" required></td>
                        <td style="width: 10%"><input type="number" name="jumlahTempat[]" value="1" class="form-control" placeholder="Jumlah" required></td>
                        <td><input type="text" class="form-control" name="kondisiTempat[]" placeholder="Kondisi" required></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="text-right">
                <a class="btn btn-sm btn-danger delete-row-1 text-white" >Hapus</a>
                <a class="btn btn-sm btn-primary text-white" id="add-row-1" >Tambah</a>
            </div>
            <br>
            <h5>Peralatan dan Bahan</h5>
            <table class="table" id="tb-2">
                <tbody>
                    <?php if($peralatan->num_rows() != 0){ foreach($tempat->result() as $p): ?>
                    <tr>
                        <td><input type="checkbox" name="record-2"></td>
                        <td style="width: 70%"><input type="text" value="<?= $p->spesifikasi ?>" name="spesifikasiBahan[]" class="form-control" placeholder="Spesifikasi" required></td>
                        <td style="width: 10%"><input type="number" name="jumlahBahan[]" value="<?= $p->jumlah ?>" class="form-control" placeholder="Jumlah" required></td>
                        <td><input type="text" class="form-control" value="<?= $p->kondisi ?>" name="kondisiBahan[]" placeholder="Kondisi" required></td>
                    </tr>
                    <?php endforeach; } else { ?>
                    <tr>
                        <td><input type="checkbox" name="record-2"></td>
                        <td style="width: 70%"><input type="text" name="spesifikasiBahan[]" class="form-control" placeholder="Spesifikasi" required></td>
                        <td style="width: 10%"><input type="number" name="jumlahBahan[]" value="1" class="form-control" placeholder="Jumlah" required></td>
                        <td><input type="text" class="form-control" name="kondisiBahan[]" placeholder="Kondisi" required></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
            <div class="text-right">
                <a class="btn btn-sm btn-danger text-white delete-row-2" >Hapus</a>
                <a class="btn btn-sm btn-primary text-white" id="add-row-2" >Tambah</a>
            </div>

            <button type="submit" class="btn btn-primary">Simpan dan Lanjutkan</button>
            </form>
        </div>

        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script>
            var loadFile = function(event) {
                var reader = new FileReader();
                reader.onload = function(){
                var output = document.getElementById('output');
                output.src = reader.result;
                };
                reader.readAsDataURL(event.target.files[0]);
            };
            $(document).ready(function(){
                $("#add-row-1").click(function(){
                    var markup = "<tr><td><input type='checkbox' name='record-1'></td><td style='width: 70%'><input type='text' class='form-control' placeholder='Spesifikasi' name='spesifikasiTempat[]' required></td><td style='width: 10%'><input type='number' name='jumlahTempat[]' class='form-control' placeholder='Jumlah' value='1' required></td><td><input type='text' name='kondisiTempat[]' class='form-control' placeholder='Kondisi'  required></td></tr>";
                    $("table#tb-1 tbody").append(markup);
                });
                
                // Find and remove selected table rows
                $(".delete-row-1").click(function(){
                    $("table#tb-1 tbody").find('input[name="record-1"]').each(function(){
                        if($(this).is(":checked")){
                            $(this).parents("tr").remove();
                        }
                    });
                });
            });

            $(document).ready(function(){
                $("#add-row-2").click(function(){
                    var markup = "<tr><td><input type='checkbox' name='record-2'></td><td style='width: 70%'><input type='text' class='form-control' placeholder='Spesifikasi' name='spesifikasiBahan[]' required></td><td style='width: 10%'><input type='number' class='form-control' name='jumlahBahan[]' placeholder='Jumlah' value='1' required></td><td><input type='text' class='form-control' name='kondisiBahan[]' placeholder='Kondisi' required></td></tr>";
                    $("table#tb-2 tbody").append(markup);
                });
                
                // Find and remove selected table rows
                $(".delete-row-2").click(function(){
                    $("table#tb-2 tbody").find('input[name="record-2"]').each(function(){
                        if($(this).is(":checked")){
                            $(this).parents("tr").remove();
                        }
                    });
                });
            });

        </script>
    </body>
</html>