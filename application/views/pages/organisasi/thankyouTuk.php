<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php #$this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->
        
        <!-- if have a warning alert -->
        <?php if($this->session->flashdata('warning')){
            echo'
            <div class="alert alert-danger alert-dismissible fade show" style="position:fixed; z-index:999; margin-top:25px; right:25px;">'
                .$this->session->flashdata('warning').
            '<button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            </div>';
        } ?>

        <!-- Section Thank You -->
		<div class="container" style="margin-top: 25vh">
            <div class="card">
                <div class="card-body text-center">
                    <h3>Pendaftaran Tempat Uji Kompetensi berhasil</h3>
                    <br>
                    <p style="font-size:14pt;">Pendaftaran sedang melalui tahap verifikasi, anda akan mendapatkan email konfirmasi ketika akun anda sudah terverifikasi</p>
                    <a href="<?= base_url('tuk/berkas-tambahan') ?>" class="btn btn-primary">Edit Berkas Tambahan</a>
                </div>
            </div>
        </div>

        <!-- Section 6 Footer -->
        <!-- Footer Section -->
		<?php #$this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
                    $(this).remove(); 
                });
            }, 5000);
        });
        </script>
    </body>
</html>