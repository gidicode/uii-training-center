<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4>Konfirmasi Pembayaran</h4>
                        </div>
                        <div class="card-body">
                            <form action="#" method="post">
                                <div class="form-group">
                                    <label>Kode Pembayaran / Invoice</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Masukkan kode pembayaran/Invoice" aria-label="Kode Pembayaran" name="code" required="required">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary" type="submit">Cari!</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section 6 Footer -->
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
                    $(this).remove(); 
                });
            }, 5000);
        });
        </script>
    </body>
</html>