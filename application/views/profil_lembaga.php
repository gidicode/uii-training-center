<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->
        
        <!-- Alert -->
        <?php $this->load->view('parts/alert'); ?>

        <!-- Main page -->
        <div class="content-mobile" style="margin-top:100px"></div>
        <div class="container mt-5 mb-5">
            
            <div class="card">
                <div class="card-body row">
                    <div class="col-md-3">
                        <img src="<?= $data['logo'] ?>" style="width:100%; max-width:150px;" alt="">
                    </div>
                    <div class="col-md-9">
                        <h2><?= $data['name'] ?></h2>
                        <p>
                            <i class="fas fa-envelope mr-2" style="color:#636363"></i><?= $data['email'] ?><br>
                            <i class="fas fa-phone mr-2" style="color:#636363"></i></i><?= $data['phone'] ?>
                        </p>
                        <p>
                            <?= $data['deskripsi'] ?>
                        </p>
                    </div>
                </div>
            </div>

            <div class="mt-5"></div>
            <hr>
            <h4>Pelatihan / Sertifikasi saat ini</h4>
            
            <div class="row mt-3">

            
            <?php 
            if($data['training']->num_rows() == 0){
                echo "<div class='col-md-12'><p>Tidak ada pelatihan saat ini</p></div>";
            }
            foreach($data['training']->result() as $t):
            $data['t'] = $t;
            $this->load->view('parts/pelatihan',$data);
            endforeach; ?>

            </div>

        </div>
        
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>
        
    </body>
</html>