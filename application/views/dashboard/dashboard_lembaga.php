<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                Dashboard
            </li>
        </ol>
        <!-- END BREADCRUMBS -->

        <div class="row m-0">
            <div class="col-md-6">
                <h1 class="h5 mt-3" style="font-weight:400">Welcome,</h1>
                <h2 class="h2" style="font-weight:400"> <?= auth()->name ?></h2>
            </div>
            <div class="col-md-3">
                <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers">
                        <?= $data['pendaftarAktif']->num_rows() ?>
                        <small>/ <?= $data['pendaftarAll']->num_rows() ?></small>
                    </span>
                    <span class="count-name">Jumlah Pendaftar </span>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card-counter primary">
                    <i class="fas fa-calendar-alt"></i>
                    <span class="count-numbers">
                        <?= $data['TrainingAktif']->num_rows() ?>
                        <small>/ <?= $data['TrainingAll']->num_rows() ?></small>
                    </span>
                    <span class="count-name">Pelatihan Aktif</span>
                </div>
            </div>
        </div>
        
        <hr>

        <div class="row m-0">
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <p class="mb-0" style="font-size:14pt; font-weigh:500">Data Pembayaran Masuk</p>
                    </div>
                    <div class="card-body">
                        <?php if($data['pembayaran']->num_rows() == 0){ ?>
                            <p><i class="fas fa-exclamation-circle"></i> Tidak ada data pembayaran masuk</p>
                        <?php }else{ ?>
                        <table class="table table-hover table-sm">
                            <thead>
                                <th>#</th>
                                <th>InvoiceID</th>
                                <th>Pelatihan</th>
                                <th>Nama</th>
                            </thead>
                            
                            <tbody>
                                <?php foreach($data['pembayaran']->result() as $i => $pay): ?>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td><?= $pay->invoice_id ?></td>
                                    <td><?= $pay->training ?></td>
                                    <td><?= $pay->participant ?></td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <a href="<?= dashboard_url('pembayaran') ?>" class="btn btn-primary btn-sm float-right">lihat semua</a>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        <p class="mb-0" style="font-size:14pt; font-weigh:500">Data Pelatihan Aktif</p>
                    </div>
                    <div class="card-body">
                        <?php if($data['pelatihan']->num_rows() == 0){ ?>
                            <p><i class="fas fa-exclamation-circle"></i> Tidak ada data pelatihan aktif</p>
                        <?php }else{ ?>
                        <table class="table table-hover table-borderless table-sm">
                            <tbody>
                                <?php foreach($data['pelatihan']->result() as $training): ?>
                                <tr>
                                    <td><?= $training->id ?></td>
                                    <td><?= $training->name ?></td>
                                    <td><?= $this->db->get_where('participants',array('training_id' => $training->id, 'is_paid' => 1))->num_rows() ?> Peserta</td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <a href="<?= dashboard_url('training') ?>" class="btn btn-primary btn-sm float-right">lihat semua</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
</body>
</html>