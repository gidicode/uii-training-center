<table class="table table-sm table-hover mt-3">
    <tr>
        <th class="w-25">Status</th><td><?= \My\Enums\SertifikasiStatus::render($sertifikasi['status']) ?></td>
    </tr>
    <tr>
        <th>Nama</th><td><?= $sertifikasi['name'] ?></td>
    </tr>
    <tr>
        <th>Tanggal Sertifikasi</th>
        <td><?= carbon($sertifikasi['start_time'])->isoFormat('DD MMMM YYYY') //. ($sertifikasi['start_time'] == $sertifikasi['finish_time'] ? '' : ' - '. carbon($training['finish_time'])->isoFormat('DD MMMM YYYY')) ?></td>
    </tr>
    <tr>
        <th>Sertifikasi</th>
        <td><?= $sertifikasi['location'] ?></td>
    </tr>
    <tr>
        <th>Deskripsi</th>
        <td><?= $sertifikasi['description'] ?></td>
    </tr>
    <tr>
        <th>Materi</th>
        <td></td>
    </tr>
    <tr>
        <th>Fasilitas</th>
        <td></td>
    </tr>
    <tr>
        <th>Batas Waktu Pendaftaran</th>
        <td><?= carbon($sertifikasi['closed_at'])->isoFormat('DD MMMM YYYY') ?></td>
    </tr>
    <tr>
        <th>Kuota Peserta</th>
        <td><?= $sertifikasi['quota'] ?></td>
    </tr>
    <tr>
        <th>Harga</th>
        <td><?= int_to_rupiah($sertifikasi['price']) ?></td>
    </tr>
    <tr>
        <th>Info Pembayaran</th>
        <td><?= $sertifikasi['infoPembayaran'] ?></td>
    </tr>
    <tr>
        <th>Skema</th>
        <td><?php foreach ($sertifikasi['skema'] as $skema): ?>
                <?= $skema['skema']?>
            <?php endforeach; ?>
        </td>
    </tr>
    <tr>
        <th>Gambar</th>
        <td>
            <?php foreach ($sertifikasi['images'] as $image): ?>
                <img src="<?= $image['filename'] ?>" alt="" class="img-thumbnail" width="100" height="100">
            <?php endforeach; ?>
        </td>
    </tr>
</table>