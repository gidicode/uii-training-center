<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | <?= $sertifikasi['name'] ?></title>
    <?php $this->load->view('dashboard/parts/head'); ?>
    
    <!-- Froala Editor -->
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebarTuk'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk') ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk/sertifikasi') ?>">Kelola Sertifikasi</a>
            </li>
            <li class="breadcrumb-item">
                <?= $sertifikasi['id'].'. '. $sertifikasi['name'] ?>
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="m-3">
            <!-- Detail Sertifikasi -->
            <div class="card card-body my-2">
                
                <div class="row">
                    <div class="col-md-8">
                        <h1 style="font-weight:400"><?= $sertifikasi['name']; ?>
                        </h1>
                        <p>
                        <?php 
                            echo '<strong>'.$sertifikasi['tuk_name'].'</strong>, ';
                        ?>
                        <?= carbon($sertifikasi['start_time'])->isoFormat('DD MMMM YYYY') ?></p>
                    </div>
                    <div class="col-md-4 text-right">
                        <?php $this->load->view('dashboard/sertifikasi/button_status_manage'); ?>
                    </div>
                </div>
                
                <div class="mt-3"></div>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                    <?php if($sertifikasi['status'] == 3 or $sertifikasi['status'] == 4 ){ ?>
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Peserta</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Detail</a>
                        
                        <?php }else{ ?>
                        <a class="nav-item nav-link active" id="nav-update-tab" data-toggle="tab" href="#nav-update" role="tab" aria-controls="nav-update" aria-selected="true">Update</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Peserta</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Detail</a>
                        <?php } ?>
                </nav>

                <div class="tab-content" id="nav-tabContent">
                    
                    <!-- Jika status sertifikasi adalah Finish -->
                    <div class="tab-pane fade <?php if($sertifikasi['status'] == 5 ){ echo "show active"; } ?>"  id="nav-update" role="tabpanel" aria-labelledby="nav-update-tab">
                        <div class="mt-3"></div>
                        <?php $trn = $sertifikasi['peserta_nilai'];?>
                        <div style="display: flow-root">
                        <p class="float-left">Jumlah peserta: <?= $trn->num_rows(); ?></p>
                        </div>
                        <table class="table table-hover table-sm mt-3" id="datatable">
                            <thead>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Institusi</th>
                                <th>Email</th>
                                <th>No Handphone</th>
                                <th>Nilai</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                <?php 
                                foreach($trn->result() as $i => $prt):  ?>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td><?= $prt->name ?></td>
                                    <td><?= $prt->institusi ?></td>
                                    <td><?= $prt->email ?></td>
                                    <td><?= $prt->phone ?></td>
                                    <td><?= $prt->nilai ?>
                                    </td>
                                    <td>
                                        <?php 
                                        $data['prt'] = $prt;
                                        $data['id_sertifikasi'] = $sertifikasi['id'];
                                        $this->load->view('dashboard/sertifikasi/table-actions-sertifikasi',$data);
                                        if($prt->nilai == NULL){echo'
                                            <a href=""
                                                class="btn btn-sm btn-primary" data-toggle="modal" data-target="#modalNilai"
                                                title="Update Nilai">
                                                Tambah Nilai
                                            </a>';}
                                        else{echo'
                                            <a href=""
                                                class="btn btn-sm btn-success" data-toggle="modal" data-target="#edit'.$prt->id.'"
                                                title="Update Nilai">
                                                Edit Nilai
                                            </a>';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <?php 
                        $data['sertifikasi'] = $sertifikasi;
                        $this->load->view('dashboard/sertifikasi/table-detailSertifikasi',$data); ?>
                    </div>
                    <!-- End of section -->

                     <!-- Jika status Sertifikasi adalah PUBLISHED atau ONGOING-->
                     <div class="tab-pane fade <?php if($sertifikasi['status'] == 3 or $sertifikasi['status'] == 4 ){ echo "show active"; } ?>" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="mt-3"></div>
                        <?php if($sertifikasi['status'] == 4 OR $sertifikasi['status'] == 5){ 
                            $trn = $sertifikasi['peserta_paid'];
                        } 
                        
                        else {
                            $trn = $sertifikasi['peserta'];
                        } ?>
                        <div style="display: flow-root">
                        <p class="float-left">Jumlah peserta: <?= $trn->num_rows(); ?></p>
                        <?php if($sertifikasi['peserta_reject']->num_rows() != 0){ ?>
                        <a href="<?= base_url('tuk/sertifikasi/'.$sertifikasi['id']) ?>/rejected-participant" class="float-right"><i class="fas fa-exclamation-circle"></i> Lihat data yang di-reject</a>
                        <?php } if($sertifikasi['status'] == 4 OR $sertifikasi['status'] == 5) { ?>
                        <a href="<?= base_url('tuk/sertifikasi/'.$sertifikasi['id']) ?>/all-participant" class="float-right"><i class="fas fa-exclamation-circle"></i> Lihat semua data peserta</a>
                        <?php } ?>
                        </div>
                        <table class="table table-hover table-sm mt-3" id="datatable">
                            <thead>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Institusi</th>
                                <th>Email</th>
                                <th>No Handphone</th>
                                <th>Status Bayar</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                <?php 
                                foreach($trn->result() as $i => $prt):  ?>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td><?= $prt->name ?></td>
                                    <td><?= $prt->institusi ?></td>
                                    <td><?= $prt->email ?></td>
                                    <td><?= $prt->phone ?></td>
                                    <td>
                                        <?php 
                                        if($prt->is_paid == 0){ echo "<span class='badge badge-danger'>belum bayar</span>"; } 
                                        elseif($prt->is_paid != 3) { echo "<span class='badge badge-success'>sudah bayar</span>"; }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                        $data['prt'] = $prt;
                                        $data['id_sertifikasi'] = $sertifikasi['id'];
                                        $this->load->view('dashboard/sertifikasi/table-actions-sertifikasi',$data); ?>
                                        <a href="#"
                                            class="btn btn-sm btn-primary" id="showData" data-toggle="tooltip" 
                                            title="lihat detail" data-id="<?= $prt->id ?>">
                                            lihat
                                        
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <?php 
                        $data['sertifikasi'] = $sertifikasi;
                        $this->load->view('dashboard/sertifikasi/table-detailSertifikasi',$data); ?>
                    </div>

                </div>

            </div>
        </div>
    </div>

    <!-- Modal Lihat Detail-->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel" style="font-weight:500">Detail Peserta<h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>
        
    <?php 
        $this->load->view('dashboard/parts/script'); 
        $data['trn'] = $sertifikasi['peserta_nilai'];
        $this->load->view('dashboard/sertifikasi/modal_add_nilai',$data); 
    ?>
    
    <!-- Froala Editor -->
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/js/froala_editor.min.js'></script>
    
    <script>

    $(function() {
        $('#editor').froalaEditor({
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '-', 'paragraphFormat', 'align'],
            height:350
        })
    });

    function konfirmasi()
    {
        if (confirm('Apakah anda yakin ingin melakukan aksi ini?')) {
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin merubah status pembayaran ? Harap berhati-hati, Aksi ini tidak dapat di kembalikan')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });

    $(document).ready(function () {
        $('#reject').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin ingin mereject peserta ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });

    $(function(){
        $(document).on('click','#showData',function(e){
            e.preventDefault();
            $("#myModal").modal('show');
            $.post('<?= base_url('dashboard/tuk/detailparticipant') ?>',
                {id:$(this).attr('data-id')},
                function(html){
                    $(".modal-body").html(html);
                }
            );
        });
    });
    
    </script>
</body>
</html>