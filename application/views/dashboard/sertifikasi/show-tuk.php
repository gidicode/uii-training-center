<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('dashboard/parts/head'); ?>
    <title>Dashboard | <?= $sertifikasi['name'] ?></title>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebarTuk'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk') ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk/sertifikasi') ?>">Kelola Sertifikasi</a>
            </li>
            <li class="breadcrumb-item">
                <?= $sertifikasi['id'].'. '. $sertifikasi['name'] ?>
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="m-3">
            <!-- Detail Pelatihan -->
            <div class="card card-body my-2">
                <div class="text-right">
                    <?php if($sertifikasi['status'] == 3){
                        echo '<a href="'.base_url('tuk/sertifikasi/'.$sertifikasi['id'].'/unpublish').'" class="btn btn-warning" data-method="POST">UNPUBLISH</a>';
                    } ?>
                    <?php if($sertifikasi['status'] == 2){
                        echo '<a href="'.base_url('tuk/sertifikasi/'.$sertifikasi['id'].'/publish').'" class="btn btn-success" data-method="POST">PUBLISH</a>';
                        echo '<a href="'.base_url('tuk/sertifikasi/'.$sertifikasi['id'].'/cancel').'" class="btn btn-danger ml-2" data-method="POST">HAPUS</a>';
                    } ?>
                </div>
                <?php 
                
                $this->load->view('dashboard/sertifikasi/table-detailSertifikasi'); ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>