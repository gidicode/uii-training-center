<ul class="nav nav-tabs" id="myTab" role="tablist">
  <li class="nav-item">
    <a class="nav-link active" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Profile</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Konfirmasi Pembayaran</a>
  </li>
</ul>
<div class="tab-content" id="myTabContent">
  <div class="tab-pane fade show active" id="profile" role="tabpanel" aria-labelledby="profile-tab">
    <table class="table">
        <tr>
            <td>Nama Peserta</td>
            <td><?= $prt['participant']; ?></td>
        </tr>
        <tr>
            <td>Nama Kegiatan</td>
            <td><?= $prt['sertifikasi']; ?></td>
        </tr>
        <tr>
            <td>No Telepon</td>
            <td><?= $prt['phone']; ?></td>
        </tr>
        <tr>
            <td>Email</td>
            <td><?= $prt['email']; ?></td>
        </tr>
        <tr>
            <td>Institusi</td>
            <td><?= $prt['institusi']; ?></td>
        </tr>
        <tr>
            <td>Pekerjaan</td>
            <td><?= $prt['pekerjaan']; ?></td>
        </tr>
        <tr>
            <td>Alamat</td>
            <td style="width:70%"><?= $prt['address']; ?></td>
        </tr>
    </table>
  </div>
  <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
    <?php if($prt['payment'] == false){  ?>
        <div class="alert alert-danger mt-3">Belum ada konfirmasi pembayaran!</div>
    <?php } else { ?>
    <table class="table">
        <tr>
            <td style="width:30%">Invoice ID</td>
            <td><?= $prt['invoice_id'] ?></td>
        </tr>
        <tr>
            <td>Tanggal Transfer</td>
            <td><?= $prt['payment_date'] ?></td>
        </tr>
        <tr>
            <td>Pemilik Rekening</td>
            <td><?= $prt['from_name'] ?></td>
        </tr>
        <tr>
            <td>Bank Tujuan</td>
            <td><?= $prt['to_bank_name'] ?></td>
        </tr>
        <tr>
            <td>Jumlah Transfer</td>
            <td><?= int_to_rupiah($prt['cash_amount']) ?></td>
        </tr>
    </table>
    <?php } ?>
  </div>
</div>