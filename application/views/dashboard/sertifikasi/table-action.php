<?php
use \My\Enums\SertifikasiStatus as Status;

?>

<?php if($sertifikasi->status == '3'
        OR $sertifikasi->status == '4'
        OR $sertifikasi->status == '5'){ ?>
<a href="<?= base_url('tuk/sertifikasi/'.$sertifikasi->sertifikasi_id.'/manage'); ?>"
    class="btn btn-sm btn-link rounded-circle" data-toggle="tooltip" title="Kelola">
    <i class="fas fa-wrench"></i>
</a>
<?php } ?>



<?php if($sertifikasi->status == '1'
        OR $sertifikasi->status == '2'
        OR $sertifikasi->status == '3'){ ?>
<a href="<?= base_url('tuk/sertifikasi/'.$sertifikasi->sertifikasi_id.'/edit')  ?>"
    class="btn btn-sm btn-link rounded-circle" data-toggle="tooltip" title="Edit">
    <i class="fas fa-pen"></i>
</a>
<?php } ?>

<a href="<?= $sertifikasi->status != Status::DRAFT ? base_url('tuk/sertifikasi/'.$sertifikasi->sertifikasi_id) : '#' ?>"
    class="btn btn-sm btn-link rounded-circle"
    data-toggle="tooltip" title="Lihat">
    <i class="fas fa-eye"></i>
</a>

<a href="<?= $sertifikasi->status <= Status::DRAFT ? base_url('tuk/sertifikasi/'.$sertifikasi->sertifikasi_id.'/delete') : '#' ?>"
    class="btn btn-sm btn-link text-danger rounded-circle" data-method="POST" data-toggle="tooltip" title="Hapus">
    <i class="fas fa-trash"></i>
</a>

<a href="<?= $sertifikasi->status == Status::SUBMIT ? base_url('tuk/sertifikasi/'.$sertifikasi->sertifikasi_id.'/cancel') : '#' ?>"
    class="btn btn-sm btn-link text-danger rounded-circle" data-method="POST" data-toggle="tooltip" title="Batal">
    <i class="fas fa-times"></i>
</a>