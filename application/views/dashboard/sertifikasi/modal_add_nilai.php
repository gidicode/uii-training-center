<!-- Modal add -->
<?php if($trn->num_rows() != 0){?>
<div class="modal fade" id="modalNilai" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('dashboard/tuk/post_sertifikasi_nilai') ?>" method="post">
            <input type="hidden" name="submitNilai" value="submit">
            <input type="hidden" name="sertifikasi_id" value="<?= $prt->sertifikasi_id ?>">
            <input type="hidden" name="user_id" value="<?= $prt->user_id ?>">
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Nilai</label>
                    <input type="text" name="nilai" class="form-control" id="">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" id="" rows="5" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-primary">Simpan Nilai</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal edit -->
<?php foreach($trn->result()as $prt): ?>
<div class="modal fade" id="edit<?= $prt->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Nilai</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?php echo base_url('dashboard/tuk/post_sertifikasi_nilai') ?>" method="post">
            <input type="hidden" name="submitType" value="edit">
            <input type="hidden" name="sertifikasi_id" value="<?= $prt->sertifikasi_id ?>">
            <input type="hidden" name="user_id" value="<?= $prt->user_id ?>">
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Nilai</label>
                    <input type="text" name="nilai" class="form-control" id="" value="<?= $prt->nilai; ?>" />
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" id="" rows="5" class="form-control" ><?= $prt->keterangan ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-primary">Simpan Nilai</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach; ?>
<?php } ?>