<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | <?= $sertifikasi['name'] ?></title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebarTuk'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk') ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk/sertifikasi') ?>">Kelola Pelatihan</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk/sertifikasi/'.$sertifikasi['id'].'/manage') ?>"><?= $sertifikasi['id'].'. '. $sertifikasi['name'] ?></a>
            </li>
            <li class="breadcrumb-item">
                Pendaftar yang di-reject
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="m-3">
            <!-- Detail Sertifikasi -->
            <div class="card card-body my-2">
                <h1 style="font-weight:400"><?= $sertifikasi['name']; ?></h1>
                
                <p>
                <?= carbon($sertifikasi['start_time'])->isoFormat('DD MMMM YYYY') . ($sertifikasi['start_time'] == $sertifikasi['closed_at'] ? '' : ' - '. carbon($sertifikasi['closed_at'])->isoFormat('DD MMMM YYYY')) ?></p>
                
                <div class="mt-3"></div>
                <p><a href="<?= base_url('tuk/sertifikasi/'.$sertifikasi['id']) ?>/manage" class="mr-3"><i class="fas fa-arrow-left"></i> kembali</a>
                <?php if($sertifikasi['status'] == 4 OR $sertifikasi['status'] == 5) { ?>
                <span class="mr-3">||</span> Data Semua Peserta : <?= $sertifikasi['peserta']->num_rows(); ?></p>
                <?php } else { ?>
                <span class="mr-3">||</span> Jumlah peserta di-reject : <?= $sertifikasi['peserta']->num_rows(); ?></p>
                <?php } ?>
                <table class="table table-sm table-hover mt-3" id="datatable">
                    <thead>
                        <th>#</th>
                        <th>Nama</th>
                        <th>Institusi</th>
                        <th>Email</th>
                        <th>No Handphone</th>
                        <th>Status Bayar</th>
                        <th>Opsi</th>
                    </thead>
                    <tbody>
                        <?php foreach($sertifikasi['peserta']->result() as $i => $prt): ?>
                        <tr>
                            <td><?= $i+1 ?></td>
                            <td><?= $prt->username ?></td>
                            <td><?= $prt->institusi ?></td>
                            <td><?= $prt->email ?></td>
                            <td><?= $prt->phone ?></td>
                            <td>
                                <?php if($prt->is_paid == 0){ ?>
                                <span class='badge badge-warning'>pending</span>
                                <?php } ?>

                                <?php if($prt->is_paid == 1){ ?>
                                <span class='badge badge-success'>active</span>
                                <?php } ?>

                                <?php if($prt->is_paid == 3){ ?>
                                <span class='badge badge-danger'>rejected</span>
                                <?php } ?>
                            </td>
                            <td>
                                <?php 
                                $data['prt'] = $prt;
                                $data['id_sertifikasi'] = $sertifikasi['id'];
                                $this->load->view('dashboard/sertifikasi/table-actions-sertifikasi',$data); ?>
                            </td>
                        </tr>
                        <?php endforeach; ?>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>