<?php
use \My\Enums\ParticipantStatus as Status;

?>

<?php if($prt->is_paid == Status::PENDING){ ?>
<a href="<?= $prt->is_paid == Status::PENDING ? base_url('tuk/sertifikasi/'.$prt->id.'-'.$id_sertifikasi.'/participant-paid') : '#' ?>"
    class="btn btn-sm btn-link rounded-circle text-success" data-method="POST" data-toggle="tooltip" title="Ubah Status Bayar">
    <i class="fas fa-check"></i>
</a>

<?php } ?>

<?php if($prt->is_paid != Status::ACTIVE && $prt->is_paid != Status::REJECTED){ ?>
<a href="<?= base_url('tuk/sertifikasi/'.$prt->id.'-'.$id_sertifikasi.'/delete-participant') ?>"
    class="btn btn-sm btn-link rounded-circle text-danger" id="reject" data-toggle="tooltip" title="Reject">
    <i class="fas fa-times"></i>
</a>
<?php } ?>

<?php if($prt->is_paid == Status::REJECTED){ ?>
<a href="<?= base_url('tuk/sertifikasi/'.$prt->id.'-'.$id_sertifikasi.'/participant-unreject') ?>"
    class="btn btn-sm btn-link rounded-circle text-primary" id="reject" data-toggle="tooltip" title="unreject">
    <i class="fas fa-check"></i>
</a>
<?php } ?>