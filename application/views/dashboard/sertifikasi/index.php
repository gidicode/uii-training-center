<?php 
use \My\Enums\SertifikasiStatus; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title> Dashboard | Kelola Sertifikasi</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebarTuk'); ?>
    
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>

        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk') ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                Kelola Sertifikasi
            </li>
        </ol>
        <!-- END BREADCRUMBS -->

        <div class="m-3">
            <div class="d-flex">
                <a href="<?= base_url('tuk/sertifikasi/create') ?>" class="btn btn-primary mr-auto">
                    <i class="fas fa-plus"></i> Sertifikasi Baru
                </a>
                <!-- <a href="<?= dashboard_url('training/trash') ?>" class="btn btn-danger ml-auto">
                    <i class="fas fa-trash"></i> Trashed
                </a> -->
            </div>
            
            <div class="card my-2">
                <div class="card-body">
                    <table class="table mb-0" id="datatable">
                        <thead>
                            <tr class="border-top-0">
                                <th>ID </th>
                                <th>Uji Kompetensi</th>
                                <th>Lembaga</th>
                                <th>Status</th>
                                <th class="text-center">Pendaftar</th>
                                <th>Aksi</th>
                            </tr>
                        </thead>

                        <tbody>
                        <?php foreach ($certifications as $sertifikasi): ?>
                            <tr>
                                <td>
                                    <?= $sertifikasi->sertifikasi_id ?>
                                </td>
                                <td>
                                    <a href="
                                        <?php if($sertifikasi->status == '3'
                                                OR $sertifikasi->status == '4'
                                                OR $sertifikasi->status == '5'){
                                            echo base_url('tuk/sertifikasi/'.$sertifikasi->sertifikasi_id.'/manage');
                                        }else{
                                            echo base_url('tuk/sertifikasi/'.$sertifikasi->sertifikasi_id);
                                        } ?>
                                    " /><?= $sertifikasi->sertifikasi_name ?></a>
                                </td>
                                <td>
                                    <?= $sertifikasi->tuk_name ?>
                                </td>
                                <td>
                                    <?= SertifikasiStatus::render($sertifikasi->status) ?>
                                </td>
                                <td class="text-center">
                                    <?= $this->Sertifikasi_model->get_participant(array('sertifikasi_id' => $sertifikasi->sertifikasi_id))->num_rows(); ?>
                                </td>
                                <td class="btn-actions">
                                    <?php $this->load->view('dashboard/sertifikasi/table-action', ['sertifikasi' => $sertifikasi]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                          
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ' + $(this).data('original-title') + '?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>

</body>
</html>