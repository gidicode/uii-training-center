<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | Kelola Sertifikasi</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
    <link rel="stylesheet" href="<?= asset('css/jquery.fileuploader.css') ?>">
    <!-- Froala Editor -->
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />
    
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebarTuk'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>

        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk') ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= base_url('tuk/sertifikasi') ?>">Kelola Sertifikasi</a>
            </li>
            <li class="breadcrumb-item">
                Edit
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        
        <div class="m-3">
        <form action="<?= base_url('tuk/sertifikasi/create') ?>" method="POST" enctype="multipart/form-data">
                <input type="hidden" name="edit" value="edit">
                <input type="hidden" name="sertifikasi_id" value="<?= $sertifikasi['id']; ?>">
            <div class="d-flex">
                <h1 class="mr-auto h3">Edit Sertifikasi</h1>
                <div class="ml-auto">
                    <button name="submit" value="DRAFT" class="btn btn-secondary mx-1"><i class="fas fa-save"></i> Simpan</button>
                    <button name="submit" value="SUBMIT" class="btn btn-primary mx-1"><i class="fas fa-check-double"></i> Submit</button>
                </div>
            </div>
            
            <!-- Detail Sertifikasi -->
            <div class="card card-body my-2">
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label for="name">Nama Sertifikasi</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-font"></i></span>
                                </div>
                                <input name="name" class="form-control" placeholder="Nama Sertifikasi" value="<?= $sertifikasi['name'] ?>" autofocus>
                            </div>
                        </div>
                    </div>
                       
                </div>

                <div class="row">
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="start-time">Tanggal Sertifikasi</label>
                            <div class="input-group">
                                <input type="text" name="start_time" id="start-time" class="form-control" value="<?= carbon($sertifikasi['start_time'])->format('Y-m-d') ?>">
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="form-group">
                            <label for="lokasi">Lokasi Sertifikasi</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-map"></i></span>
                                </div>
                                <input id="location" type="text" name="location" class="form-control" value="<?= $sertifikasi['location'] ?>">
                            </div>
                        </div>
                    </div>
                </div>

                
                <div class="row">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label>Deskripsi</label>
                            <textarea name="description" class="form-control" id="editor" rows="8"><?= $sertifikasi['description'] ?>
                            </textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label for="closed-at">Batas Waktu Pendaftaran</label>
                            <div class="input-group">
                                <input type="date" value="<?= carbon($sertifikasi['closed_at'])->format('Y-m-d') ?>" name="closed_at" id="closed-at" class="form-control">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="cost">harga</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">Rp</span>
                                </div>
                                <input name="price" value="<?= int_to_rupiah($sertifikasi['price']) ?>" type="text" class="form-control mask-rupiah" placeholder="Rp 0,-">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quota">Batas Peserta</label>
                            <div class="input-group">
                                <div class="input-group-prepend">
                                    <span class="input-group-text"><i class="fas fa-users"></i></span>
                                </div>
                                <input name="quota" value="<?= $sertifikasi['quota'] ?>" type="number" class="form-control" placeholder="0">
                            </div>
                            <small class="form-text text-muted">kosongi atau isi dengan 0 untuk peserta tak terbatas</small>
                        </div>
                    </div>
                </div>
                
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Fasilitas</label>
                            <textarea name="fasilitas" class="form-control" id="editor1" rows="8"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Materi Pelatihan</label>
                            <textarea name="materi" class="form-control" id="editor2" rows="8"></textarea>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Informasi Pembayaran</label>
                            <textarea name="infoPembayaran" class="form-control" id="editor3" rows="8"><?= $sertifikasi['infoPembayaran'] ?></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label for="skema_id">Skema</label>
                            <div class="form-group">
                                <select name="skema_id" type="text" class="custom-select select-tags" style="border-top-left-radius: 0 border-bottom-left-radius: 0;" multiple>
                                <?php foreach ($sertifikasi['skemas'] as $skema): ?>
                                    <option value="<?= $skema->id ?>" selected><?= $skema->skema ?></option>
                                <?php endforeach; ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

             <!-- Gambar Sertifikasi -->
             <div class="row">
                <div class="col-md-6">
                    <h4 class="mt-3">Tambah Gambar</h4>
                    <div class="card card-body">
                        <input type="file" name="images" id="images">
                    </div>
                </div>
                
                <div class="col-md-6">
                    <h4 class="mt-3">Gambar yang tersimpan</h4>
                    <div class="card card-body">
                    <?php foreach ($sertifikasi['images']->result() as $image): ?>
                        <img src="<?= $image->filename ?>" alt="" class="img-thumbnail" width="150" height="100">
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
            
        </form>
        </div>
    </div>



    <?php $this->load->view('dashboard/parts/script'); ?>
    <?php // '<script src="https://maps.googleapis.com/maps/api/js?key=' .config('google.api_key') . '&libraries=places"></script>' ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBMYYRH4Fa_d23vk9SVMLxW_-2kazM2eUA&libraries=places"></script>
    <script>
      // This example adds a search box to a map, using the Google Place Autocomplete
      // feature. People can enter geographical searches. The search box will return a
      // pick list containing a mix of places and predicted search terms.

      // This example requires the Places library. Include the libraries=places
      // parameter when you first load the API. For example:
      // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

      

    </script>
    
    <script src="<?= asset('js/jquery.fileuploader.js') ?>"></script>
    
    <!-- Froala Editor -->
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/js/froala_editor.min.js'></script>
    
    <!-- Datepicker -->
    <script src="https://unpkg.com/gijgo@1.9.13/js/gijgo.min.js" type="text/javascript"></script>
    
    <script>
    $(function() {
        $('#editor').froalaEditor({
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '-', 'paragraphFormat', 'align'],
            height:200
        })
    });
    $(function() {
        $('#editor1').froalaEditor({
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '-', 'paragraphFormat', 'align'],
            height:200
        })
    });
    $(function() {
        $('#editor2').froalaEditor({
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '-', 'paragraphFormat', 'align'],
            height:200
        })
    });
    $(function() {
        $('#editor3').froalaEditor({
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '-', 'paragraphFormat', 'align'],
            height:200
        })
    });
    </script>
    
    <script>
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $('#start-time').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            format: 'yyyy-mm-dd',
            minDate: today,
            maxDate: function () {
                return $('#finish-time').val();
            }
        });
        $('#finish-time').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            format: 'yyyy-mm-dd',
            minDate: function () {
                return $('#start-time').val();
            }
        });

        $('#closed-at').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            format: 'yyyy-mm-dd',
            minDate: today,
            maxDate: function () {
                return $('#start-time').val();
            }
        });

        $(document).ready(function () {
            function initAutocomplete() {
                var input = document.getElementById('location');
                autocomplete = new google.maps.places.Autocomplete(input);
            }
            $('.select-tags').on('select2:close', function () {
                var select2SearchField = $(this).parent().find('.select2-search__field'),
                    setFocus = setTimeout(function () {
                        select2SearchField.focus();
                    }, 100);
            });
            $('.select-tags').select2({
                placeholder: "Pilih Skema",
                tags: true,
                tokenSeparators: [','],
                createTag: function (params) {
                    if (/^\d+$/.test(params.term) || params.term.includes(',')) {
                        return null;
                    }
                    var term = params.term.toLowerCase();
                    return {
                        id: term,
                        text: term,
                        newTag: true,
                    };
                }
            })
            $('#images').fileuploader({
                addMore: true
            });
        });
    </script>
</body>
</html>