<!-- modal -->
<div id="fsModal"
     class="modal modal-full"
     tabindex="-1"
     role="dialog"
     aria-labelledby="myModalLabel"
     aria-hidden="true">

  <!-- dialog -->
  <div class="modal-dialog modal-dialog-full">

    <!-- content -->
    <div class="modal-content modal-content-full">

      <!-- header -->
      <div class="modal-header modal-header-full">
        <h1 id="myModalLabel"
            class="modal-title modal-title-full">
          Tambah Update
        </h1>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <!-- header -->
      
      <!-- body -->
      <div class="modal-body modal-body-full">
        <div class="container mt-3">
            <form action="<?= base_url('dashboard/training/postUpdate') ?>" method="post">
            <input type="hidden" name="training_id" value="<?= $training_id ?>">
            <div class="form-group">
                <label for="">Judul</label>
                <input type="text" name="title" class="form-control">
            </div>
            <div class="form-group">
                <label>Notes:</label>
                <textarea name="note" class="form-control" id="editor"></textarea>
            </div>
        </div>
      </div>
      <!-- body -->

      <!-- footer -->
      <div class="modal-footer modal-footer-full">
        <button class="btn btn-secondary"
                data-dismiss="modal">
          close
        </button>
        <button class="btn btn-success" type="submit">
          PUBLISH
        </button>
        </form>
      </div>
      <!-- footer -->

    </div>
    <!-- content -->

  </div>
  <!-- dialog -->

</div>
<!-- modal -->