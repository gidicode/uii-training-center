<?php if($training['status'] == 3){
    echo '
    <small class="mb-1">
        <i class="fas fa-exclamation-circle"></i>
        Status: Pendaftaran dibuka
    </small><br>
    ';
    echo '
    <form action="'.base_url('dashboard/training/changestatus').'" method="post">
    <input type="hidden" name="training_id" value="'.$training['id'].'">
    <input type="hidden" name="status" value="4">
    <button class="btn btn-warning" type="submit" onClick="return konfirmasi();">Tutup Pendaftaran</button>
    </form>';
} ?>
<?php if($training['status'] == 4){
    echo '
    <small class="mb-1">
        <i class="fas fa-exclamation-circle"></i>
        Status: Aktif
    </small><br>
    ';
    echo '
    <div style="display:inline-flex">
    <form action="'.base_url('dashboard/training/changestatus').'" method="post" class="mr-2">
    <input type="hidden" name="training_id" value="'.$training['id'].'">
    <input type="hidden" name="status" value="3">
    <button class="btn btn-primary" type="submit" onClick="return konfirmasi();">Buka Pendaftaran</button>
    </form>';
    echo '
    <form action="'.base_url('dashboard/training/changestatus').'" method="post">
    <input type="hidden" name="training_id" value="'.$training['id'].'">
    <input type="hidden" name="status" value="5">
    <button class="btn btn-warning" type="submit" onClick="return konfirmasi();">FINISH</button>
    </form>
    </div>';
} ?>
<?php if($training['status'] == 2){
    echo '<a href="'.base_url('admin/training/'.$training['id'].'/publish').'" class="btn btn-success" data-method="POST">PUBLISH</a>';
    echo '<a href="'.base_url('admin/training/'.$training['id'].'/cancel').'" class="btn btn-danger ml-2" data-method="POST">HAPUS</a>';
} ?>