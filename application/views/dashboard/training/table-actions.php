<?php
use \My\Enums\TrainingStatus as Status;

?>

<?php if($training->training_status == '3'
        OR $training->training_status == '4'
        OR $training->training_status == '5'){ ?>
<a href="<?= dashboard_url('training/'.$training->id_training.'/manage'); ?>"
    class="btn btn-sm btn-link rounded-circle" data-toggle="tooltip" title="Kelola">
    <i class="fas fa-wrench"></i>
</a>
<?php } ?>

<?php if($training->training_status == '2' && auth()->level == 'admin'){ ?>
    <a href="<?= dashboard_url('training/'.$training->id_training.'/publish') ?>"
        class="btn btn-sm btn-link text-success rounded-circle"
        data-toggle="tooltip" title="Publish">
        <i class="fas fa-check"></i>
    </a>
<?php } ?>

<?php if($training->training_status == '3' && auth()->level == 'admin'){ ?>
    <a href="<?= dashboard_url('training/'.$training->id_training.'/unpublish') ?>"
        class="btn btn-sm btn-link text-danger rounded-circle"
        data-toggle="tooltip" title="unpublish">
        <i class="fas fa-times"></i>
    </a>
<?php } ?>

<?php if($training->training_status == '1'
        OR $training->training_status == '2'
        OR $training->training_status == '3'){ ?>
<a href="<?= dashboard_url('training/'.$training->id_training.'/edit')  ?>"
    class="btn btn-sm btn-link rounded-circle" data-toggle="tooltip" title="Edit">
    <i class="fas fa-pen"></i>
</a>
<?php } ?>

<a href="<?= $training->training_status != Status::DRAFT ? dashboard_url('training/'.$training->id_training) : '#' ?>"
    class="btn btn-sm btn-link rounded-circle"
    data-toggle="tooltip" title="Lihat">
    <i class="fas fa-eye"></i>
</a>

<a href="<?= $training->training_status <= Status::DRAFT ? dashboard_url('training/'.$training->id_training.'/delete') : '#' ?>"
    class="btn btn-sm btn-link text-danger rounded-circle" data-method="POST" data-toggle="tooltip" title="Hapus">
    <i class="fas fa-trash"></i>
</a>

<a href="<?= $training->training_status == Status::SUBMIT ? dashboard_url('training/'.$training->id_training.'/cancel') : '#' ?>"
    class="btn btn-sm btn-link text-danger rounded-circle" data-method="POST" data-toggle="tooltip" title="Batal">
    <i class="fas fa-times"></i>
</a>