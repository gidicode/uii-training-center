<table class="table table-sm table-hover mt-3">
    <tr>
        <th class="w-25">Status</th><td><?= \My\Enums\TrainingStatus::render($training['status']) ?></td>
    </tr>
    <tr>
        <th>Nama</th><td><?= $training['name'] ?></td>
    </tr>
    <tr>
        <th>Tanggal Pelatihan</th>
        <td><?= carbon($training['start_time'])->isoFormat('DD MMMM YYYY') . ($training['start_time'] == $training['finish_time'] ? '' : ' - '. carbon($training['finish_time'])->isoFormat('DD MMMM YYYY')) ?></td>
    </tr>
    <tr>
        <th>Pelatihan</th>
        <td><?= $training['location'] ?></td>
    </tr>
    <tr>
        <th>Deskripsi</th>
        <td><?= $training['description'] ?></td>
    </tr>
    <tr>
        <th>Materi</th>
        <td><?= $training['materi'] ?></td>
    </tr>
    <tr>
        <th>Fasilitas</th>
        <td><?= $training['fasilitas'] ?></td>
    </tr>
    <tr>
        <th>Batas Waktu Pendaftaran</th>
        <td><?= carbon($training['closed_at'])->isoFormat('DD MMMM YYYY') ?></td>
    </tr>
    <tr>
        <th>Kuota Peserta</th>
        <td><?= $training['quota'] ?></td>
    </tr>
    <tr>
        <th>Harga</th>
        <td><?= int_to_rupiah($training['price']) ?></td>
    </tr>
    <tr>
        <th>Info Pembayaran</th>
        <td><?= $training['infoPembayaran'] ?></td>
    </tr>
    <tr>
        <th>Tags</th>
        <td>
            <?= 
                implode(', ', array_map(function ($item) {
                    return $item['name'];
                }, $training['tags'])) 
            ?>
        </td>
    </tr>
    <tr>
        <th>Gambar</th>
        <td>
            <?php foreach ($training['images'] as $image): ?>
                <img src="<?= $image['filename'] ?>" alt="" class="img-thumbnail" width="100" height="100">
            <?php endforeach; ?>
        </td>
    </tr>
</table>