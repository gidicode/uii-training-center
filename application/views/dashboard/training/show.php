<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('dashboard/parts/head'); ?>
    <title>Dashboard | <?= $training['name'] ?> </title>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url() ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url('training') ?>">Kelola Pelatihan</a>
            </li>
            <li class="breadcrumb-item">
                <?= $training['id'].'. '. $training['name'] ?>
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="m-3">
            <!-- Detail Pelatihan -->
            <div class="card card-body my-2">
                <div class="text-right">
                    <?php if($training['status'] == 3){
                        echo '<a href="'.base_url('admin/training/'.$training['id'].'/unpublish').'" class="btn btn-warning" data-method="POST">UNPUBLISH</a>';
                    } ?>
                    <?php if($training['status'] == 2){
                        echo '<a href="'.base_url('admin/training/'.$training['id'].'/publish').'" class="btn btn-success" data-method="POST">PUBLISH</a>';
                        echo '<a href="'.base_url('admin/training/'.$training['id'].'/cancel').'" class="btn btn-danger ml-2" data-method="POST">HAPUS</a>';
                    } ?>
                </div>
                <?php 
                $data['training'] = $training;
                $this->load->view('dashboard/training/tableDetailPelatihan',$data); ?>
            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>