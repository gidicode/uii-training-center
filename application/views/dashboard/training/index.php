<?php 
use \My\Enums\TrainingStatus; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | Kelola Pelatihan</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url() ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                Kelola Pelatihan
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="m-3">
            <div class="d-flex">
                <a href="<?= dashboard_url('training/create') ?>" class="btn btn-primary mr-auto">
                    <i class="fas fa-plus"></i> Pelatihan Baru
                </a>
                <!-- <a href="<?= dashboard_url('training/trash') ?>" class="btn btn-danger ml-auto">
                    <i class="fas fa-trash"></i> Trashed
                </a> -->
            </div>
            
            <div class="card my-2">
                <div class="card-body">
                    <table class="table mb-0" id="datatable">
                        <thead>
                            <tr class="border-top-0">
                                <th>ID </th><th>Nama</th><th>Lembaga</th><th>Status</th><th class="text-center">Pendaftar</th><th>Aksi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($trainings as $training): ?>
                            <tr>
                                <td><?= $training->id_training ?></td>
                                <td>
                                    <a href="
                                        <?php if($training->training_status == '3'
                                                OR $training->training_status == '4'
                                                OR $training->training_status == '5'){
                                            echo base_url('admin/training/'.$training->id_training.'/manage');
                                        }else{
                                            echo base_url('admin/training/'.$training->id_training);
                                        } ?>
                                    " /><?= $training->training_name ?></a>
                                </td>
                                <td><?= $training->org_name ?></td>
                                <td>
                                    <?= TrainingStatus::render($training->training_status) ?>
                                </td>
                                <td class="text-center">
                                    <?= $this->training_model->get_participant(array('training_id' => $training->id_training))->num_rows(); ?>
                                </td>
                                <td class="btn-actions">
                                    <?php $this->load->view('dashboard/training/table-actions', ['training' => $training]) ?>
                                </td>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ' + $(this).data('original-title') + '?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>