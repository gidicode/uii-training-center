<form action="<?= base_url('dashboard/training/postUpdate') ?>" method="post">
<!-- body -->
<div class="modal-body modal-body-full">
    <div class="container mt-3">
        <input type="hidden" name="training_id" value="<?= $upt['training_id'] ?>">
        <input type="hidden" name="ket" value="<?= $upt['ket'] ?>">
        <input type="hidden" name="id" value="<?= $upt['id'] ?>">
        <div class="form-group">
            <label for="">Judul</label>
            <input type="text" name="title" class="form-control" value="<?= $upt['title'] ?>">
        </div>
        <div class="form-group">
            <label>Notes:</label>
            <textarea name="note" class="form-control" id="editor"><?= $upt['note'] ?></textarea>
        </div>
    </div>
</div>
<!-- body -->

<!-- footer -->
<div class="modal-footer modal-footer-full">
    <button class="btn btn-secondary"
            data-dismiss="modal">
        close
    </button>
    <button class="btn btn-success" type="submit">
        PUBLISH
    </button>
</div>
<!-- footer -->

</form>