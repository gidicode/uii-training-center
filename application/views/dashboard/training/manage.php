<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | <?= $training['name'] ?></title>
    <?php $this->load->view('dashboard/parts/head'); ?>
    
    <!-- Froala Editor -->
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url() ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url('training') ?>">Kelola Pelatihan</a>
            </li>
            <li class="breadcrumb-item">
                <?= $training['id'].'. '. $training['name'] ?>
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="m-3">
            <!-- Detail Pelatihan -->
            <div class="card card-body my-2">
                
                <div class="row">
                    <div class="col-md-8">
                        <h1 style="font-weight:400"><?= $training['name']; ?>
                        </h1>
                        <p>
                        <?php if(auth()->level == 'admin'){
                            echo '<strong>'.$training['org_name'].'</strong>, ';
                        } ?>
                        <?= carbon($training['start_time'])->isoFormat('DD MMMM YYYY') . ($training['start_time'] == $training['finish_time'] ? '' : ' - '. carbon($training['finish_time'])->isoFormat('DD MMMM YYYY')) ?></p>
                    </div>
                    <div class="col-md-4 text-right">
                        <?php $this->load->view('dashboard/training/button_status_manage'); ?>
                    </div>
                </div>
                
                <div class="mt-3"></div>
                <nav>
                    <div class="nav nav-tabs" id="nav-tab" role="tablist">
                        <?php if($training['status'] == 3){ ?>
                        <a class="nav-item nav-link active" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Peserta</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Detail</a>
                        
                        <?php }else{ ?>
                        <a class="nav-item nav-link active" id="nav-update-tab" data-toggle="tab" href="#nav-update" role="tab" aria-controls="nav-update" aria-selected="true">Update</a>
                        <a class="nav-item nav-link" id="nav-home-tab" data-toggle="tab" href="#nav-home" role="tab" aria-controls="nav-home" aria-selected="true">Peserta</a>
                        <a class="nav-item nav-link" id="nav-profile-tab" data-toggle="tab" href="#nav-profile" role="tab" aria-controls="nav-profile" aria-selected="false">Detail</a>
                        <?php } ?>
                    </div>
                </nav>

                <div class="tab-content" id="nav-tabContent">
                    
                    <!-- Jika status training adalah ONGOING atau Finish -->
                    <?php if($training['status'] != 3){ ?>
                    <div class="tab-pane fade show active" id="nav-update" role="tabpanel" aria-labelledby="nav-update-tab">
                        <div class="mt-3"></div>
                        <button class="btn btn-primary btn-modal" id="editUpdate" id="editUpdate" data-id="<?= 0 ?>" data-ket="<?= 'add' ?>" data-training="<?= $training['training_id'] ?>" />
                        Tambah Update
                        </button>
                        <table class="table table-hover mt-3">
                            <tbody>
                                <?php 
                                if($training['training_update']->num_rows() == 0){
                                    echo "<br><div class='mt-3'></div><i> Belum ada update, silahkan tambah update!</i>";
                                }else{
                                foreach($training['training_update']->result() as $i => $upt): ?>
                                <div class="card mt-3">
                                    <div class="card-header">
                                        <div class="float-left">
                                            <h5 class="mb-0"><?= $upt->title ?></h5>
                                            <small><?= carbon($upt->last_update)->isoFormat('DD MMMM YYYY - H:m') ?></small>
                                        </div>
                                        
                                        <div class="float-right">
                                            <div class="btn-group dropleft">
                                                <a href="#" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                    <i class="fas fa-ellipsis-v"></i>
                                                </a>
                                                <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                                                    <a class="dropdown-item" href="<?= dashboard_url('training/'.$upt->uniqID.'-'.$training['training_id'].'/delete-update') ?>" onClick="return konfirmasi();" />Hapus</a>
                                                    <a class="dropdown-item" href="#" data-toggle="modal" id="editUpdate" data-id="<?= $upt->uniqID ?>" data-ket="<?= 'edit' ?>" data-training="<?= $training['training_id'] ?>" />Edit</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="card-body">
                                        <?= $upt->note ?>
                                    </div>
                                </div>
                                <?php endforeach; } ?>
                            </tbody>
                        </table>
                    </div>
                    <?php } ?>
                    <!-- End of section -->
                    
                    <!-- Jika status training adalah PUBLISHED -->
                    <div class="tab-pane fade <?php if($training['status'] == 3){ echo "show active"; } ?>" id="nav-home" role="tabpanel" aria-labelledby="nav-home-tab">
                        <div class="mt-3"></div>
                        <?php if($training['status'] == 4 OR $training['status'] == 5){ 
                            $trn = $training['peserta_paid'];
                        } 
                        
                        else {
                            $trn = $training['peserta'];
                        } ?>
                        <div style="display: flow-root">
                        <p class="float-left">Jumlah peserta: <?= $trn->num_rows(); ?></p>
                        <?php if($training['peserta_reject']->num_rows() != 0){ ?>
                        <a href="<?= dashboard_url('training/'.$training['id']) ?>/rejected-participant" class="float-right"><i class="fas fa-exclamation-circle"></i> Lihat data yang di-reject</a>
                        <?php } if($training['status'] == 4 OR $training['status'] == 5) { ?>
                        <a href="<?= dashboard_url('training/'.$training['id']) ?>/all-participant" class="float-right"><i class="fas fa-exclamation-circle"></i> Lihat semua data peserta</a>
                        <?php } ?>
                        </div>
                        <table class="table table-hover table-sm mt-3" id="datatable">
                            <thead>
                                <th>#</th>
                                <th>Nama</th>
                                <th>Institusi</th>
                                <th>Email</th>
                                <th>No Handphone</th>
                                <th>Status Bayar</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                <?php 
                                foreach($trn->result() as $i => $prt):  ?>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td><?= $prt->username ?></td>
                                    <td><?= $prt->institusi ?></td>
                                    <td><?= $prt->email ?></td>
                                    <td><?= $prt->phone ?></td>
                                    <td>
                                        <?php 
                                        if($prt->is_paid == 0){ echo "<span class='badge badge-danger'>belum bayar</span>"; } 
                                        elseif($prt->is_paid != 3) { echo "<span class='badge badge-success'>sudah bayar</span>"; }
                                        ?>
                                    </td>
                                    <td>
                                        <?php 
                                        $data['prt'] = $prt;
                                        $data['id_training'] = $training['id'];
                                        $this->load->view('dashboard/training/table-actions-training',$data); ?>
                                        <a href="#"
                                            class="btn btn-sm btn-primary" id="showData" data-toggle="tooltip" 
                                            title="lihat detail" data-id="<?= $prt->id ?>">
                                            lihat
                                        </a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <div class="tab-pane fade" id="nav-profile" role="tabpanel" aria-labelledby="nav-profile-tab">
                        <?php 
                        $data['training'] = $training;
                        $this->load->view('dashboard/training/tableDetailPelatihan',$data); ?>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel" style="font-weight:500">Detail Peserta<h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal Edit Update -->
    <!-- modal -->
    <div id="modalEdit"
        class="modal modal-full"
        tabindex="-1"
        role="dialog"
        aria-labelledby="myModalLabel"
        aria-hidden="true">

    <!-- dialog -->
    <div class="modal-dialog modal-dialog-full">

        <!-- content -->
        <div class="modal-content modal-content-full">

            <!-- header -->
            <div class="modal-header modal-header-full">
                <h3 id="myModalLabel"
                    class="modal-title modal-title-full">
                Tambah / Edit Update
                </h3>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <!-- header -->
            
            <!-- body -->
            <div class="html-body">
            </div>
        </div>
    </div>
        

    <?php $this->load->view('dashboard/parts/script'); ?>
    
    <!-- Froala Editor -->
    <script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/js/froala_editor.min.js'></script>
    
    <script>

    $(function() {
        $('#editor').froalaEditor({
            toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '-', 'paragraphFormat', 'align'],
            height:350
        })
    });

    function konfirmasi()
    {
        if (confirm('Apakah anda yakin ingin melakukan aksi ini?')) {
            return true;
        } else {
            return false;
        }
    }

    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin merubah status pembayaran ? Harap berhati-hati, Aksi ini tidak dapat di kembalikan')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });

    $(document).ready(function () {
        $('#reject').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin ingin mereject peserta ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });

    $(function(){
        $(document).on('click','#showData',function(e){
            e.preventDefault();
            $("#myModal").modal('show');
            $.post('<?= base_url('dashboard/training/detailparticipant') ?>',
                {id:$(this).attr('data-id')},
                function(html){
                    $(".modal-body").html(html);
                }
            );
        });
    });

    $(function(){
        $(document).on('click','#editUpdate',function(e){
            e.preventDefault();
            $("#modalEdit").modal('show');
            $.post('<?= base_url('dashboard/training/editUpdate') ?>',
                {
                    id:$(this).attr('data-id'), 
                    training:$(this).attr('data-training'),
                    ket:$(this).attr('data-ket')
                },
                function(html){
                    $(".html-body").html(html);
                }
            );
        });
    });
    
    </script>
</body>
</html>