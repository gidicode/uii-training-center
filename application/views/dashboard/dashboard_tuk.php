<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard TUK</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebarTuk'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                Dashboard
            </li>
        </ol>
        <!-- END BREADCRUMBS -->

        <div class="row m-0">
            <div class="col-md-9">
                <h1 class="h5 mt-3" style="font-weight:400">Welcome,</h1>
                <h2 class="h2" style="font-weight:400"> <?= auth()->nama_tuk ?></h2>
            </div>
            <div class="col-md-4">
                <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers">
                        12
                        <small>/412</small>
                    </span>
                    <span class="count-name">Jumlah Pendaftar </span>
                </div>
            </div>

        </div>
        
        <hr>
        <div class="row m-0 mt-3">
            <div class="col-md-12 mt-3 mb-5">
                <?php if($ifskema == 0): ?>
                    <div class="alert-2 alert-danger"><i class="fas fa-exclamation-circle mr-2"></i> 
                        Belum ada skema, silahkan tambah skema <a href="#">disini</a>
                    </div>
                <?php endif; ?>
            </div>
            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Pembayaran
                    </div>
                    <div class="card-body">

                    </div>
                </div>
            </div>

            <div class="col-md-6">
                <div class="card">
                    <div class="card-header">
                        Pendaftar
                    </div>
                    <div class="card-body">
                    
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <hr>
            </div>
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        Skema
                        <a href="" class="btn btn-primary btn-sm float-right" data-toggle="modal" data-target="#exampleModal">tambah skema</a>
                    </div>
                    <div class="card-body">
                        <table class="table">
                            <thead>
                                <th>#</th>
                                <th>Skema</th>
                                <th>Jumlah Sertifikasi</th>
                                <th>Opsi</th>
                            </thead>
                            <tbody>
                                <?php foreach($skema->result() as $i => $s): ?>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td><?= $s->skema ?></td>
                                    <td><?= $s->jumlah?></td>
                                    <td>
                                        <a href="<?= base_url('tuk/sertifikasi/skema/'.$s->id) ?>" class="btn btn-primary btn-sm">detail</a>
                                        <a href="" data-toggle="modal" data-target="#edit<?= $s->id ?>" class="btn btn-sm btn-success">edit</a>
                                        <a href="<?= base_url('dashboard/tuk/deleteSkema/'.$s->id) ?>" class="btn btn-danger btn-sm" onClick="return konfirmasi()">hapus</a>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>


    </div>

    <?php 
    
    $data['skema'] = $skema;
    $this->load->view('dashboard/organization/part_skema',$data); ?>
    
    <?php $this->load->view('dashboard/parts/script'); ?>
<script>    
    function konfirmasi()
    {
        if (confirm('Apakah anda yakin ingin melakukan aksi ini?')) {
            return true;
        } else {
            return false;
        }
    }
</script>
</body>
</html>