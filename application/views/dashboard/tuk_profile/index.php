<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | Kelola Profile</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
    <link rel="stylesheet" href="<?= asset('css/jquery.fileuploader.css') ?>">
    <!-- Froala Editor -->
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
    <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />
    
    <!-- Datepicker -->
    <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />

    <style>
            #hide input[type=file] {
            display:none;
            margin:10px;
            }
            #hide input[type=file] + label {
            display:inline-block;
            margin:20px;
            padding: 4px 32px;
            background-color: #FFFFFF;
            border:solid 1px #666F77;
            border-radius: 6px;
            color:#666F77;
            }
            #hide input[type=file]:active + label {
            background-image: none;
            background-color:#2D6C7A;
            color:#FFFFFF;
            }
    </style>

</head>

<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebarTuk'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>

        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url() ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url('training') ?>">Kelola Profile</a>
            </li>
        </ol>
        <!-- END BREADCRUMBS -->

        <!-- START CONTENT -->
        <div class="container mt-5 mb-5">
            <h3>Registrasi Tempat Uji Kompetensi (TUK)</h3><hr>
               
                <div class="row">

                    <div class="col-md-3">
                        <div style="height: 250px;">
                            <img id="output" style="width:100%;" src="<?= $profile['logo'] ?>" />
                            <input type="hidden" name="avatar_google" >
                        </div>
                        <hr>
                        <div class="fileUpload btn btn-primary">
                            <span>Ganti Logo Organisasi</span>
                            <input id="hide" type="file" accept="image/*" onchange="loadFile(event)" name="avatar" class="upload">
                        </div>
                    </div>

                        

                    <div class="col-md-9">
                        <div class="form-group row">
                            <label for="staticEmail" class="col-sm-3 col-form-label">Email</label>
                            <div class="col-sm-9">
                            <input type="text" name="email" readonly class="form-control-plaintext" id="staticEmail" value="<?= $profile['email'] ?>" />
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama TUK</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_tuk" name="name" placeholder="Organisasi" value="<?= $profile['nama_tuk'] ?>" required>
                            </div>
                        </div>
                        
                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama Laboratorium</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_laboratorium" placeholder="Nama Laboratorium" value="<?= $profile['nama_laboratorium'] ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="name" class="col-sm-3 col-form-label">Nama Kepala Laboratorium</label>
                            <div class="col-sm-9">
                            <input type="text" class="form-control" name="nama_kepala_laboratorium" placeholder="Nama Kepala Laboratorium" value="<?= $profile['nama_kepala_laboratorium'] ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-3 col-form-label">Alamat</label>
                            <div class="col-sm-9">
                            <textarea name="alamat" class="form-control" id="alamat" cols="30" rows="3" required> <?= $profile['deskripsi'] ?> </textarea>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="phone" class="col-sm-3 col-form-label">No Telephon</label>
                            <div class="col-sm-9">
                            <input type="number" name="phone" class="form-control" id="phone"  value="<?= $profile['phone'] ?>" required>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="deskripsi" class="col-sm-3 col-form-label">Deskripsi</label>
                            <div class="col-sm-9">
                            <textarea name="deskripsi" class="form-control" id="deskripsi" cols="30" rows="10" required> <?= $profile['deskripsi'] ?> </textarea>
                            </div>
                        </div>

                        <div class="float-right">
                            <button type="submit" class="btn btn-primary">Lanjutkan</button>
                        </div>

                    </div>
                </div>
            </form>
        </div>
        <!-- END CONTENT -->

    </div>
</body>

</html>
        