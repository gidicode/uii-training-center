<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="<?= asset('vendor/bootstrap/js/bootstrap.js') ?>"></script>
<script src="<?= asset('js/select2.js') ?>"></script>
<script src="<?= asset('js/jquery.mask.min.js') ?>"></script>

<!-- DataTable JS -->
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>

<!-- Sweet alert -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.15.3/dist/sweetalert2.js"></script>



<script>
    $(document).ready(function () {
        $('.mask-rupiah').mask('#.##0,-', { reverse: true });
        $('.sidebar-wrapper .list-group a').each(function (i, element) {
            var url = $(element).attr('href');
            if ($(element).data('exact')) {
                if (url == window.location.href) {
                    $(element).addClass('active');
                }
            } else {
                if (window.location.href.startsWith(url)) {
                    $(element).addClass('active');
                }
            }
        });
        $('.sidebar-toggler').click(function () {
            $('.sidebar-wrapper, .sidebar-toggler').toggleClass('active');
        });
        $('[data-toggle="tooltip"]').tooltip();
    });

    $(document).ready(function() {
        $('#datatable').DataTable({
            
        });
    } );
</script>

