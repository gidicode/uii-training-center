
<div class="sidebar-wrapper bg-primary">
    <div class="sidebar-brand p-1" style="height:70px;">
        <img height="100%" src="<?= asset('img/dashboard-uii.png') ?>">
    </div>

    <div class="list-group">
        <!-- DASHBOARD -->
        <a href="<?= dashboard_url() ?>" class="list-group-item list-group-item-action" data-exact="true">
            <i class="fas fa-chart-line"></i>
            Dashboard
        </a>

        <!-- KELOLA ORGANISASI -->
        <?php if (auth()->level == 'admin'): ?>
        <a href="<?= dashboard_url('organization') ?>" class="list-group-item list-group-item-action">
            <i class="fas fa-city"></i>
            Kelola Organisasi
        </a>
        <?php endif; ?>

        <!-- Kelola Pelatihan -->
        <a href="<?= dashboard_url('training') ?>" class="list-group-item list-group-item-action">
            <i class="fas fa-chalkboard-teacher"></i>
            Kelola Pelatihan
        </a>

        <!-- Kelola Pembayaran -->
        <a href="<?= dashboard_url('pembayaran') ?>" class="list-group-item list-group-item-action">
            <i class="far fa-money-bill-alt"></i>
            Kelola Pembayaran
        </a>

        <!-- KELOLA SUPER USERS -->
        <?php if (auth()->level == 'admin'): ?>
        <hr>
        <a href="<?= dashboard_url('users') ?>" class="list-group-item list-group-item-action">
            <i class="fas fa-users"></i>
            Kelola User
        </a>
        <?php endif; ?>
    </div>
</div>