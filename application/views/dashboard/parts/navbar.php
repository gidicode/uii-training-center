<nav class="navbar navbar-expand-md navbar-light bg-secondary" style="background:#f6d300; height:70px">
    <a class="navbar-brand" href="<?= base_url('/dashboard') ?>">Dashboard</a>
    <div class="sidebar-toggler d-md-none" aria-label="Toggle navigation" style="z-index: 1004">
    </div>

    <div class="collapse navbar-collapse" id="navbarSupportedContent">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="navbar-nav ml-auto">
            <li class="nav-item dropdown">
                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                    <?= auth()->name ?? auth()->nama_tuk ?>
                    <img class="ml-1 rounded-circle m-0" style="height: 38px; width: 38px; margin-top: -0.5rem; margin-bottom: -0.5rem;" src="<?= auth()->logo ?>" alt="user_avatar">
                    <span class="caret"></span>
                </a>

                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="<?= base_url('auth/logout') ?>"
                        onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                        Log out
                    </a>

                    <form id="logout-form" action="<?= base_url('auth/logout') ?>" method="POST" style="display: none;">
                    </form>
                </div>
            </li>
        </ul>
    </div>
</nav>

<?php $this->load->view('parts/alert'); ?>
