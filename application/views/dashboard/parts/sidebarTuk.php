
<div class="sidebar-wrapper bg-primary">
    <div class="sidebar-brand p-1" style="height:70px;">
        <img height="100%" src="<?= asset('img/dashboard-uii.png') ?>">
    </div>

    <div class="list-group mt-3">
        <!-- DASHBOARD -->
        <a href="<?= base_url('tuk') ?>" class="list-group-item list-group-item-action" data-exact="true">
            <i class="fas fa-chart-line"></i>
            Dashboard
        </a>

        <!-- Kelola Profil -->
        <a href="<?= base_url('tuk/profile') ?>" class="list-group-item list-group-item-action">
            <i class="fas fa-chalkboard-teacher"></i>
            Kelola Profil
        </a>

        <!-- Kelola Uji Kompetensi -->
        <a href="<?= base_url('tuk/sertifikasi') ?>" class="list-group-item list-group-item-action">
            <i class="fas fa-award"></i>
            Kelola Sertifikasi
        </a>

        <!-- Kelola Pembayaran -->
        <a href="<?= base_url('tuk/pembayaran') ?>" class="list-group-item list-group-item-action">
            <i class="far fa-money-bill-alt"></i>
            Kelola Pembayaran
        </a>

    </div>
</div>