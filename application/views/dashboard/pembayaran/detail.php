<input type="hidden" name="idparticipant" value="<?= $inv['id_participant'] ?>">
<h3>#<?= $inv['invoice_id'] ?></h3>
<table class="table">
    <tr>
        <td>Invoice ID</td>
        <td><?= $inv['invoice_id'] ?></td>
    </tr>
    <tr>
        <td>Nama Peserta</td>
        <td><?= $inv['participant'] ?></td>
    </tr>
    <tr>
        <td>Kegiatan</td>
        <td><?= $inv['training'] ?></td>
    </tr>
    <tr>
        <td>Rekening Tujuan</td>
        <td><?= $inv['to_bank_name'] ?></td>
    </tr>
    <tr>
        <td>Pengirim/Pemilik Rekening</td>
        <td><?= $inv['from_name'] ?></td>
    </tr>
    <tr>
        <td>Nominal</td>
        <td><?= int_to_rupiah($inv['cash_amount']) ?></td>
    </tr>
</table>