<?php 
use \My\Enums\TrainingStatus; 
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard | Kelola Pelatihan</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url() ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                Kelola Pembayaran
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="m-3">
            <?php if($this->uri->segment(3) == 'all'){ ?>
            <a href="<?= dashboard_url('pembayaran') ?>" class="btn btn-default"><i class="fas fa-arrow-left mr-3"></i> Kembali</a>
            
            <p class="text-right"><i class="fas fa-exclamation-circle"></i> Menampilkan semua data konfirmasi pembayaran</p>
            <?php } else { ?>
            <a href="<?= dashboard_url('pembayaran/all') ?>" class="btn btn-primary">Lihat semua konfirmasi</a>
            <?php } ?>
            <div class="card my-2">
                <div class="card-body">
                    <table class="table" id="datatable" style="width:100% !important">
                        <thead>
                            <tr class="border-top-0">
                                <th>Invoice ID </th>
                                <?php if($admin == true){ echo "<th>Lembaga</th>"; } ?>
                                <th>Nama Peserta</th>
                                <th>Kegiatan</th>
                                <th>Nominal</th>
                                <th>Status</th>
                                <th>Opsi</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php foreach ($data->result() as $inv): ?>
                            <tr>
                                <td>
                                    <a href="#" id="showOnly" data-id="<?= $inv->invoice_id ?>" >
                                    <?= $inv->invoice_id ?>
                                    </a>
                                </td>
                                <?php if($admin == true){ echo 
                                    "<td>".$inv->organization."</td>"; } 
                                ?>
                                <td><?= $inv->participant ?></td>
                                <td><?= $inv->training ?></td>
                                <td><?= int_to_rupiah($inv->cash_amount) ?></td>
                                <td>
                                    <?php if($inv->is_paid == false){ ?>
                                        <span class="badge badge-warning">pending</span>
                                    <?php } else { ?>
                                        <span class="badge badge-success">LUNAS</span>
                                    <?php } ?>
                                </td>
                                <?php if($inv->is_paid == false){ ?>
                                <td>
                                    <a class="btn btn-sm btn-success" id="showData" data-id="<?= $inv->invoice_id ?>" style="color: white !important"/>Ubah Status</a>
                                </td>
                                <?php }else{ ?>
                                <td></td>
                                <?php } ?>
                            </tr>
                            <?php endforeach; ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Detail Transaksi<h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <form action="<?= base_url('dashboard/training/participantPaidPost') ?>" method="post">
                <div class="modal-body">
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-success form-control" onclick="return konfirmasi();">Ubah Status</button>
                </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="modalShow" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Detail Transaksi<h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <form action="<?= base_url('dashboard/training/participantPaidPost') ?>" method="post">
                <div class="modal-body">
                </div>
                </form>
            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>

    <script>
    $(function(){
        $(document).on('click','#showData',function(e){
            e.preventDefault();
            $("#myModal").modal('show');
            $.post('<?= base_url('dashboard/pembayaran/detailconfirm') ?>',
                {id:$(this).attr('data-id')},
                function(html){
                    $(".modal-body").html(html);
                }
            );
        });
    });

    $(function(){
        $(document).on('click','#showOnly',function(e){
            e.preventDefault();
            $("#modalShow").modal('show');
            $.post('<?= base_url('dashboard/pembayaran/detailconfirm') ?>',
                {id:$(this).attr('data-id')},
                function(html){
                    $(".modal-body").html(html);
                }
            );
        });
    });

    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin merubah status pembayaran ? Harap berhati-hati, Aksi ini tidak dapat di kembalikan')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });

    function konfirmasi(){
        if (confirm('Apakah anda yakin merubah status pembayaran ? Harap berhati-hati, Aksi ini tidak dapat di kembalikan')) {
            return true;
        } else {
            return false;
        }
    }
    </script>
</body>
</html>