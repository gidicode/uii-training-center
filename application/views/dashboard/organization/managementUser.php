<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('dashboard/parts/head'); ?>
    <title>Dashboard | Kelola Users</title>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= base_url('/dashboard') ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                Kelola Users
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <h1 class="h2 m-3" style="font-weight:400">Kelola Users
        <a href="#" class="btn btn-sm btn-primary ml-3" data-toggle="modal" data-target="#exampleModal"><i class="fas fa-plus mr-2"></i>Tambah User</a> </h1>
        <div class="card card-body m-3">
            <table class="table table-hover m-0" id="datatable">
                <thead>
                    <th>#</th>
                    <th>Username</th>
                    <th>Email</th>
                    <th>Phone</th>
                    <th>Status</th>
                    <th>Opsi</th>
                </thead>
                <tbody>
                    <?php foreach($users->result() as $i =>$usr): ?>
                    <tr>
                        <td><?= $i+1 ?></td>
                        <td><?= $usr->name ?></td>
                        <td><?= $usr->email ?></td>
                        <td><?= $usr->phone ?></td>
                        <td>
                        <?php 
                            if($usr->status == 0){
                                echo "<span class='badge badge-danger'>NON ACTIVE</span>";
                            } elseif($usr->status == 1){
                                echo "<span class='badge badge-success'>ACTIVE</span>";
                            }
                        ?>
                        </td>
                        <td>
                        <?php 
                            if($usr->status == 0){ ?>
                                <a href="<?= dashboard_url('users/'.$usr->id.'/active') ?>"
                                    class="btn btn-sm btn-link rounded-circle text-primary" id="active" data-toggle="tooltip" title="active" data-method="POST">
                                    <i class="fas fa-check"></i>
                                </a>
                            <?php } elseif($usr->status == 1){ ?>
                                <a href="<?= dashboard_url('users/'.$usr->id.'/suspend') ?>"
                                    class="btn btn-sm btn-link rounded-circle text-primary" id="suspend" data-toggle="tooltip" title="suspend" data-method="POST">
                                    <i class="fas fa-ban"></i>
                                </a>
                            <?php }
                        ?>
                        <a href="#"
                            class="btn btn-sm btn-link rounded-circle text-success" id="editData" data-id="<?= $usr->id ?>" data-toggle="tooltip" title="edit">
                            <i class="fas fa-pen"></i>
                        </a>
                        <a href="<?= dashboard_url('users/'.$usr->id.'/delete') ?>"
                            class="btn btn-sm btn-link rounded-circle text-danger" id="delete" data-toggle="tooltip" title="delete" data-method="POST">
                            <i class="far fa-trash-alt"></i>
                        </a>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="<?= dashboard_url('users'); ?>" method="post">
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Email</label>
                    <input type="text" class="form-control" name="email" id="" required>
                </div>
                <div class="form-group">
                    <label for="">Username</label>
                    <input type="text" class="form-control" name="username" id="" required>
                </div>
                <div class="form-group">
                    <label for="">Phone</label>
                    <input type="text" class="form-control" name="phone" id="" required>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Simpan</button>
            </div>
            </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="showEditForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Edit User<h4>
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                </div>
                <form action="<?= dashboard_url('users') ?>" method="post">
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
                </form>
            </div>
        </div>
    </div>


    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>

    $(function(){
        $(document).on('click','#editData',function(e){
            e.preventDefault();
            $("#showEditForm").modal('show');
            $.post('<?= base_url('dashboard/organization/detailUser') ?>',
                {id:$(this).attr('data-id')},
                function(html){
                    $(".modal-body").html(html);
                }
            );
        });
    });

    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>