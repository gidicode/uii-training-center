<!DOCTYPE html>
<html lang="en">
<head>
    <title>LOGIN ADMIN | UII Training Center</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('parts/alert'); ?>
    <div class="container mt-5">
        <div class="col-md-5 mx-auto my-auto">
            <h2 class="text-center">
                Login ADMIN<br>
                <small style="font-size:11pt">UII Training Center, Simpul Tumbuh UII</small>
            </h2>
            <div class="jumbotron mt-4">
                
                <p class="text-center">Silahkan login menggunakan akun Google Lembaga</p>
                <a href="<?= base_url('auth/login') ?>" class="btn btn-primary form-control">Masuk ke Dashboard</a>
            
            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    
</body>
</html>