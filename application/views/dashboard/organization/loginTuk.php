<!DOCTYPE html>
<html lang="en">
<head>
    <title>Tempat Uji Kompetensi (TUK) | UII Training Center</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('parts/alert'); ?>
    <div class="container mt-5">
        <div class="col-md-5 mx-auto my-auto">
            <h2 class="text-center">
                Tempat Uji Kompetensi (TUK)<br>
                <small style="font-size:11pt">UII Training Center, Simpul Tumbuh UII</small>
            </h2>
            <div class="jumbotron mt-4">
                <p class="text-center">Untuk mendaftar sebagai TUK, silahkan gunakan akun Google lembaga anda. Setelah itu silahkan isi formulir lembaga anda dan tunggu konfirmasi aktivasi akun dari ADMIN UII Training Center</p>
                <a href="<?= base_url('auth/registerTUK') ?>" class="btn btn-primary form-control">Daftar TUK</a>
                <hr>
                <a href="<?= base_url('auth/loginTuk') ?>" class="btn btn-warning form-control">Masuk ke Dashboard TUK</a>
            
            </div>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    
</body>
</html>