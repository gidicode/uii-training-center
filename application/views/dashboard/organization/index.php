<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('dashboard/parts/head'); ?>
    <title>Dashboard | Kelola Organisasi</title>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= base_url('/dashboard') ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                Kelola Organisasi
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        <div class="card m-3">
            <table class="table mb-0">
                <thead>
                    <tr class="border-top-0">
                        <th>No. </th><th>Email</th><th>Nama</th><th>Tingkat</th><th>Opsi</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach ($organizations as $i => $organization): ?>
                    <tr <?php if ($organization->status == My\Enums\OrgStatus::PENDING): ?> style="background-color: #fffddd;" <?php endif; ?>
                        <?php if ($organization->status == My\Enums\OrgStatus::INACTIVE): ?> style="background-color: #ffeeee;" <?php endif; ?>
                        >
                        <td><?= $i + 1 ?></td>
                        <td><?= $organization->email ?></td>
                        <td><?= $organization->name ?></td>
                        <td><?= $organization->level ?></td>
                        <td>
                            <?php if ($organization->status == My\Enums\OrgStatus::PENDING): ?>
                                <a href="<?= dashboard_url('organization/'. $organization->id .'/activate') ?>" role="button" class="btn btn-sm btn-link text-success" data-method="POST"><i class="fas fa-check"></i> Terima</button>
                                <a href="<?= dashboard_url('organization/'. $organization->id .'/decline') ?>" role="button" class="btn btn-sm btn-link text-danger" data-method="POST"><i class="fas fa-times"></i> Tolak</button>
                            <?php elseif ($organization->status == My\Enums\OrgStatus::ACTIVE): ?>
                                <a href="<?= dashboard_url('organization/'. $organization->id) ?>" role="button" class="btn btn-sm btn-link" ><i class="fas fa-desktop"></i> Monitor</button>
                                <a href="<?= dashboard_url('organization/'. $organization->id .'/nonactivate') ?>" role="button" class="btn btn-sm btn-link text-danger" data-method="POST"><i class="fas fa-minus-circle"></i> Non Aktifkan</button>
                            <?php elseif ($organization->status == My\Enums\OrgStatus::INACTIVE): ?>
                                <a href="<?= dashboard_url('organization/'. $organization->id) ?>" role="button" class="btn btn-sm btn-link"><i class="fas fa-desktop"></i> Monitor</button>
                                <a href="<?= dashboard_url('organization/'. $organization->id .'/activate') ?>" role="button" class="btn btn-sm btn-link text-success" data-method="POST"><i class="fas fa-check-circle"></i> Aktifkan</button>
                            <?php endif; ?>
                        </td>
                    </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>