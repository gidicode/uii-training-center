<!-- Modal add -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tambah Skema</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#" method="post">
            <input type="hidden" name="submitType" value="submit">
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Nama Skema</label>
                    <input type="text" name="skema" class="form-control" id="">
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" id="" rows="5" class="form-control"></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-primary">Simpan Skema</button>
            </div>
            </form>
        </div>
    </div>
</div>

<!-- Modal edit -->
<?php foreach($skema->result() as $skema): ?>
<div class="modal fade" id="edit<?= $skema->id ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Edit Skema</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#" method="post">
            <input type="hidden" name="submitType" value="edit">
            <input type="hidden" name="id" value="<?= $skema->id ?>">
            <div class="modal-body">
                <div class="form-group">
                    <label for="">Nama Skema</label>
                    <input type="text" name="skema" class="form-control" id="" value="<?= $skema->skema; ?>" />
                </div>
                <div class="form-group">
                    <label for="">Keterangan</label>
                    <textarea name="keterangan" id="" rows="5" class="form-control" ><?= $skema->keterangan; ?></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <a class="btn btn-danger" data-dismiss="modal">Close</a>
                <button type="submit" class="btn btn-primary">Simpan Skema</button>
            </div>
            </form>
        </div>
    </div>
</div>
<?php endforeach; ?>