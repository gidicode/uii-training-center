<!DOCTYPE html>
<html lang="en">
<head>
    <?php $this->load->view('dashboard/parts/head'); ?>
    <title>Dashboard | Kelola Organisasi</title>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                <a href="<?= dashboard_url() ?>">Dashboard</a>
            </li>
            <li class="breadcrumb-item">
                Monitor Organisasi - <?= $org['name'] ?>
            </li>
        </ol>
        <!-- END BREADCRUMBS -->
        
        <div class="card m-3">
            <div class="card-body">
                <div class="d-block">
                    <div class="float-left">
                        <h3 style="font-weight:400">
                            <i class="fas fa-arrow-left mr-3" ></i>
                            <?= $org['name'] ?>
                        </h3>
                    </div>
                    <div class="float-right">
                        <a href="<?= dashboard_url('organization/'. $org['id'] .'/nonactivate') ?>" class="btn btn-warning" data-method="POST">SUSPEND</a>
                    </div> 
                </div>
                
                <div class="row mt-5">

                    <div class="col-md-3">
                        <div class="card-counter info">
                            <i class="fa fa-users"></i>
                            <span class="count-numbers">
                                <?= $org['jmlPendaftarAktif']->num_rows() ?>
                                <small>/ <?= $org['jmlPendaftarAll']->num_rows() ?></small>
                            </span>
                            <span class="count-name">Pendaftar </span>
                        </div>
                    </div>

                    <div class="col-md-3">
                        <div class="card-counter primary">
                            <i class="fas fa-calendar-alt"></i>
                            <span class="count-numbers"> 
                                <?= $org['TrainingAktif']->num_rows() ?>
                                <small>/ <?= $org['TrainingAll']->num_rows() ?></small>
                            </span>
                            <span class="count-name">Pelatihan</span>
                        </div>
                    </div>

                    <div class="col-md-6">
                        <table class="table table-sm">
                            <tr>
                                <td>Email</td>
                                <td><?= $org['email'] ?></td>
                            </tr>
                            <tr>
                                <td>Level</td>
                                <td><?= $org['level'] ?></td>
                            </tr>
                            <tr>
                                <td>Tanggal Terdaftar</td>
                                <td><?= carbon($org['created_at'])->format('d M Y') ?></td>
                            </tr>
                            <tr>
                                <td>Deskripsi</td>
                                <td><?= $org['deskripsi'] ?></td>
                            </tr>
                        </table>
                    </div>

                </div>   
                
                
            </div>
        </div>

        <div class="container">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#pills-active" role="tab" aria-controls="pills-home" aria-selected="true">Pelatihan Aktif</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#pills-all" role="tab" aria-controls="pills-profile" aria-selected="false">Semua Pelatihan</a>
            </li>
        </ul>
        <div class="tab-content" id="pills-tabContent">
            <div class="tab-pane fade show active" id="pills-active" role="tabpanel" aria-labelledby="pills-home-tab">
                <div class="row">
                <?php foreach($org['TrainingAktif']->result() as $t): ?>
                <div class="col-lg-4 col-md-3 col-sm-6 mb-3">
                    <div class="card border-0">
                        <img class="card-img-top" src="<?= $t->filename ?>" alt="Card image cap">
                        <div class="card-body">
                            <a class="link" target="_blank" href="<?= dashboard_url('training/'.$t->training_id.'/manage') ?>">
                            <h5 class="card-title">
                                <?= $t->training_name ?>
                                <br><a class="link" href=""><small style="font-size:10pt"><?= $t->org_name ?></small></a>
                            </h5></a>
                            <p class="card-text" style="font-size:11pt;">
                            <?= carbon($t->start_time)->isoFormat('DD MMMM YYYY') . ($t->start_time == $t->finish_time ? '' : ' - '. carbon($t->finish_time)->isoFormat('DD MMMM YYYY')) ?><br>
                                <?= $t->location ?>
                            </p>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                </div>
            </div>
            <div class="tab-pane fade" id="pills-all" role="tabpanel" aria-labelledby="pills-profile-tab">
                <div class="row">
                    <?php foreach($org['TrainingAll']->result() as $t): ?>
                    <div class="col-lg-4 col-md-3 col-sm-6 mb-3">
                        <div class="card border-0">
                            <img class="card-img-top" src="<?= $t->filename ?>" alt="Card image cap">
                            <div class="card-body">
                                <a class="link" target="_blank" href="<?= dashboard_url('training/'.$t->training_id.'/manage') ?>">
                                <h5 class="card-title">
                                    <?= $t->training_name ?>
                                    <br><a class="link" href=""><small style="font-size:10pt"><?= $t->org_name ?></small></a>
                                </h5></a>
                                <p class="card-text" style="font-size:11pt;">
                                <?= carbon($t->start_time)->isoFormat('DD MMMM YYYY') . ($t->start_time == $t->finish_time ? '' : ' - '. carbon($t->finish_time)->isoFormat('DD MMMM YYYY')) ?><br>
                                    <?= $t->location ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <?php endforeach; ?>
                    </div>
            </div>
        </div>
        </div>

    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>
    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>