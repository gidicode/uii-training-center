<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->
        <div class="content-mobile" style="margin-top:100px"></div>
        <div class="container mt-5 mb-5">
            <div class="container-card-pelatihan">
                <div class="card">
                    <div class="card-header">
                        <h4><strong>Konfirmasi</strong> Pendaftaran</h4>
                        <?php print_r($data) ?>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h5>Anda mendaftar:</h5>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td>Nama Sertifikasi</td>
                                            <td> <?= $data2['sertifikasi_name'] ?> </td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pelaksanaan</td>
                                            <td> <?= $data2['time'] ?> </td>
                                        </tr>
                                        <tr>
                                            <td>Lembaga Pelaksana</td>
                                            <td> <?= $data2['tuk_name'] ?> </td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pendaftar</td>
                                            <td><?= auth()->name ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?= auth()->email ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <b>Petunjuk Pembayaran:</b>
                                <?= $data2['infoPembayaran']; ?>
                            </div>
                            <div class="col-md-4">
                                <p><b>Total yang harus dibayar</b></p>
                                <h3>Rp. <?= number_format($data2['price']) ?></h3>
                                <hr>
                                <form action="<?= base_url('Peserta/konfirmasiRegistrasiSertifikasi') ?>" method="post">
                                    <input type="hidden" name="konfirmasiRegistrasi2" value="true">
                                    <button type="submit" id="send-data" name="send-data"  class="btn btn-primary form-control">Lanjutkan Pendaftaran</button>

                                    <input type="hidden" name="userID" value="<?= $data['user_id'] ?>">
                                    <input type="hidden" name="sertID" value="<?= $data['sertification_id'] ?>">
                                    <input type="hidden" name="invID" value="<?= $data['invoice_id'] ?>">
                                    <input type="hidden" name="form" value="<?= htmlentities($data['form']) ?>">

                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script>
            $(document).ready(function)

        </script>
        
    </body>
</html>