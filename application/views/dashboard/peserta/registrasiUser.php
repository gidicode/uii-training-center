<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->

        <!-- Section Thank You -->
		<div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-3">
                    <div class="img-profile">
                        <img src="<?= auth()->avatar; ?>" alt="">
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4><strong>Lengkapi</strong> Profil</h4>
                            <hr>
                            <form action="#" method="post">
                                <input type="hidden" name="registrasiUser" value="true">
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                    <input type="text" readonly name="email" class="form-control-plaintext" id="staticEmail" value="<?= auth()->email; ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama" name="name" placeholder="Nama Lengkap" value="<?= auth()->name; ?>" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="hp" class="col-sm-2 col-form-label">No HP</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="hp" name="phone" placeholder="No HP" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="institusi" class="col-sm-2 col-form-label">Institusi</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="institusi" name="institusi" placeholder="Institusi" value="<?= $institusi ?>" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="pekerjaan" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" name="pekerjaan" id="pekerjaan" placeholder="Pekerjaan">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                    <textarea name="address" id="alamat" class="form-control" rows="5"></textarea>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary float-right">Lanjutkan...</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section 6 Footer -->
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

    </body>
</html>