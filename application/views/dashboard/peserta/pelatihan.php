<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->

        <!-- Section Thank You -->
		<div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-3" style="margin-bottom:25px;">
                    <div class="img-profile">
                        <img src="<?= $training['filename'] ?>" alt="">
                    </div>
                    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
                        <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">Overview</a>
                        <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">Peserta</a>
                        <!--
                        <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Nilai / Sertifikat</a> -->
                    </div>
                </div>
                <div class="col-md-8">
                    <nav aria-label="breadcrumb">
                        <ol class="breadcrumb" style="background-color: transparent; padding:0px;">
                            <li class="breadcrumb-item"><a href="<?= base_url('profile') ?>">Beranda Profil</a></li>
                            <li class="breadcrumb-item active" style="white-space:nowrap; overflow:hidden; text-overflow:ellipsis;" aria-current="page"><?= $training['training_name'] ?></li>
                        </ol>
                    </nav>
                    <div class="card">
                        <div class="card-body">
                            <h4><?= $training['training_name'] ?></h4>
                            <br>
                            <div class="tab-content" id="v-pills-tabContent">
                                <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
                                    
                                    <!-- Kondisi jika peserta belum bayar -->
                                    <?php if($training['status_peserta'] == 0){ ?>
                                        <p><i class="fas fa-exclamation-circle mr-3"></i> Silahkan selesaikan pembayaran anda untuk pelatihan ini!</p>
                                        <hr>
                                        <b>Petunjuk Pembayaran:</b>
                                        <?= $training['infoPembayaran']; ?>
                                    <?php } ?>
                                    
                                    <!-- Kondisi jika peserta sudah bayar dan status pelatihan belum dimulai -->
                                    <?php if($training['status_peserta'] == 1 AND $training['status'] == 3){ ?>
                                    <table class="table table-striped">
                                        <tr>
                                            <td>Tanggal Pelatihan</td>
                                            <td>
                                                <?= carbon($training['start_time'])->isoFormat('DD MMMM YYYY') . ($training['start_time'] == $training['finish_time'] ? '' : ' - '. carbon($training['finish_time'])->isoFormat('DD MMMM YYYY')) ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Tempat Pelatihan</td>
                                            <td><?= $training['location'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Daftar</td>
                                            <td>
                                                <?= carbon($training['joined_at'])->isoFormat('DD MMMM YYYY') ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Status</td>
                                            <td>
                                                <div class="badge badge-warning">Belum dimulai</div>
                                            </td>
                                        </tr>
                                    </table> 
                                    <?php } ?>

                                    <!-- Kondisi jika status peserta aktif dan berjalan -->
                                    <?php if($training['status_peserta'] == 1 AND $training['status'] == 4 OR $training['status'] == 5){ ?>
                                    <?php
                                    if($training['status'] == 4){
                                        echo "Status: <div class='badge badge-success'>ONGOING</div>";
                                    } elseif($training['status'] == 5){
                                        echo "Status: <div class='badge badge-info'>FINISH</div>";
                                    }
                                    if($training['updates']->num_rows() == 0){
                                    echo "<br><div class='mt-3'></div><i> Belum ada update, silahkan tambah update!</i>";
                                    }else{
                                    foreach($training['updates']->result() as $i => $upt): ?>
                                    <div class="card mt-3">
                                        <div class="card-header">
                                            <div class="float-left">
                                                <h5 class="mb-0"><?= $upt->title ?></h5>
                                                <small><?= carbon($upt->last_update)->isoFormat('DD MMMM YYYY - H:m') ?></small>
                                            </div>
                                        </div>
                                        <div class="card-body">
                                            <?= $upt->note ?>
                                        </div>
                                    </div>
                                    <?php endforeach; } ?>
                                    <?php } ?>
                                    <!-- 
                                    -->
                                </div>
                                <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
                                    <div class="table-responsive">
                                        <table class="table table-striped">
                                            <thead>
                                                <th>No</th>
                                                <th>Nama</th>
                                                <th>Instansi</th>
                                                <th>Email</th>
                                            </thead>
                                            <tbody>
                                                <?php foreach($training['participant']->result() as $i => $prt): ?>
                                                <tr>
                                                    <td><?= $i+1 ?></td>
                                                    <td><?= $prt->username ?></td>
                                                    <td><?= $prt->institusi ?></td>
                                                    <td><?= $prt->email ?></td>
                                                </tr>
                                                <?php endforeach; ?>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                                <!-- 
                                <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
                                    <!-- if status pelatihan FINISH -->
                                    <!--
                                    <table class="table table-striped">
                                        <tr>
                                            <td>Nilai</td>
                                            <td>9.2 (Istimewa)</td>
                                        </tr>
                                        <tr>
                                            <td>Sertifikat</td>
                                            <td><a href="#">Unduh disini</a></td>
                                        </tr>
                                    </table> -->

                                    <!-- if status pelatihan HAVEN'T FINISH 
                                    <div class="alert alert-danger">Nilai / Sertifikat anda belum keluar. Pelatihan masih berjalan</div> -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section 6 Footer -->
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

    </body>
</html>