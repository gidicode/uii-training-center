<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Alert -->
        <?php $this->load->view('parts/alert'); ?>

        <!-- Main page -->

        <!-- Section Thank You -->
		<div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-3">
                    <div class="img-profile">
                        <img src="<?= auth()->avatar ?>" alt="">
                    </div>
                    <div class="list-group">
                        <a href="<?= base_url('profile') ?>" class="list-group-item list-group-item-action active">
                            Beranda
                        </a>
                        <a href="<?= base_url('profile/edit') ?>" class="list-group-item list-group-item-action">Edit Profil</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h2>Hai <strong><?= auth()->name ?></strong></h2>
                            <hr>
                            <h4>Pelatihan & Sertifikasi saya</h4>
                            <div class="row mt-4">
                            <!-- List pelatihan  -->
                                <?php foreach($data as $tr): ?>
                                <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                                    <div class="card border-0">
                                        <a href="<?= base_url('profile/pelatihan/'.$tr->id_training) ?>">
                                            <img class="card-img-top" src="<?= $tr->filename; ?>" alt="Card image cap">
                                        </a>
                                        <div class="card-body">
                                            <span class="label-title">
                                                <a href="<?= base_url('profile/pelatihan/'.$tr->id_training) ?>">
                                                    <p><?= $tr->name; ?></p>
                                                </a>
                                                <small> <?= carbon($tr->start_time)->isoFormat('DD MMMM YYYY'); ?> - <?= carbon($tr->finish_time)->isoFormat('DD MMMM YYYY'); ?>  </small>
                                            </span>
                                            
                                        </div>
                                        <?php if($tr->is_paid == 0): ?>
                                        <div class="training-exp" style="background-color:#ffcc00">
                                            <p>Pending</p>
                                        </div>
                                        <?php endif; ?>

                                        <?php if($tr->is_paid == 1): ?>
                                        <div class="training-exp" style="background-color:#5cb85c; color:#fff">
                                            <p>Active</p>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php endforeach; ?>
                                
                                <!-- List sertifikasi 1 -->
                                <?php foreach($data2 as $tr): ?>
                                <div class="col-lg-4 col-md-4 col-sm-6 mb-3">
                                    <div class="card border-0">
                                        <a href="<?= base_url('profile/sertifikasi/'.$tr->id_sertifikasi) ?>">
                                            <img class="card-img-top" src="<?= $tr->filename; ?>" alt="Card image cap">
                                        </a>
                                        <div class="card-body">
                                            <span class="label-title">
                                                <a href="<?= base_url('profile/sertifikasi/'.$tr->id_sertifikasi) ?>">
                                                    <p><?= $tr->name; ?></p>
                                                </a>
                                                <small> <?= carbon($tr->start_time)->isoFormat('DD MMMM YYYY'); ?> </small>
                                            </span>
                                            
                                        </div>
                                        <?php if($tr->is_paid == 0): ?>
                                        <div class="training-exp" style="background-color:#ffcc00">
                                            <p>Pending</p>
                                        </div>
                                        <?php endif; ?>

                                        <?php if($tr->is_paid == 1): ?>
                                        <div class="training-exp" style="background-color:#5cb85c; color:#fff">
                                            <p>Active</p>
                                        </div>
                                        <?php endif; ?>
                                    </div>
                                </div>
                                <?php endforeach; ?>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section 6 Footer -->
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

    </body>
</html>