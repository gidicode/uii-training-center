<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
        <link rel="stylesheet" href="<?= asset('css/jquery.fileuploader.css') ?>">
        <!-- Froala Editor -->
        <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_editor.min.css' rel='stylesheet' type='text/css' />
        <link href='https://cdn.jsdelivr.net/npm/froala-editor@2.9.5/css/froala_style.min.css' rel='stylesheet' type='text/css' />
         <!-- Datepicker -->
        <link href="https://unpkg.com/gijgo@1.9.13/css/gijgo.min.css" rel="stylesheet" type="text/css" />
    </head>

    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->
        <div class="content-mobile" style="margin-top:100px"></div>
        <div class="container mt-5 mb-5">
            <div class="container-card-pelatihan">
                <div class="card">
                    <div class="card-header">
                        <h4><strong>Formulir</strong> Pendaftaran Sertifikasi</h4>
                    </div>
                    <?php $idSertifikasiDaftar = $this->uri->segment('2')  ?>
                    <form action="<?= base_url('form-registrasi-sertifikasi/'.$idSertifikasiDaftar) ?>" method="POST" enctype="multipart/form-data">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h5> cantumkan data pribadi, data pendidikan formal serta data pekerjaan anda pada saat ini.  </h5>
                                    <br>
                                    <h5> <strong> A. Data Pribadi  </strong> </h5>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td> Nama lengkap  </td>
                                                <td> 
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fas fa-font"></i></span>
                                                        </div>
                                                        <input name="namaLengkap" class="form-control" placeholder="Nama lengkap" required autofocus>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Tempat / Tanggal Lahir</td>
                                                <td> 
                                                    <div class="form-row">
                                                        <div class="col-6">
                                                            <input type="text" name="tempatLahir" class="form-control" required> 
                                                        </div>
                                                        <h2> <strong>&nbsp;/&nbsp;</strong> </h2>
                                                        <div class="col">
                                                            <div class="input-group-prepend">
                                                                <span class="input-group-text"><i class="fas fa-calendar-day"></i></span>
                                                                <input type="date" name="tglLahir" id="b-day" class="form-control" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Jenis Kelamin</td>
                                                <td> 
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="customRadioInline1" name="jenisKelamin" value="laki-laki" class="custom-control-input" required>
                                                        <label class="custom-control-label" for="customRadioInline1"> Laki-Laki </label>
                                                    </div>
                                                    <div class="custom-control custom-radio custom-control-inline">
                                                        <input type="radio" id="customRadioInline2" name="jenisKelamin" value="perempuan" class="custom-control-input" required>
                                                        <label class="custom-control-label" for="customRadioInline2"> Perempuan </label>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Kebangsaan</td>
                                                <td> 
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fas fa-flag"></i></span>
                                                        </div>
                                                        <input id="location" type="text" name="kebangsaan" class="form-control" required>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Alamat Rumah / Kode Pos</td>
                                                <td>
                                                    <div class="form-row">
                                                        <div class="col-7">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-map"></i></span>
                                                                </div>
                                                                <input id="location" type="text" name="alamatRumah" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <h2> <strong>&nbsp;/&nbsp;</strong> </h2>
                                                        <div class="col">
                                                            <input type="text" name="kodeposRumah" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email / No.telephone</td>
                                                <td>
                                                    <div class="form-row">
                                                        <div class="col-6">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-at"></i></span>
                                                                </div>
                                                                <input type="text" name="emailRumah" class="form-control" required>
                                                            </div>
                                                        </div>
                                                        <h2> <strong>&nbsp;/&nbsp;</strong> </h2>
                                                        <div class="col">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                                                </div>
                                                                <input type="text" name="telponRumah" class="form-control" required>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Pendidikan Terakhir</td>
                                                <td> 
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="input-group-text"><i class="fas fa-school"></i></span>
                                                        </div>
                                                        <input type="text" name="pendidikanTerakhir" class="form-control" required>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>

                                    <br>

                                    <h5> <strong> B. Data Pekerjaan Sekarang </strong> </h5>
                                    <div class="table-responsive">
                                        <table class="table">
                                            <tr>
                                                <td> Nama Instansi  </td>
                                                <td> <input type="text" name="namaInstansi" class="form-control" required/> </td>
                                            </tr>
                                            <tr>
                                                <td>Jabatan</td>
                                                <td> 
                                                    <input type="text" name="jabatan" class="form-control" required> 
                                                </td>
                                            </tr>
                                            <tr>
                                            <tr>
                                                <td>Alamat Instansi / Kode Pos</td>
                                                <td>
                                                    <div class="form-row">
                                                        <div class="col-7">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-map"></i></span>
                                                                </div>
                                                                <input type="text" name="alamatInstansi" class="form-control" required> 
                                                            </div>
                                                        </div>
                                                        <h2> <strong>&nbsp;/&nbsp;</strong> </h2>
                                                        <div class="col">
                                                            <input type="text" name="kodeposInstansi" class="form-control" required>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Email / No.telephone</td>
                                                <td>
                                                    <div class="form-row">
                                                        <div class="col-6">
                                                             <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-at"></i></span>
                                                                </div>
                                                                <input type="email" name="emailInstansi" class="form-control" required> 
                                                            </div>
                                                        </div>
                                                        <h2> <strong>&nbsp;/&nbsp;</strong> </h2>
                                                        <div class="col">
                                                            <div class="input-group">
                                                                <div class="input-group-prepend">
                                                                    <span class="input-group-text"><i class="fas fa-phone"></i></span>
                                                                </div>
                                                                <input type="text" name="telponInstansi" class="form-control" required>
                                                            </div>
                                                        </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <input type="hidden" name="konfirmasiRegistrasi" value="true">
                            <input type="hidden" name="sertification_id" value=value="<?= $this->uri->segment('2') ?>">
                            <button type="submit" class="btn btn-primary form-control">Lanjutkan Pendaftaran</button>
                        </div>
                    </form>

                </div>
            </div>
        </div>
        
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script>
        var today = new Date(new Date().getFullYear(), new Date().getMonth(), new Date().getDate());
        $('#b-day').datepicker({
            uiLibrary: 'bootstrap4',
            iconsLibrary: 'fontawesome',
            format: 'yyyy-mm-dd'
        });
        </script>
        
    </body>
</html>