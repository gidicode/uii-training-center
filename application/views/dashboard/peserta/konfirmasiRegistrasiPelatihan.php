<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->
        <div class="content-mobile" style="margin-top:100px"></div>
        <div class="container mt-5 mb-5">
            <div class="container-card-pelatihan">
                <div class="card">
                    <div class="card-header">
                        <h4><strong>Konfirmasi</strong> Pendaftaran</h4>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <h5>Anda mendaftar:</h5>
                                <div class="table-responsive">
                                    <table class="table">
                                        <tr>
                                            <td>Nama Pelatihan</td>
                                            <td><?= $data['name'] ?></td>
                                        </tr>
                                        <tr>
                                            <td>Tanggal Pelaksanaan</td>
                                            <td>31 Dec 2018 - 4 Feb 2019</td>
                                        </tr>
                                        <tr>
                                            <td>Lembaga Pelaksana</td>
                                            <td>IT Centrum UII</td>
                                        </tr>
                                        <tr>
                                            <td>Nama Pendaftar</td>
                                            <td><?= auth()->name ?></td>
                                        </tr>
                                        <tr>
                                            <td>Email</td>
                                            <td><?= auth()->email ?></td>
                                        </tr>
                                    </table>
                                </div>
                                <b>Petunjuk Pembayaran:</b>
                                <?= $data['infoPembayaran']; ?>
                            </div>
                            <div class="col-md-4">
                                <p><b>Total yang harus dibayar</b></p>
                                <h3>Rp. <?= number_format($data['price']) ?></h3>
                                <hr>
                                <form action="#" method="post">
                                    <input type="hidden" name="konfirmasiRegistrasi" value="true">
                                    <input type="hidden" name="user" value="<?= auth()->id ?>" />
                                    <input type="hidden" name="training_id" value="<?= $data['id'] ?>" />
                                    <button type="submit" class="btn btn-primary form-control">Lanjutkan Pendaftaran</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>
        
    </body>
</html>