<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->

        <!-- Section Thank You -->
		<div class="container mt-5 mb-5">
            <div class="row">
                <div class="col-md-3">
                    <div class="img-profile">
                        <img src="https://lh5.googleusercontent.com/-3IC_Q27PfxU/AAAAAAAAAAI/AAAAAAAAAAs/nQojhqeRO1k/s96-c/photo.jpg" alt="">
                    </div>
                    <div class="list-group">
                        <a href="<?= base_url('profile') ?>" class="list-group-item list-group-item-action">
                            Beranda
                        </a>
                        <a href="<?= base_url('profile/edit') ?>" class="list-group-item list-group-item-action active">Edit Profil</a>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="card">
                        <div class="card-body">
                            <h4><strong>Edit</strong> Profil</h4>
                            <hr>
                            <form action="#" method="POST">
                                <input type="hidden" name="editUser" value="true" />
                                <div class="form-group row">
                                    <label for="staticEmail" class="col-sm-2 col-form-label">Email</label>
                                    <div class="col-sm-10">
                                    <input type="text" name="email" readonly class="form-control-plaintext" id="staticEmail" value="<?= $user['email'] ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="nama" class="col-sm-2 col-form-label">Nama Lengkap</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="nama" name="name" placeholder="Nama Lengkap" value="<?= $user['name'] ?>" required>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="hp" class="col-sm-2 col-form-label">No HP</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="hp" name="phone" placeholder="No HP" value="<?= $user['phone'] ?>" required >
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="institusi" class="col-sm-2 col-form-label">Institusi</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" id="institusi" name="institusi" placeholder="Institusi" value="<?= $user['institusi'] ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="pekerjaan" class="col-sm-2 col-form-label">Pekerjaan</label>
                                    <div class="col-sm-10">
                                    <input type="text" class="form-control" name="pekerjaan" id="pekerjaan" placeholder="Pekerjaan" value="<?= $user['pekerjaan'] ?>">
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="alamat" class="col-sm-2 col-form-label">Alamat</label>
                                    <div class="col-sm-10">
                                    <textarea name="address" id="alamat" class="form-control" rows="5"><?= $user['address'] ?></textarea>
                                    </div>
                                </div>

                                <button type="submit" class="btn btn-primary float-right">Simpan Perubahan</button>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section 6 Footer -->
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

    </body>
</html>