<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Main page -->
        <div class="content-mobile" style="margin-top:100px"></div>
        <div class="container mt-5 mb-5">
            <div class="container-card-pelatihan">
                <div class="card">
                    <div class="card-body">
                        <h3>Selamat, registrasi anda berhasil</h3>
                        <hr>
                        <p>Untuk melanjutkan pendaftaran, silahkan cek email dan ikuti instruksi pembayaran</p>
                        <hr>
                        <strong>Petunjuk Pembayaran:</strong>
                        <?= $data['infoPembayaran'] ?>
                        <hr>
                        <span class="alert alert-danger">
                            <i class="fas fa-exclamation-circle"></i>Cek Email anda atau dasbor profil anda untuk melihat petunjuk pembayaran, Jika anda belum mendapatkan email, silahkan mengecek folder spam pada email anda. 
                        </span>
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>
        
    </body>
</html>