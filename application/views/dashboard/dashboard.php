<!DOCTYPE html>
<html lang="en">
<head>
    <title>Dashboard</title>
    <?php $this->load->view('dashboard/parts/head'); ?>
</head>
<body style="background-image: url(<?= asset('img/bg-dashboard.svg') ?>);">
    <?php $this->load->view('dashboard/parts/sidebar'); ?>
    <div>
        <?php $this->load->view('dashboard/parts/navbar'); ?>
        <!-- BREADCRUMBS -->
        <ol class="breadcrumb" style="background: none;">
            <li class="breadcrumb-item">
                Dashboard
            </li>
        </ol>
        <!-- END BREADCRUMBS -->

        <div class="row m-0">
            <div class="col-md-6">
                <h1 class="h5 mt-3" style="font-weight:400">Welcome,</h1>
                <h2 class="h2" style="font-weight:400"> <?= auth()->name ?></h2>
                <p class="badge badge-success">DASBOR ADMIN</p>
            </div>
            <div class="col-md-3">
                <div class="card-counter info">
                    <i class="fa fa-users"></i>
                    <span class="count-numbers">
                        <?= $data['lembagaAktif']->num_rows() ?>
                        <small>/ <?= $data['lembagaAll']->num_rows() ?></small>
                    </span>
                    <span class="count-name">Lembaga Aktif</span>
                </div>
            </div>

            <div class="col-md-3">
                <div class="card-counter primary">
                    <i class="fas fa-calendar-alt"></i>
                    <span class="count-numbers">
                        <?= $data['TrainingAktif']->num_rows() ?>
                        <small>/ <?= $data['TrainingAll']->num_rows() ?></small>
                    </span>
                    <span class="count-name">Pelatihan Aktif</span>
                </div>
            </div>
        </div>
        
        <hr>

        <div class="row m-0">
            <div class="col-md-7">
                <div class="card">
                    <div class="card-header">
                        <p class="mb-0" style="font-size:14pt; font-weigh:500">Data Lembaga Baru</p>
                    </div>
                    <div class="card-body">
                        <?php if($data['lembaga']->num_rows() == 0){ ?>
                            <p><i class="fas fa-exclamation-circle"></i> Tidak ada data lembaga baru</p>
                        <?php }else{ ?>
                        <table class="table table-hover table-sm">
                            
                            <tbody>
                                <?php foreach($data['lembaga']->result() as $i => $org): ?>
                                <tr>
                                    <td><?= $i+1 ?></td>
                                    <td><?= $org->name ?></td>
                                    <td><?= $org->level ?></td>
                                    <td><?= carbon($org->created_at)->format('d M Y') ?></td>
                                    <td>
                                        <a href="<?= dashboard_url('organization/'. $org->id .'/activate') ?>" role="button" class="btn btn-sm btn-link text-success" data-method="POST"><i class="fas fa-check"></i> Terima</button>
                                        <a href="<?= dashboard_url('organization/'. $org->id .'/decline') ?>" role="button" class="btn btn-sm btn-link text-danger" data-method="POST"><i class="fas fa-times"></i> Tolak</button>
                                    </td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <a href="<?= dashboard_url('organization') ?>" class="btn btn-primary btn-sm float-right">lihat semua</a>
                        <?php } ?>
                    </div>
                </div>
            </div>

            <div class="col-md-5">
                <div class="card">
                    <div class="card-header">
                        <p class="mb-0" style="font-size:14pt; font-weigh:500">Data Pelatihan Aktif</p>
                    </div>
                    <div class="card-body">
                        <?php if($data['pelatihan']->num_rows() == 0){ ?>
                            <p><i class="fas fa-exclamation-circle"></i> Tidak ada data pelatihan aktif</p>
                        <?php }else{ ?>
                        <table class="table table-hover table-borderless table-sm">
                            <tbody>
                                <?php foreach($data['pelatihan']->result() as $training): ?>
                                <tr>
                                    <td><?= $training->id ?></td>
                                    <td><?= $training->name ?></td>
                                    <td><?= $this->db->get_where('participants',array('training_id' => $training->id, 'is_paid' => 1))->num_rows() ?> Peserta</td>
                                </tr>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                        <a href="<?= dashboard_url('training') ?>" class="btn btn-primary btn-sm float-right">lihat semua</a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>



    </div>

    <?php $this->load->view('dashboard/parts/script'); ?>

    <script>
    $(document).ready(function () {
        $('[data-method="POST"]').click(function (e) {
            e.preventDefault();
            if (confirm('Apakah anda yakin melakukan aksi ini?')) {
                var form = document.createElement('form');
                form.setAttribute('method', 'POST');
                form.setAttribute('action', $(this).attr('href'));
                document.body.appendChild(form);
                form.submit();
            }
        });
    });
    </script>
</body>
</html>