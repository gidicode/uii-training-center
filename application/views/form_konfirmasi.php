<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <div class="container">
            <div class="row mt-5 mb-5">
                <div class="col-md-6 mx-auto">
                    <div class="card">
                        <div class="card-header">
                            <h4>Konfirmasi Pembayaran</h4>
                        </div>
                        <div class="card-body">
                            <?php if($this->session->flashdata('payment_success')){ ?>
                            <?= $this->session->flashdata('payment_success'); ?>
                            <?php } else { ?>
                            <form action="<?= base_url('homepage/post_payment_confirm') ?>" method="post">
                                <div class="form-group">
                                    <label>Kode Pembayaran / Invoice</label>
                                    <input type="text" class="form-control" value="<?= $code['invoice_id'] ?>" disabled />
                                </div>

                                <input type="hidden" name="invoice_id" value="<?= $code['invoice_id'] ?>" >

                                <div class="form-group">
                                    <label>Nama Peserta</label>
                                    <input type="text" class="form-control" name="participant" value="<?= $code['participant'] ?>" disabled />
                                </div>

                                <div class="form-group">
                                    <label>Nama Kegiatan </label>
                                    <input type="text" class="form-control" name="training" value="<?= $code['training'] ?>" disabled />
                                </div>

                                <div class="form-group">
                                    <label>Tanggal Transfer</label>
                                    <input type="date" class="form-control" name="payment_date" required />
                                </div>

                                <div class="form-group">
                                    <label>Bank Tujuan</label>
                                    <input type="text" class="form-control" placeholder="Contoh: BNI, Mandiri, dsb" name="to_bank_name" required />
                                </div>

                                <div class="form-group">
                                    <label>Nama Pengirim / Pemilik Rekening</label>
                                    <input type="text" class="form-control" placeholder="Tulis nama anda/pemilik rekening" name="from_name" required />
                                </div>

                                <div class="form-group">
                                    <label>Nominal Transfer</label>
                                    <input type="text" class="form-control mask-rupiah" value="<?= $code['price'] ?>" name="cash_amount" />
                                </div>

                                <button type="submit" class="btn btn-primary form-control">Submit</button>
                            </form>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section 6 Footer -->
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>
        <?php $this->load->view('dashboard/parts/script'); ?>
        
    </body>
</html>