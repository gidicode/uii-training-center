<div class="col-lg-4 col-md-4 col-sm-6 mb-3">
    <div class="card border-0">
        <img class="card-img-top" src="<?= $c->filename ?>" alt="Card image cap">
        <div class="card-body">
            <a class="link" href="<?= base_url('sertifikasi/'.$c->sertifikasi_id) ?>">
            <h5 class="card-title">
                <?= $c->sertifikasi_name ?>
                <br><a class="link" href="<?= base_url('tuk/'.$c->tuk_id) ?>"><small style="font-size:10pt"><?= $c->tuk_name ?></small></a>
            </h5></a>
            <span class="label-price">
                <small>Start from</small>
                <p><?= 'Rp. '.number_format($c->price) ?></p>
            </span>
            <p class="card-text" style="font-size:11pt;">
            <?= carbon($c->start_time)->isoFormat('DD MMMM YYYY') ?><br>
                <?= $c->location ?>
            </p>
        </div>
    </div>
</div>