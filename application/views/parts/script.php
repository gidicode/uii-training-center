<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/iScroll/5.2.0/iscroll.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js"></script>
<script src="<?= asset('vendor/bootstrap/js/bootstrap.js') ?>"></script>

<script type="text/javascript" src="<?= asset('js/jquery.magnific-popup.js') ?>"></script>

<!-- OWL Carousel -->
<script src="<?= asset('vendor/owl/dist/owl.carousel.min.js') ?>"></script>

<!-- drawer.js -->
<script src="<?= asset('vendor/drawer/dist/js/drawer.min.js') ?>" charset="utf-8"></script>

<!-- Croppie.Js -->
<script src="<?= asset('vendor/croppie/croppie.js') ?> "></script>

<script type="text/javascript" src="<?= asset('js/main.js') ?>"></script>

<script>
    $('.drawer').drawer();
</script>