<?php if (session()->flashdata('danger') || 
        session()->flashdata('info') || 
        session()->flashdata('warning')): ?>
<div style="position:absolute; z-index: 10020; margin-top: 25px; right: 25px;">
    <?php if (session()->flashdata('danger')): ?>
    <div class="alert alert-danger alert-dismissable fade show" role="alert">
        <i class="fas fa-lg fa-exclamation-circle mr-2"></i>
        <?= session()->flashdata('danger') ?>
        <button class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php endif; ?>
    <?php if (session()->flashdata('info')): ?>
    <div class="alert alert-info alert-dismissable fade show" role="alert">
        <i class="fas fa-lg fa-info-circle mr-2"></i>
        <?= session()->flashdata('info') ?>
        <button class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php endif; ?>
    <?php if (session()->flashdata('warning')): ?>
    <div class="alert alert-warning alert-dismissable fade show" role="alert" style="min-width: 300px;">
        <i class="fas fa-lg fa-exclamation-triangle mr-2"></i>
        <?= session()->flashdata('warning') ?>
        <button class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <?php endif; ?>
</div>
<?php endif; ?>

<script>
window.setTimeout(function() {
    $("div.alert").fadeTo(500, 0).slideUp(500, function(){
        $(this).remove(); 
    });
}, 4000);
</script>