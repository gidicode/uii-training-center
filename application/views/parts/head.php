<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta http-equiv="Content-Language" content="en" />
<meta name="keywords" content=" " />
<meta name="description" content=" " />
<meta name="language" content="en" />
<meta name="distribution" content="global" />
<meta name="copyright" content="2018" />
<meta name="author" content=" " />
<meta name="revisit-after" content="1 day" />
<meta name="rating" content="general" />
<meta name="robots" content="index,follow" />
<meta name="googlebot" content="index,follow" />
<meta name="msnbot" content="index,follow" />
<meta name="spiders" content="all" />
<meta name="webcrawlers" content="all" />
<meta name="expires" content="never" />
<meta name="dc.title" content=" " />
<meta name="dc.creator.e-mail" content=" " />
<meta name="dc.creator.name" content=" " />
<meta name="dc.creator.website" content=" " />
<meta name="tgn.name" content="Yogyakarta" />
<meta name="tgn.nation" content="Indonesia" />
<meta name="geo.country" content="Indonesia" />
<meta name="geo.placename" content="Yogyakarta" />
<meta charset="UTF-8">

<title>UII Training Center</title>

<link rel="icon" href="<?= asset('favicon.png') ?>" type="image/png">

<link href="https://fonts.googleapis.com/css?family=Open+Sans|Open+Sans+Condensed:700" rel="stylesheet">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="<?= asset('css/dashboard.bootstrap.min.css'); ?>" />

<!-- Owl Carousel -->
<link type="text/css" rel="stylesheet" href="<?= asset('css/owl.carousel.css')?>" />
<link type="text/css" rel="stylesheet" href="<?= asset('css/owl.theme.default.css')?>" />

<!-- Magnific Popup -->
<link type="text/css" rel="stylesheet" href="<?= asset('css/magnific-popup.css')?>" />

<!-- Font Awesome Icon -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css">

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700" rel="stylesheet">

<!-- Style -->
<link rel="stylesheet" href="<?= asset('css/style.css') ?>">

<!-- Owl Carousel -->
<link rel="stylesheet" href="<?= asset('vendor/owl/dist/assets/owl.carousel.min.css') ?>">
<link rel="stylesheet" href="<?= asset('vendor/owl/dist/assets/owl.theme.default.min.css') ?>">

<!-- drawer.css -->
<link rel="stylesheet" href="<?= asset('vendor/drawer/dist/css/drawer.css') ?>">

<!-- Croppie -->
<link rel="stylesheet" href="<?= asset('vendor/croppie/croppie.css') ?>" />

<!-- URL Theme Color untuk Chrome, Firefox OS, Opera dan Vivaldi -->
<meta name="theme-color" content="#08387f" />
<!-- URL Theme Color untuk Windows Phone -->
<meta name="msapplication-navbutton-color" content="#08387f" />
<!-- URL Theme Color untuk iOS Safari -->
<meta name="apple-mobile-web-app-capable" content="yes" />
<meta name="apple-mobile-web-app-status-bar-style" content="#08387f" />