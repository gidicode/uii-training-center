<header id="header">

    <!-- Top Header -->
    <div id="top-navbar" class="navbar content-desktop">
        <div class="container">
            <div class="d-flex w-100">
                <ul class="list-inline mr-auto social-icon">
                    <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fab fa-facebook"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                    </li>
                    <li class="list-inline-item">
                        <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fas fa-envelope"></i></a>
                    </li>
                </ul>
                <ul class="ml-auto list-inline social-icon">
                    <?php if (session('logged_in')): ?>
                        <li class="list-inline-item dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="height: 20px; text-decoration: none !important;">
                                <img src="<?= auth()->avatar ?? '' ?>" class="rounded-circle d-inline mr-1" alt="" width="18" height="18">
                                <?= auth()->name  ?? '' ?>
                            </a>
                            <div class="dropdown-menu dropdown-menu-right pt-0">
                                <div class="py-2" style="background: #f8f9fa; padding: 0.25rem 1.5rem; width: auto; white-space: nowrap; border-radius: 0.25rem 0.25rem 0rem 0rem;">
                                    <img src="<?= auth()->avatar ?? '' ?>" class="rounded-circle d-inline" alt="" width="30" height="30">
                                    <span class="ml-auto d-inline"><?= auth()->name ?? '' ?></span>
                                </div>
                                <a href="<?= base_url('profile') ?>" class="dropdown-item">
                                    <i class="fas fa-sm fa-user-edit mr-1"></i> Profil
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="<?= base_url('auth/logout') ?>" class="dropdown-item" onclick="
                                    event.preventDefault(); document.getElementById('logout-form').submit();">
                                    <i class="fas fa-sm fa-door-open mr-1"></i> Keluar
                                </a>
                                <form id="logout-form" action="<?= base_url('auth/logout') ?>" class="d-none" method="POST"></form>
                            </div>
                        </li>
                    <?php else: ?>
                        <li class="list-inline-item dropdown">
                            <a href="" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Masuk atau Daftar 
                            </a>
                            <div class="dropdown-menu dropdown-menu-right" >
                                
                                <a class="dropdown-item" href="<?= base_url('sso/google'); ?>">
                                    <i class="fab fa-google mr-3"></i>Lanjutkan dengan Google
                                </a>

                                <a class="dropdown-item" href="#">
                                    <i class="fab fa-facebook-f mr-3"></i>Lanjutkan dengan Facebook
                                </a>

                                <!--
                                <div class="dropdown-divider"></div>
                                <h1 class="h6 dropdown-header">Sebagai organisasi</h1>
                                <div class="text-center d-flex" style="padding: 0.25rem 1.5rem;">
                                    <a href="<?= base_url('auth/login') ?>" role="button" class="btn btn-sm btn-primary mr-auto">Masuk</a>
                                    <small class="mx-2">atau</small>
                                    <a href="<?= base_url('auth/register') ?>" role="button" class="btn ml-auto btn-sm btn-secondary">Registrasi</a>
                                </div> -->
                            </div>
                        </li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
    </div>

    <!-- Main navbar -->
    <nav class="navbar navbar-expand-md navbar-light bg-light" id="main-navbar">
        <div class="container">

            <div class="content-mobile">
                <button type="button" class="drawer-toggle drawer-hamburger">
                    <span class="sr-only">toggle navigation</span>
                    <span class="drawer-hamburger-icon"></span>
                </button>
            </div>

            <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= asset('img/logo-uii.png')?>" style="width:140px" alt=""></a>
            
            <div class="float-right">
                <div class="collapse navbar-collapse" id="navbarNavDropdown">
                    <ul class="navbar-nav">

                        <button class="navbar-toggler float-right" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        
                        <li class="nav-item active">
                            <a class="nav-link" href="<?= base_url() ?>">Beranda</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="#">Jadwal Pelatihan & Sertifikasi</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="<?= base_url('konfirmasi') ?>">Konfirmasi</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link" href="#">Program</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link" href="#">Berita Terbaru</a>
                        </li>
                        <!-- 
                        <li class="nav-item dropdown">
                            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">dropdown</a>
                            <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                                <a class="dropdown-item" href="#">dropdown 1</a>
                                <a class="dropdown-item" href="#">dropdown 2</a>
                            </div>
                        </li> -->
                        
                    </ul>
                </div>
            </div>

        </div>

        
    </nav>

    <nav class="drawer-nav km-side-menu" role="navigation">
        <ul class="drawe-menu">
            <a class="drawer-menu-item" href="#"><img src="assets/img/logo-uii.png" style="width:140px" alt=""></a>
        </ul>
        <ul class="drawer-menu">
            <li><a class="drawer-menu-item " href="<?= base_url() ?>">Beranda</a></li>
        </ul>
        <ul class="drawer-menu">
            <li><a class="drawer-menu-item " href="#">Jadwal Pelatihan dan Sertifikasi</a></li>
        </ul>
        <ul class="drawer-menu">
            <li><a class="drawer-menu-item " href="<?= base_url('konfirmasi') ?>">Konfirmasi</a></li>
        </ul>
        <ul class="drawer-menu">
            <li><a class="drawer-menu-item " href="#">Program</a></li>
        </ul>
        <ul class="drawer-menu">
            <li><a class="drawer-menu-item" href="#">Berita Terbaru</a></li>
        </ul>
    </nav>
    
</header>
<?php $this->load->view('parts/alert'); ?>
