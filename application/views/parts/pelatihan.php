<div class="col-lg-4 col-md-4 col-sm-6 mb-3">
    <div class="card border-0">
        <img class="card-img-top" src="<?= $t->filename ?>" alt="Card image cap">
        <div class="card-body">
            <a class="link" href="<?= base_url('pelatihan/'.$t->training_id) ?>">
            <h5 class="card-title">
                <?= $t->training_name ?>
                <br><a class="link" href="<?= base_url('lembaga/'.$t->organization_id) ?>"><small style="font-size:10pt"><?= $t->org_name ?></small></a>
            </h5></a>
            <span class="label-price">
                <small>Start from</small>
                <p><?= 'Rp. '.number_format($t->price) ?></p>
            </span>
            <p class="card-text" style="font-size:11pt;">
            <?= carbon($t->start_time)->isoFormat('DD MMMM YYYY') . ($t->start_time == $t->finish_time ? '' : ' - '. carbon($t->finish_time)->isoFormat('DD MMMM YYYY')) ?><br>
                <?= $t->location ?>
            </p>
        </div>
    </div>
</div>