<footer id="footer">

    <!-- Top footer -->
    <div id="top-footer" class="section sm-section">

        <!-- container -->
        <div class="container">

            <!-- row -->
            <div class="row">

                <div class="col mt-3">
                    <h5>Pelatihan</h5>
                    <a href="">Pelatihan satu</a><br>
                    <a href="">Pelatihan dua</a><br>
                    <a href="">Pelatihan tiga</a><br>
                    <a href="">Pelatihan empat</a><br>
                    <a href="">Pelatihan lima</a><br>
                </div>

                <div class="col mt-3">
                    <h5>Informasi</h5>
                    <a href="">Tentang Simpul Tumbuh UII</a><br>
                    <a href="">Kontak Simpul Tumbuh UII</a><br>
                </div>

                <div class="col mt-3">
                    <h5>Peserta</h5>
                    <a href="">Daftar</a><br>
                </div>

                <div class="col mt-3">
                    <h5>Hubungi Kami</h5>
                    <p>
                        Gedung GBPH Prabuningrat (Rektorat) Lantai 3<br>
                        Kampus Terpadu Universitas Islam Indonesia<br>
                        Jalan Kaliurang km. 14,5 Sleman<br>
                        Yogyakarta 55584<br>
                        <br>
                        Telepon: +62 274 898444<br>
                        Faks: +62 274 898459<br>
                        Email: simpul-tumbuh@uii.ac.id<br>
                    </p>
                </div>

            </div>
            <!-- /row -->

        </div>
        <!-- /container -->

    </div>
    <!-- /Top footer -->

    <!-- Bottom footer -->
    <div id="bottom-footer">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <span>&copy; 2019 Simpul Tumbuh UII, Yogyakarta</span>
                </div>
                <div class="col-md-6 content-desktop">
                    <div class="social-icon">
                        <ul class="list-inline">
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fab fa-facebook"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fab fa-twitter"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fab fa-instagram"></i></a>
                            </li>
                            <li class="list-inline-item">
                                <a class="social-icon text-xs-center" target="_blank" href="#"><i class="fas fa-envelope"></i></a>
                            </li>
                        </ul>
                        
                    </div>
                </div>
                
            </div>
        </div>
    </div>
    <!-- /Bottom footer -->

</footer>