<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->

        <!-- Carousel Image -->
        <div class="content-mobile" style="margin-top:68px;"></div>
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <img class="d-block w-100" src="<?= base_url('public/img/uii-home.jpg') ?>" alt="First slide">
                    <div class="carousel-caption text-left content-desktop">
                        <small>Selamat Datang di Portal Pusat Sertifikasi dan Pelatihan</small>
                        <h2>UII Training Center</h2>
                    </div>
                </div>
                <!--
                <div class="carousel-item">
                    <img class="d-block w-100" src="https://training.uii.ac.id/wp-content/uploads/2017/10/DES-Training-and-Consultancy541134790-1500x500-1500x500.jpg" alt="Second slide">
                    <div class="carousel-caption text-left content-desktop">
                        <small>9 - 10 Januari 2019</small>
                        <h5>OCCUPATIONAL HEALTH AND SAFETY TRAINING</h5>
                    </div>
                </div> -->
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>

        <div class="slider-intro text-center">
            <h1 class="h5">PUSAT PELATIHAN DAN SERTIFIKASI | UNIVERSITAS ISLAM INDONESIA</h1>
        </div>

        <!-- section 2  -->
        <div class="intro-2">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <h5>PROFESIONAL DAN AKADEMISI</h5>
                        <p>Perdalam pengetahuan dan kemampuan di bidang profesi dan keahlian yang anda tekuni.</p>
                        <a href="#" class="btn btn-sm btn-secondary">Pilih Pelatihan</a>
                    </div>
                    <div class="col-md-4">
                        <h5>PELAKU BISNIS DAN UKM</h5>
                        <p>Kelola bisnis dan usaha anda menjadi lebih baik agar dapat berkembang lebih pesat.</p>
                        <a href="#" class="btn btn-sm btn-secondary">Pilih Pelatihan</a>
                    </div>
                    <div class="col-md-4">
                        <h5>KETERAMPILAN DAN KEJURUAN</h5>
                        <p>Asah keterampilan anda dalam mengoperasikan mesin dan peralatan di tempat kerja.</p>
                        <a href="#" class="btn btn-sm btn-secondary">Pilih Pelatihan</a>
                    </div>
                </div>
            </div>
        </div>

        <!-- Section 4 Pelatihan -->
		<div class="container py-5" >
            <h1 class="h3 text-center text-primary" style="font-family: Arial"><b>Pelatihan Saat Ini</b></h1>
            <p class="text-center">
                Tetap terhubung dengan pelatihan terbaru untuk mengembangkan potensi anda.
            </p>
            <div class="row justify-content-center mt-3">

                <!-- Pelatihan -->
                <?php if($is_training_active == true){ ?>
                <?php foreach($trainings->result() as $t): 
                    $data['t'] = $t;
                    $this->load->view('parts/pelatihan',$data);
                endforeach;
                } else { ?>
                    <i class="text-center">Tidak ada pelatihan/sertifikasi yang aktif saat ini</i>
                    
                <?php } ?>

            </div>
            <div class="d-flex">
                <a href="<?= base_url('pelatihan'); ?>" class="btn btn-primary mx-auto">Lihat lebih banyak</a>
            </div>
        </div>

         <!-- Section 4 Sertifikasi -->
		<div class="container py-5" >
            <h1 class="h3 text-center text-primary" style="font-family: Arial"><b>Sertifikasi Saat Ini</b></h1>
            <p class="text-center">
                Tetap terhubung dengan sertifikasi terbaru untuk mengembangkan potensi anda.
            </p>
            <div class="row justify-content-center mt-3">

                <!-- sertifikasi -->
                <?php if($is_certification_active == true){ ?>
                <?php foreach($certifications->result() as $c): 
                    $data['c'] = $c;
                    $this->load->view('parts/sertifikasi',$data);
                endforeach;
                } else { ?>
                    <i class="text-center">Tidak ada pelatihan/sertifikasi yang aktif saat ini</i>
                    
                <?php } ?>
            </div>
            <div class="d-flex">
                <a href="<?= base_url('sertifikasi'); ?>" class="btn btn-primary mx-auto">Lihat lebih banyak</a>
            </div>
        </div>

        <!-- section 3  -->
        <div class="intro-3">
            <div class="row no-gutters">
                <div class="col-md-6 intro-img">
                    <img src="https://training.uii.ac.id/wp-content/uploads/2018/03/WhatsApp-Image-2018-03-06-at-14.49.00-845x321.jpeg" alt=""
                        style="height: 100%;">
                </div>
                <div class="col-md-6 col-intro-3-text bg-secondary" style="margin-top:0px;">
                    <h3>PELATIHAN DAN SERTIFIKASI RESMI</h3>
                    <p>Kami bekerja sama dengan berbagai lembaga/institusi pelatihan dan sertifikasi di berbagai bidang, baik tingkat nasional maupun internasional.</p>
                    <a href="#" class="btn btn-biru-uii">Lihat jadwal pelatihan dan sertifikasi</a>
                </div>
            </div>
        </div>

        <!-- Section 8 Kontak -->
        <div class="kontak">
            <div class="container">
                <div class="row">
                    
                    <div class="col-md-6">
                        <h4 class="mt-2">HUBUNGI KAMI</h4>
                        <div class="mt-3"></div>
                        <h6>SIMPUL TUMBUH UNIVERSITAS ISLAM INDONESIA</h6>
                        <p>
                            Gedung GBPH Prabuningrat (Rektorat) Lantai 3<br>
                            Kampus Terpadu Universitas Islam Indonesia<br>
                            Jalan Kaliurang km. 14,5 Sleman<br>
                            Yogyakarta 55584<br>
                            <br>
                            Telepon: +62 274 898444<br>
                            Faks: +62 274 898459<br>
                            Email: simpul-tumbuh@uii.ac.id<br>
                        </p>
                    </div>

                    <div class="col-md-6 kontak-maps">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3953.9621665707837!2d110.41573111472525!3d-7.687209294458402!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e7a5e990e4088f5%3A0xce0d8cee48ddf4c9!2sBook+Store+UII!5e0!3m2!1sid!2sid!4v1547012623595" style="width:100%; height:350px" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>

                </div>
            </div>
        </div>

        <!-- Section 9 Footer -->
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
                    $(this).remove(); 
                });
            }, 5000);
        });
        </script>
    </body>
</html>