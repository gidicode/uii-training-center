<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->
        
        <!-- Alert -->
        <?php $this->load->view('parts/alert'); ?>

        <!-- Main page -->
        <?php foreach($trainings as $t): ?>
        <div class="content-mobile" style="margin-top:100px"></div>
        <div class="container mt-5 mb-5">
            <div class="container-card-pelatihan">
                <div class="card">
                    <div class="header-page-pelatihan row">
                        <div class="col-md-8">
                            <div class="image" style="background-image:url('<?= $t->filename ?>')"></div>
                        </div>
                        <div class="col-md-4 title-info">
                            <a class="link" href="pelatihan_page.html">
                            <h5 class="card-title">
                                <?= $t->training_name ?>
                                <br><a class="link" href=""><small style="font-size:10pt"><?= $t->org_name ?></small></a>
                            </h5></a>
                            <hr>
                            <p class="card-text" style="font-size:11pt;">
                            <?= carbon($t->start_time)->isoFormat('DD MMMM YYYY') . ($t->start_time == $t->finish_time ? '' : ' - '. carbon($t->finish_time)->isoFormat('DD MMMM YYYY')) ?><br>
                                <?= $t->location ?>
                            </p>
                            <h4 class="info-price"><?= 'Rp. '.number_format($t->price) ?></h4>
                            <div class="content-desktop">
                                <hr>
                                <div class="btn-daftar">
                                    <?php if($logged_in == true) { ?>
                                    <form action="#" method="post">
                                        <input type="hidden" name="daftar" value="logged_in">
                                        <input type="hidden" name="idPelatihan" value="<?= $this->uri->segment('2') ?>">
                                        <button type="submit" class="btn btn-biru-uii form-control text-white" style="background-color: #DB4437; border: none"> 
                                            DAFTAR SEKARANG
                                        </button>
                                    </form>
                                    <?php } else { ?>
                                    <a href="" class="btn btn-biru-uii form-control" data-toggle="modal" data-target="#formDaftar">DAFTAR SEKARANG</a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="content-mobile" style="margin-top: -20px"></div>
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-8">
                                <ul class="nav nav-tabs" id="myTab" role="tablist">
                                    <li class="nav-item">
                                        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Deskripsi</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Fasilitas</a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Materi</a>
                                    </li>
                                </ul>
                                <div class="tab-content mt-3" id="myTabContent"> 
                                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                        <?= $t->training_description ?>
                                    </div>
                                    <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                        <?= $t->fasilitas ?>
                                    </div>
                                    <div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
                                        <?= $t->materi ?>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-4">
                                <h5>Penyelenggara</h5>
                                <p><?= $t->org_name ?></p>
                                <hr>
                                <h5>Lokasi Pelatihan</h5>
                                <?= $t->location ?>
                            </div>
                        </div>

                    </div>
                </div>
            </div>

            <div class="mt-5"></div>
            <hr>
            <h4>Pelatihan yang serupa</h4>
            
            <div class="row mt-3">

            <?php foreach($trainings_all as $t):
            $data['t'] = $t;
            $this->load->view('parts/pelatihan',$data);
            endforeach; ?>

            </div>

        </div>
        
        <div class="content-mobile">
            <?php if($logged_in == true) { ?>
            <form action="#" method="post">
                <input type="hidden" name="daftar" value="logged_in">
                <input type="hidden" name="idPelatihan" value="<?= $this->uri->segment('2') ?>">
                <div class="btn-daftar">
                    <button type="submit" class="btn btn-biru-uii form-control">
                        DAFTAR SEKARANG
                        <span class="float-right">
                            <small>Mulai dari</small><br>
                            <?= 'Rp. '.number_format($t->price) ?>
                        </span>
                    </button>
                </div>
            </form>
            <?php } else { ?>
            <div class="btn-daftar">
                <a href="" data-toggle="modal" data-target="#formDaftar" class="btn btn-biru-uii form-control">
                    DAFTAR SEKARANG
                    <span class="float-right">
                        <small>Mulai dari</small><br>
                        <?= 'Rp. '.number_format($t->price) ?>
                    </span>
                </a>
            </div>
            <?php } ?>
        </div>
        <?php endforeach; ?>

        <!-- Modal Daftar -->
        <div class="modal fade" id="formDaftar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-sm" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">Daftar Sekarang</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    </div>
                    <div class="modal-body text-center mb-5">
                        <p>Silahkan mendaftar dengan akun sosial media anda</p>
                        <div class="mt-3"></div>
                        <!-- Login Google -->
                        <form action="#" method="post">
                            <input type="hidden" name="daftar" value="google">
                            <input type="hidden" name="idPelatihan" value="<?= $this->uri->segment('2') ?>">
                            <button type="submit" class="btn btn-secondary form-control text-white" style="background-color: #DB4437; border: none"> 
                                <i class="fab fa-google mr-3"></i>Google
                            </button>
                        </form>
                        
                        <hr>
                        <!-- Login Facebook -->
                        <button class="btn btn-secondary form-control text-white" style="background-color: #3C5A99; border: none"> 
                            <i class="fab fa-facebook-f mr-3"></i>Facebook
                        </button>
                        
                    </div>
                </div>
            </div>
        </div>
        
        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>
        
    </body>
</html>