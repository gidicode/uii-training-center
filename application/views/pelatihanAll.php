<!doctype html>
<html lang="en">
    <head>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body class="drawer drawer--left">

        <!-- Header -->
		<?php $this->load->view('parts/header'); ?>
		<!-- /Header -->
		
		<div class="container py-5" >
            <div class="row">
                <div class="col-md-3">
                    <div class="card">
                        <div class="card-header">
                            <h3>Filter</h3>
                        </div>
                        <div class="card-body">
                            
                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="row">
        
                        <!-- Pelatihan -->
                        <?php if($is_training_active == true){ ?>
                        <?php foreach($trainings->result() as $t): 
                            $data['t'] = $t;
                            $this->load->view('parts/pelatihan',$data);
                        endforeach;
                        } else { ?>
                            <i class="text-center">Tidak ada pelatihan/sertifikasi yang aktif saat ini</i>
                            
                        <?php } ?>
        
                    </div>
                </div>
            </div>
        </div>

        <!-- Footer Section -->
		<?php $this->load->view('parts/footer'); ?>
        <!-- end footer Section -->
        
        <?php $this->load->view('parts/script'); ?>

        <script type="text/javascript">
        $(document).ready(function () {
            window.setTimeout(function() {
                $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
                    $(this).remove(); 
                });
            }, 5000);
        });
        </script>
    </body>
</html>