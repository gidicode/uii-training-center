<!doctype html>
<html lang="en">
    <head>
        <style>
            *{
                transition: all 0.6s;
            }

            html {
                height: 100%;
            }

            body{
                font-family: 'Lato', sans-serif;
                color: #888;
                margin: 0;
            }

            #main{
                display: table;
                width: 100%;
                height: 100vh;
                text-align: center;
            }

            .fof{
                display: table-cell;
                vertical-align: middle;
            }

            .fof h3{
                font-size: 20px;
                display: inline-block;
                padding-right: 12px;
                animation: type .5s alternate infinite;
            }

            @keyframes type{
                from{box-shadow: inset -3px 0px 0px #888;}
                to{box-shadow: inset -3px 0px 0px transparent;}
            }
        </style>
        <?php $this->load->view('parts/head'); ?>
    </head>
    <body>


        <div id="main">
            <div class="fof">
                <h1 style="font-size: 50px">404 - Not Found</h1>
                <h3>Oops, halaman yang anda cari tidak ditemukan</h3><br><br>
                <?php if($admin == true){ ?>
                <a href="<?= dashboard_url() ?>" class="btn btn-primary">Back to Dashboard</a>
                <?php } else { ?>
                <a href="<?= base_url() ?>" class="btn btn-primary">Back to Homepage</a>
                <?php } ?>
            </div>
        </div>
        
    </body>
</html>