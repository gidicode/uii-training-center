<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Google {

    protected $CI;
    
    protected $client;

    protected $config;
	
	public function __construct()
	{
		$this->CI =& get_instance();
        $this->CI->load->library('session');
        $this->config = $this->CI->config->item('google');
        $this->client = new \Google_Client();
		$this->client->setClientId($this->config['client_id']);
		$this->client->setClientSecret($this->config['secret_key']);
		$this->client->setRedirectUri($this->config['redirect_url']);
        $this->client->setScopes($this->config['scopes']);
    }

    public function setRedirect($redirect)
    {
        $this->client->setRedirectUri($redirect);
    }

    public function getRedirect()
    {
        return $this->client->getRedirectUri();
    }
    
    public function get_login_url()
    {
		return  $this->client->createAuthUrl();
    }

    public function me()
    {
        if ($access_token = $this->CI->session->userdata('access_token')) {
            $this->client->setAccessToken($access_token);

            $profile = $this->client->verifyIdToken();

            return [
                'id' => $profile['sub'],
                'email' => $profile['email'],
                'name' => $profile['name'],
                'first_name' => $profile['given_name'],
                'last_name' => $profile['family_name'],
                'avatar' => $profile['picture'],
                'locale' => $profile['locale'],
            ];
        }
    }
    
    public function validate($code)
    {		
        $this->client->authenticate($code);
        $this->CI->session->set_userdata([
            'access_token' => $this->client->getAccessToken()            
        ]);

        if ($access_token = $this->CI->session->userdata('access_token')) {
            $this->client->setAccessToken($access_token);
            $profile = $this->client->verifyIdToken();
            
            return [
                'id' => $profile['sub'],
                'email' => $profile['email'],
                'name' => $profile['name'],
                'first_name' => $profile['given_name'],
                'last_name' => $profile['family_name'],
                'avatar' => $profile['picture'],
                'locale' => $profile['locale'],
            ];
		}
	}
}