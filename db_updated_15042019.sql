-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 15, 2019 at 04:17 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `trainingcenter`
--

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `email` varchar(255) NOT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `level` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `logo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `name`, `description`, `email`, `google_id`, `level`, `deskripsi`, `status`, `logo`, `created_at`) VALUES
(8, 'Gidi Training', NULL, '14523300@students.uii.ac.id', '101719349283792166674', 'Prodi', 'contoh deskripsi', 1, 'https://lh5.googleusercontent.com/-3IC_Q27PfxU/AAAAAAAAAAI/AAAAAAAAAAs/nQojhqeRO1k/s96-c/photo.jpg', '2019-02-25 05:20:13'),
(9, 'Training Center UII', NULL, 'work.wildan@gmail.com', '109112128695924974258', 'admin', 'Tes user', 1, 'https://lh6.googleusercontent.com/-jKn11o2hYO4/AAAAAAAAAAI/AAAAAAAAAAs/QWNyFGenqrE/s96-c/photo.jpg', '2019-03-25 23:45:38'),
(10, 'Gidicode Project', NULL, 'hi@gidicode.com', '118296070394187610489', 'Universitas', 'contoh user lembaga', 1, 'https://lh6.googleusercontent.com/-fpEZSL9f-EM/AAAAAAAAAAI/AAAAAAAAACM/cz8pMEeAXK8/s96-c/photo.jpg', '2019-04-13 14:55:28');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `training_id` int(11) UNSIGNED NOT NULL,
  `joined_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `user_id`, `training_id`, `joined_at`, `is_paid`) VALUES
(1, 7, 45, '2019-04-14 21:21:07', 1),
(2, 4, 45, '2019-04-15 07:38:08', 0),
(3, 7, 61, '2019-04-15 08:40:45', 1),
(4, 7, 64, '2019-04-15 09:15:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(6, 'uii'),
(7, 'db'),
(8, 'administrasi'),
(9, 'laravel'),
(10, 'web'),
(11, 'nodejs'),
(12, 'web developer');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(11) UNSIGNED NOT NULL,
  `organization_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `location` text,
  `price` int(11) UNSIGNED DEFAULT NULL,
  `fasilitas` text NOT NULL,
  `materi` text NOT NULL,
  `infoPembayaran` text NOT NULL,
  `quota` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `organization_id`, `name`, `description`, `location`, `price`, `fasilitas`, `materi`, `infoPembayaran`, `quota`, `start_time`, `finish_time`, `closed_at`, `status`, `created_at`, `updated_at`) VALUES
(36, 9, 'Sertifikasi Oracle Database', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '', 3250000, '', '', '', 25, '2019-03-29 00:00:00', '2019-03-30 00:00:00', '2019-03-28 00:00:00', 3, '2019-03-26 00:18:34', '2019-03-26 00:18:34'),
(37, 9, 'Pelatihan Jurnalistik ', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '', 5000000, '', '', '', 150, '2019-03-28 00:00:00', '2019-04-05 00:00:00', '2019-03-27 00:00:00', 3, '2019-03-26 00:19:26', '2019-03-26 00:19:26'),
(38, 9, 'Pelatihan Metode Dakwah', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum', '', 3400000, '', '', '', 120, '2019-03-30 00:00:00', '2019-04-06 00:00:00', '2019-03-29 00:00:00', 3, '2019-03-26 00:20:33', '2019-03-26 00:20:33'),
(45, 8, 'Laravel for Beginier', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed est tristique, dignissim risus sed, tempor mauris. Aliquam sed massa eget sem posuere luctus ut ut neque. Integer scelerisque ligula nibh,<strong>&nbsp;eu tincidunt neque venenatis quis. Cras eget libero velit. Mauris quam turpis</strong>, volutpat vel laoreet ac, dapibus vitae turpis. Sed facilisis elit non ligula porttitor, ac dignissim risus efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras tempus fringilla neque, sed fermentum magna congue ut. Sed vehicula risus nec ornare aliquet.</p>', 'FTI UII', 1250000, '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed est tristique, dignissim risus sed, tempor mauris. Aliquam sed massa eget sem posuere luctus ut ut neque. Integer scelerisque ligula nibh,<strong>&nbsp;eu tincidunt neque venenatis quis. Cras eget libero velit. Mauris quam turpis</strong>, volutpat vel laoreet ac, dapibus vitae turpis. Sed facilisis elit non ligula porttitor, ac dignissim risus efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras tempus fringilla neque, sed fermentum magna congue ut. Sed vehicula risus nec ornare aliquet.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed est tristique, dignissim risus sed, tempor mauris. Aliquam sed massa eget sem posuere luctus ut ut neque. Integer scelerisque ligula nibh,<strong>&nbsp;eu tincidunt neque venenatis quis. Cras eget libero velit. Mauris quam turpis</strong>, volutpat vel laoreet ac, dapibus vitae turpis. Sed facilisis elit non ligula porttitor, ac dignissim risus efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras tempus fringilla neque, sed fermentum magna congue ut. Sed vehicula risus nec ornare aliquet.</p>', '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam sed est tristique, dignissim risus sed, tempor mauris. Aliquam sed massa eget sem posuere luctus ut ut neque. Integer scelerisque ligula nibh,<strong>&nbsp;eu tincidunt neque venenatis quis. Cras eget libero velit. Mauris quam turpis</strong>, volutpat vel laoreet ac, dapibus vitae turpis. Sed facilisis elit non ligula porttitor, ac dignissim risus efficitur. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Cras tempus fringilla neque, sed fermentum magna congue ut. Sed vehicula risus nec ornare aliquet.</p>', 12, '2019-04-25 00:00:00', '2019-04-24 00:00:00', '2019-04-17 00:00:00', 3, '2019-04-14 06:10:09', '2019-04-14 06:10:09'),
(61, 10, 'AngularJS for beginer', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'FTI UII', 1250000, '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 11, '2019-04-18 00:00:00', '2019-04-24 00:00:00', '2019-04-25 00:00:00', 3, '2019-04-14 07:12:34', '2019-04-14 07:12:34'),
(64, 10, 'NodeJS for beginer', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 'FTI UII', 1250000, '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', '<p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum. </p>', 12, '2019-04-18 00:00:00', '2019-04-30 00:00:00', '2019-04-25 00:00:00', 3, '2019-04-14 07:20:26', '2019-04-14 07:20:26');

-- --------------------------------------------------------

--
-- Table structure for table `training_images`
--

CREATE TABLE `training_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_images`
--

INSERT INTO `training_images` (`id`, `training_id`, `filename`) VALUES
(3, 36, 'http://localhost/uii-training-center/public/images/pelatihan/36/42459016_1794566797318127_2942976464203863732_n.jpg'),
(4, 37, 'http://localhost/uii-training-center/public/images/pelatihan/37/42459016_1794566797318127_2942976464203863732_n.jpg'),
(5, 37, 'http://localhost/uii-training-center/public/images/pelatihan/37/31890415_2048625928731085_7538423827961741312_n.jpg'),
(6, 38, 'http://localhost/uii-training-center/public/images/pelatihan/38/41949782_478405899335885_60959342170049569_n.jpg'),
(7, 38, 'http://localhost/uii-training-center/public/images/pelatihan/38/30084089_158072634872109_4821656554233135104_n.jpg'),
(10, 45, 'http://localhost/uii-training-center/public/images/pelatihan/45/41949782_478405899335885_60959342170049569_n.jpg'),
(11, 61, 'http://localhost/uii-training-center/public/images/pelatihan/61/585e9ddbcb11b227491c34fc.png'),
(15, 64, 'http://localhost/uii-training-center/public/images/pelatihan/64/1_YVkK570-GSHyqtiEiIi6Xw.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `training_tags`
--

CREATE TABLE `training_tags` (
  `training_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_tags`
--

INSERT INTO `training_tags` (`training_id`, `tag_id`) VALUES
(45, 9),
(61, 9),
(64, 12);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` text,
  `institusi` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `google_id`, `name`, `avatar`, `phone`, `address`, `institusi`, `pekerjaan`, `created_at`) VALUES
(2, 'hi@gidicode.com', '118296070394187610489', 'Gidicode Project', 'https://lh6.googleusercontent.com/-fpEZSL9f-EM/AAAAAAAAAAI/AAAAAAAAACM/cz8pMEeAXK8/s96-c/photo.jpg', '081312260287', NULL, 'Gidicode Group', NULL, '2019-02-22 15:39:18'),
(4, 'wildaanmaulana@gmail.com', '108711921299526121277', 'Wildan Maulana', 'https://lh6.googleusercontent.com/-q1PhA2MqUl0/AAAAAAAAAAI/AAAAAAAALKo/gkcVFXOt4zg/s96-c/photo.jpg', '081312260586', NULL, 'Delokal', NULL, '2019-04-05 06:54:49'),
(7, '14523300@students.uii.ac.id', '101719349283792166674', 'Maulana Malik', 'https://lh5.googleusercontent.com/-3IC_Q27PfxU/AAAAAAAAAAI/AAAAAAAAJ-s/Dpea4YyNgkk/s96-c/photo.jpg', '081312260687', 'Ngipikrejo 1 RT 024/ RW 012, Banjararum, Kalibawang, Kulonprogo, DI Yogyakarta', 'Universitas Islam Indonesia', 'CEO', '2019-04-05 15:50:29'),
(8, 'work.wildan@gmail.com', '109112128695924974258', 'Ahmad Syukron', 'https://lh6.googleusercontent.com/-jKn11o2hYO4/AAAAAAAAAAI/AAAAAAAAAAs/QWNyFGenqrE/s96-c/photo.jpg', '081312260738', 'tes alamat', 'Gidicode', 'CEO', '2019-04-06 02:51:55');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `organizations_email_uindex` (`email`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_id` (`training_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Indexes for table `training_images`
--
ALTER TABLE `training_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_id` (`training_id`);

--
-- Indexes for table `training_tags`
--
ALTER TABLE `training_tags`
  ADD PRIMARY KEY (`training_id`,`tag_id`),
  ADD KEY `training_tags_ibfk_1` (`tag_id`),
  ADD KEY `training_tags_ibfk_2` (`training_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_uindex` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=65;

--
-- AUTO_INCREMENT for table `training_images`
--
ALTER TABLE `training_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON UPDATE NO ACTION,
  ADD CONSTRAINT `participants_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`) ON UPDATE NO ACTION;

--
-- Constraints for table `training_images`
--
ALTER TABLE `training_images`
  ADD CONSTRAINT `training_images_ibfk_1` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_tags`
--
ALTER TABLE `training_tags`
  ADD CONSTRAINT `training_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `training_tags_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
