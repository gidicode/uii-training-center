-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Aug 27, 2019 at 01:37 PM
-- Server version: 8.0.17
-- PHP Version: 7.3.8-1+ubuntu19.04.1+deb.sury.org+1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `uiitrainingcenter`
--

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `version` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`version`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `organizations`
--

CREATE TABLE `organizations` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text,
  `email` varchar(255) NOT NULL,
  `phone` varchar(20) NOT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `level` varchar(100) NOT NULL,
  `deskripsi` text NOT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `logo` varchar(255) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `organizations`
--

INSERT INTO `organizations` (`id`, `name`, `description`, `email`, `phone`, `google_id`, `level`, `deskripsi`, `status`, `logo`) VALUES
(8, 'Gidi Training', NULL, '14523300@students.uii.ac.id', '0274123109', '101719349283792166674', 'Prodi', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 'https://lh5.googleusercontent.com/-3IC_Q27PfxU/AAAAAAAAAAI/AAAAAAAAAAs/nQojhqeRO1k/s96-c/photo.jpg'),
(9, 'Training Center UII', NULL, 'work.wildan@gmail.com', '080981252358', '109112128695924974258', 'admin', 'Tes user', 1, 'https://lh6.googleusercontent.com/-jKn11o2hYO4/AAAAAAAAAAI/AAAAAAAAAAs/QWNyFGenqrE/s96-c/photo.jpg'),
(10, 'Gidicode Project', NULL, 'hi@gidicode.com', '080981231231', '118296070394187610489', 'Universitas', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', 1, 'https://lh6.googleusercontent.com/-fpEZSL9f-EM/AAAAAAAAAAI/AAAAAAAAACM/cz8pMEeAXK8/s96-c/photo.jpg'),
(35, 'Hari Setiaji', NULL, '115230402@uii.ac.id', '', NULL, 'admin', '', 1, NULL),
(36, 'KONTRAKAN', NULL, 'fajri310899@gmail.com', '2332', '109546673807307621719', 'Prodi', 'Coba Membuat Lembaga', 1, 'https://lh3.googleusercontent.com/a-/AAuE7mChpRP0GET61ZotdMPdsfkcZqv0x0frpra14Hrx9Q=s96-c');

-- --------------------------------------------------------

--
-- Table structure for table `participants`
--

CREATE TABLE `participants` (
  `id` int(11) NOT NULL,
  `user_id` int(11) UNSIGNED NOT NULL,
  `training_id` int(11) UNSIGNED NOT NULL,
  `joined_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `is_paid` tinyint(1) NOT NULL DEFAULT '0',
  `invoice_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `participants`
--

INSERT INTO `participants` (`id`, `user_id`, `training_id`, `is_paid`, `invoice_id`) VALUES
(2, 12, 82, 1, 19083742),
(6, 13, 82, 0, 19081819);

-- --------------------------------------------------------

--
-- Table structure for table `payment`
--

CREATE TABLE `payment` (
  `id` int(11) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `payment_date` varchar(55) NOT NULL,
  `to_bank_name` varchar(55) NOT NULL,
  `from_name` varchar(55) NOT NULL,
  `cash_amount` int(12) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `payment`
--

INSERT INTO `payment` (`id`, `invoice_id`, `payment_date`, `to_bank_name`, `from_name`, `cash_amount`, `status`) VALUES
(1, 19083742, '2019-08-21', 'BNI', 'Fajri', 1250000, ''),
(2, 19085726, '2019-08-21', 'BNI', 'Fajri', 1200000, ''),
(3, 19084531, '2019-08-21', 'BNI', 'Fajri', 1200000, '');

-- --------------------------------------------------------

--
-- Table structure for table `skema_tuk`
--

CREATE TABLE `skema_tuk` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `skema` varchar(55) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `status` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `name`) VALUES
(15, 'website'),
(16, 'laravel');

-- --------------------------------------------------------

--
-- Table structure for table `trainings`
--

CREATE TABLE `trainings` (
  `id` int(11) UNSIGNED NOT NULL,
  `organization_id` int(11) UNSIGNED NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` text,
  `price` int(11) UNSIGNED DEFAULT NULL,
  `fasilitas` text,
  `materi` text NOT NULL,
  `infoPembayaran` text NOT NULL,
  `quota` int(11) DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `finish_time` datetime DEFAULT NULL,
  `closed_at` datetime DEFAULT NULL,
  `status` tinyint(3) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `clicked` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `trainings`
--

INSERT INTO `trainings` (`id`, `organization_id`, `name`, `description`, `location`, `price`, `fasilitas`, `materi`, `infoPembayaran`, `quota`, `start_time`, `finish_time`, `closed_at`, `status`, `clicked`) VALUES
(80, 10, 'Pelatihan Laravel untuk Pemula', '<p>Laravel merupakan framework PHP yang dirancang untuk membangun situs web dengan teknik pengembangan yang mudah dipahami karena mengikuti pola MVC (Model View Controller).</p><p>Pada training ini diharapkan mampu membantu meningkatkan kemampuan dan kinerja peserta dalam memperbaharui dan membangun website yang lebih mudah dan cepat, serta memiliki kinerja akses yang cepat.</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 'Fakultas Teknologi Industri Universitas Islam Indonesia', 1250000, '<p>Kelas ber-AC</p><p>Modul pelatihan</p><p>source code</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>1. Introducing to Laravel</p><p>1.1. Installing Laravel<br>1.2. Creating the TODOParrot Application<br>1.3. Configuring Laravel Application<br>1.4. Useful Development and Debugging Tools<br>1.5. Testing Laravel Application with PHPUnit</p><p>2. Managing Project Controllers, Layout, Views, and Other Assets</p><p>2.1. Creating First View<br>2.2. Creating First Controller<br>2.3. Managing Application Routes<br>2.4. Introducing the Blade Template Engine<br>2.5. Integrating Images, CSS and JavaScript<br>2.6. Introducing Elixir<br>2.7. Testing Views</p><p>3. Introducing Laravel Models</p><p>3.1. Configuring Project Database<br>3.2. Introducing the Eloquent ORM<br>3.3. Creating First Model<br>3.4. Introducing Migrations<br>3.5. Defining Accessors, Mutators, and Methods<br>3.6. Validating Models<br>3.7. Creating a RESTful Controller<br>3.8. Seeding the Database<br>3.9. Finding Data<br>3.10. Inserting New Records<br>3.11. Updating Existing Records<br>3.12. Deleting Records<br>3.13. Introducing Query Builder<br>3.14. Creating Sluggable URLs<br>3.15. Testing Models</p><p>4. Model Relations, Scopes, and Other Advanced Features</p><p>4.1. Introducing Relations<br>4.2. Introducing One-to-One Relations<br>4.3. Introducing the Belongs To Relation<br>4.4. Introducing One-to-Many Relations<br>4.5. Introducing Many-to-Many Relations<br>4.6. Introducing Has Many Through Relations<br>4.7. Introducing Polymorphic Relations<br>4.8. Eager Loadin<br>4.9. Introducing Scopes</p><p>5. Integrating Web Forms</p><p>5.1. Web Form Fundamentals<br>5.2. Creating a User Feedback For<br>5.3. Creating New TODO Lists<br>5.4. Updating a TODO List<br>5.5. Deleting TODO Lists<br>5.6. Associating Tasks with Categories<br>5.7. Uploading Files</p><p>6. Introducing Middleware</p><p>6.1. Introducing Laravel’s Default Middleware<br>6.2. Creating Middleware Solution<br>6.3. Using Middleware Parameters</p><p>7. Authenticating and Managing Users</p><p>7.1. Configuration Laravel Authentication<br>7.2. Registering Users<br>7.3. Retrieving the Authenticated User<br>7.4. Restricting Access to Authenticated Users<br>7.5. Restricting Forms to Authenticated Users<br>7.6. Creating Route Aliases<br>7.7. Adding Custom Fields to the Registration Form<br>7.8. Restricting an Entire Application to Authenticated Users</p><p>8. Deploying, Optimizing and Maintaining Application</p><p>8.1. Introducing the Laravel 5 Command Scheduler<br>8.2. Optimizing Application<br>8.3. Deploying Application<br>8.4. Placing Application in Maintenance Mode</p><p>9. Creating a Restricted Administration Console</p><p>9.1. Identifying Administrators<br>9.2. Creating the Administration Controllers<br>9.3. Restricting Access to the Administration Console</p><p>10. Introducing Lumen</p><p>10.1. Creating First Lumen Application<br>10.2. Creating a Status API</p><p>11. Introducing Events</p><p>11.1. Creating an Event<br>11.2. Broadcasting Events</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', '<p>Pembayaran melalui rekning Mandiri Syariah No.rek 09809381297 An. Gidicode Project</p><p data-f-id=\"pbf\" style=\"text-align: center; font-size: 14px; margin-top: 30px; opacity: 0.65; font-family: sans-serif;\">Powered by <a href=\"https://www.froala.com/wysiwyg-editor?pb=1\" title=\"Froala Editor\">Froala Editor</a></p>', 25, '2019-07-05 00:00:00', '2019-07-25 00:00:00', '2019-06-20 00:00:00', 3, 54),
(82, 36, 'Pelatihan Membuat Website', '<p>Pelatihan Membuat Website</p>', 'Lab Informatika UII', 1200000, '<p>Makanan</p>', '<p>Laravel</p>', '<p>Bayarr</p>', 100, '2019-08-17 00:00:00', '2019-08-20 00:00:00', '2019-08-16 00:00:00', 4, 21),
(88, 36, 'CI', '<p>Pelatihan CI</p>', 'Lab Informatika UII', 50000, '<p>Makan Minum</p>', '<p>CI</p>', '<p>tes</p>', 100, '2019-08-25 00:00:00', '2019-08-30 00:00:00', '2019-08-20 00:00:00', 5, 1),
(99, 36, 'tes', '', '', 0, '', '', '', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 2, 0);

-- --------------------------------------------------------

--
-- Table structure for table `training_images`
--

CREATE TABLE `training_images` (
  `id` int(10) UNSIGNED NOT NULL,
  `training_id` int(10) UNSIGNED NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_images`
--

INSERT INTO `training_images` (`id`, `training_id`, `filename`) VALUES
(1, 80, 'http://staging-utc.nkmd-uii.id/public/images/pelatihan/80/laravel.jpg'),
(2, 82, 'http://localhost/uii-training-center/public/images/pelatihan/82/1_YVkK570-GSHyqtiEiIi6Xw.jpeg'),
(4, 88, 'http://localhost/uii-training-center/public/images/pelatihan/88/1_YVkK570-GSHyqtiEiIi6Xw.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `training_tags`
--

CREATE TABLE `training_tags` (
  `training_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_tags`
--

INSERT INTO `training_tags` (`training_id`, `tag_id`) VALUES
(80, 15),
(82, 15),
(88, 15),
(80, 16),
(82, 16),
(88, 16),
(99, 16);

-- --------------------------------------------------------

--
-- Table structure for table `training_update`
--

CREATE TABLE `training_update` (
  `id` int(11) NOT NULL,
  `uniqID` varchar(155) NOT NULL,
  `training_id` int(11) NOT NULL,
  `title` varchar(155) NOT NULL,
  `note` text NOT NULL,
  `last_update` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `training_update`
--

INSERT INTO `training_update` (`id`, `uniqID`, `training_id`, `title`, `note`) VALUES
(1, '5372475c71f61972c639628baa808c4e183702ac5d87bb92a3020a09dfc4355c529a0bd4bb9501c1e57890c6a2a9afafcc592c7d462ed89bdf696f6f93903e64bkVziFM+gkUP5jPGP0J9QMnfhTD', 82, 'IYA', 'OKEE'),
(2, 'fe5c4ed82fcad65baf79195cdae76472c3ec370a4bc8de94b5f195ab24aa0bcefedb9c24bce8c4d73a2a18e72531ff05c8a19f346c55cd824b4f726759d782e9SK3pzaNCI/x4b+R2S6ICjf/wa1i', 82, 'Coba update', 'Coba');

-- --------------------------------------------------------

--
-- Table structure for table `tuk`
--

CREATE TABLE `tuk` (
  `id` int(11) NOT NULL,
  `google_id` varchar(155) NOT NULL,
  `email` varchar(111) NOT NULL,
  `nama_tuk` varchar(55) NOT NULL,
  `nama_laboratorium` varchar(155) NOT NULL,
  `nama_kepala_laboratorium` varchar(111) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `deskripsi` text NOT NULL,
  `phone` varchar(15) NOT NULL,
  `validasi` text NOT NULL,
  `status` varchar(8) NOT NULL,
  `logo` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk`
--

INSERT INTO `tuk` (`id`, `google_id`, `email`, `nama_tuk`, `nama_laboratorium`, `nama_kepala_laboratorium`, `alamat`, `deskripsi`, `phone`, `validasi`, `status`, `logo`) VALUES
(2, '103513587021182227139', 'wildan@gidicode.com', 'Wildan  Maulana', 'Tes 1', 'tes 2', 'tes 3', 'tes 4', '341', '', '1', 'https://lh5.googleusercontent.com/-ofLf5YDsA_I/AAAAAAAAAAI/AAAAAAAAAAc/OwdsAugLTRQ/s96-c/photo.jpg'),
(3, '109546673807307621719', 'fajri310899@gmail.com', 'Fajri Idza', 'Laboratorium UII', 'Fajri Idza Inayah', 'Jl UII', 'sasasafsa', '09876', '', '1', 'https://lh4.googleusercontent.com/-fSfa52L5qfg/AAAAAAAAAAI/AAAAAAAAAiE/nZ64-OGyL4U/s96-c/photo.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_participants`
--

CREATE TABLE `tuk_participants` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `nama` varchar(55) NOT NULL,
  `phone` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tuk_peralatan`
--

CREATE TABLE `tuk_peralatan` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `spesifikasi` varchar(155) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `kondisi` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk_peralatan`
--

INSERT INTO `tuk_peralatan` (`id`, `id_tuk`, `spesifikasi`, `jumlah`, `kondisi`) VALUES
(5, 2, 'spesifikasi 2.1', 1, 'oke bos'),
(6, 2, 'spesifikasi 2.2', 1, 'oke bos'),
(9, 3, 'Ruangan AC', 1, 'Mantab Lur');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_sertifikasi`
--

CREATE TABLE `tuk_sertifikasi` (
  `id` int(11) NOT NULL,
  `tuk_id` int(11) NOT NULL,
  `skema_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `description` text NOT NULL,
  `location` text NOT NULL,
  `price` int(11) NOT NULL,
  `infoPembayaran` text NOT NULL,
  `quota` int(11) NOT NULL,
  `start_time` datetime NOT NULL,
  `closed_at` datetime NOT NULL,
  `status` tinyint(3) NOT NULL,
  `created_at` timestamp NOT NULL,
  `updated_at` timestamp NOT NULL,
  `clicked` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tuk_sertifikasi`
--

INSERT INTO `tuk_sertifikasi` (`id`, `tuk_id`, `skema_id`, `name`, `description`, `location`, `price`, `infoPembayaran`, `quota`, `start_time`, `closed_at`, `status`, `created_at`, `updated_at`, `clicked`) VALUES
(19, 3, 2, 'Sertifikasi Pelatihan Laravel', '<p>Sertifikasi Web Berbasis Laravel</p>', 'Lab ', 500000, 'Transfer BNI a/n Genji', 100, '2019-08-30 00:00:00', '2019-08-25 00:00:00', 5, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 51),
(20, 3, 3, 'Sertifikasi Oracle', '<p>Sertifikasi Database Oracle</p>', 'Lab ', 2000000, '', 10, '2019-08-30 00:00:00', '2019-08-25 00:00:00', 3, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 5),
(51, 3, 2, 'Sertifikasi Pelatihan CodeIgniter', '<p>Pelatihan Web Framework CodeIgniter</p>', 'Lab ', 250000, '<p>Transfer Ke Bank BNI an Coba 34567543</p>', 100, '2019-08-31 00:00:00', '2019-08-25 00:00:00', 2, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tuk_sertifikasi_image`
--

CREATE TABLE `tuk_sertifikasi_image` (
  `id` int(10) NOT NULL,
  `sertifikasi_id` int(10) NOT NULL,
  `filename` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci ROW_FORMAT=COMPACT;

--
-- Dumping data for table `tuk_sertifikasi_image`
--

INSERT INTO `tuk_sertifikasi_image` (`id`, `sertifikasi_id`, `filename`) VALUES
(5, 19, 'http://localhost/uii-training-center/public/images/sertifikasi/19/1_YVkK570-GSHyqtiEiIi6Xw.jpeg'),
(8, 20, 'http://localhost/uii-training-center/public/images/sertifikasi/20/1_YVkK570-GSHyqtiEiIi6Xw1.jpeg'),
(9, 51, 'http://localhost/uii-training-center/public/images/sertifikasi/51/1_YVkK570-GSHyqtiEiIi6Xw8.jpeg');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_sertifikasi_nilai`
--

CREATE TABLE `tuk_sertifikasi_nilai` (
  `id_user` int(11) UNSIGNED NOT NULL,
  `id_sertifikasi` int(11) NOT NULL,
  `nilai` varchar(255) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tuk_sertifikasi_nilai`
--

INSERT INTO `tuk_sertifikasi_nilai` (`id_user`, `id_sertifikasi`, `nilai`, `keterangan`) VALUES
(12, 19, '100', 'Mantabb');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_sertifikasi_participants`
--

CREATE TABLE `tuk_sertifikasi_participants` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `sertifikasi_id` int(11) NOT NULL,
  `joined_at` datetime NOT NULL,
  `is_paid` tinyint(1) NOT NULL,
  `invoice_id` int(11) NOT NULL,
  `formulir` json NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Dumping data for table `tuk_sertifikasi_participants`
--

INSERT INTO `tuk_sertifikasi_participants` (`id`, `user_id`, `sertifikasi_id`, `joined_at`, `is_paid`, `invoice_id`, `formulir`) VALUES
(6, 12, 19, '0000-00-00 00:00:00', 1, 19084531, '{\"dataPribadi\": {\"Email\": \"dsf@dfs.com\", \"Alamat\": \"sad\", \"Telepon\": \"322\", \"Kode_Pos\": \"23\", \"Kebangsaan\": \"sad\", \"Nama_Lengkap\": \"sda\", \"Tempat_Lahir\": \"asd\", \"Jenis_Kelamin\": \"laki-laki\", \"Tanggal_Lahir\": \"2019-08-20\", \"Pendidikan_Terakhir\": \"dfs\"}, \"dataPekerjaan\": {\"Email\": \"sdf@gh.com\", \"Alamat\": \"dsf\", \"Jabatan\": \"df\", \"Telepon\": \"fds\", \"Kode_Pos\": \"23\", \"Nama_Instansi\": \"dfs\"}}'),
(7, 13, 19, '0000-00-00 00:00:00', 1, 19089729, '{\"dataPribadi\": {\"Email\": \"fajri310899@gmail.com\", \"Alamat\": \"Jakal\", \"Telepon\": \"4398439\", \"Kode_Pos\": \"34224\", \"Kebangsaan\": \"Indonesia\", \"Nama_Lengkap\": \"Fajri Idza Inayah\", \"Tempat_Lahir\": \"Bengkulu\", \"Jenis_Kelamin\": \"laki-laki\", \"Tanggal_Lahir\": \"2019-08-31\", \"Pendidikan_Terakhir\": \"SMK\"}, \"dataPekerjaan\": {\"Email\": \"adsa@gmail.com\", \"Alamat\": \"Jakal\", \"Jabatan\": \"Mahasiswa\", \"Telepon\": \"sddsfdsf\", \"Kode_Pos\": \"45343\", \"Nama_Instansi\": \"UII\"}}');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_skema`
--

CREATE TABLE `tuk_skema` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `skema` varchar(55) NOT NULL,
  `keterangan` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk_skema`
--

INSERT INTO `tuk_skema` (`id`, `id_tuk`, `skema`, `keterangan`) VALUES
(1, 2, 'Skema 1', 'lorem ipsum'),
(2, 3, 'Sertifikasi Website UII', 'Sertifikasi Website UII'),
(3, 3, 'Sertifikasi Database', 'Sertifikasi Database');

-- --------------------------------------------------------

--
-- Table structure for table `tuk_tempat`
--

CREATE TABLE `tuk_tempat` (
  `id` int(11) NOT NULL,
  `id_tuk` int(11) NOT NULL,
  `spesifikasi` varchar(155) NOT NULL,
  `jumlah` int(10) NOT NULL,
  `kondisi` varchar(55) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tuk_tempat`
--

INSERT INTO `tuk_tempat` (`id`, `id_tuk`, `spesifikasi`, `jumlah`, `kondisi`) VALUES
(5, 2, 'spesifikasi 1.2', 1, 'good aja'),
(6, 2, 'spek 1.1', 1, 'fa'),
(9, 3, 'Ruangan AC', 1, 'Mantab Lur');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) UNSIGNED NOT NULL,
  `email` varchar(255) NOT NULL,
  `google_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  `avatar` varchar(255) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  `address` text,
  `institusi` varchar(100) DEFAULT NULL,
  `pekerjaan` varchar(100) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `google_id`, `name`, `avatar`, `phone`, `address`, `institusi`, `pekerjaan`) VALUES
(10, '14523300@students.uii.ac.id', '101719349283792166674', 'WILDAN MAULANA -', 'https://lh5.googleusercontent.com/-3IC_Q27PfxU/AAAAAAAAAAI/AAAAAAAAJ-s/Dpea4YyNgkk/s96-c/photo.jpg', NULL, NULL, NULL, NULL),
(11, 'nizomsidiq@gmail.com', '106552100903921270486', 'Nizom Sidiq', 'https://lh4.googleusercontent.com/-EHFgR2GXh8Q/AAAAAAAAAAI/AAAAAAAABDs/_2RoPVNK2uI/s96-c/photo.jpg', '0895385351954', 'jalan jalan di jogja', 'Gidicode', 'Kuli'),
(12, '17523122@students.uii.ac.id', '114621989846157023780', 'FAJRI IDZA INAYAH', 'https://lh3.googleusercontent.com/-BHabi5Gn4s4/AAAAAAAAAAI/AAAAAAAAAAA/ACHi3rd0YudHX599va6fNH29m0lciT0-GQ/s96-c/photo.jpg', '213424', 'JAKAL', 'Universitas Islam Indonesia', 'Mahasiswa'),
(13, 'fajri310899@gmail.com', '109546673807307621719', 'Fajri', 'https://lh3.googleusercontent.com/a-/AAuE7mChpRP0GET61ZotdMPdsfkcZqv0x0frpra14Hrx9Q=s96-c', '43334', 'JAKAL', 'UII', 'Mahasiswa');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `organizations`
--
ALTER TABLE `organizations`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `organizations_email_uindex` (`email`);

--
-- Indexes for table `participants`
--
ALTER TABLE `participants`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `invoice_id` (`invoice_id`),
  ADD KEY `training_id` (`training_id`),
  ADD KEY `user_id` (`user_id`);

--
-- Indexes for table `payment`
--
ALTER TABLE `payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `skema_tuk`
--
ALTER TABLE `skema_tuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `trainings`
--
ALTER TABLE `trainings`
  ADD PRIMARY KEY (`id`),
  ADD KEY `organization_id` (`organization_id`);

--
-- Indexes for table `training_images`
--
ALTER TABLE `training_images`
  ADD PRIMARY KEY (`id`),
  ADD KEY `training_id` (`training_id`);

--
-- Indexes for table `training_tags`
--
ALTER TABLE `training_tags`
  ADD PRIMARY KEY (`training_id`,`tag_id`),
  ADD KEY `training_tags_ibfk_1` (`tag_id`),
  ADD KEY `training_tags_ibfk_2` (`training_id`);

--
-- Indexes for table `training_update`
--
ALTER TABLE `training_update`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk`
--
ALTER TABLE `tuk`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_participants`
--
ALTER TABLE `tuk_participants`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_peralatan`
--
ALTER TABLE `tuk_peralatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_sertifikasi`
--
ALTER TABLE `tuk_sertifikasi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tuk_id` (`tuk_id`),
  ADD KEY `skema_id` (`skema_id`);

--
-- Indexes for table `tuk_sertifikasi_image`
--
ALTER TABLE `tuk_sertifikasi_image`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sertifikasi_id` (`sertifikasi_id`);

--
-- Indexes for table `tuk_sertifikasi_nilai`
--
ALTER TABLE `tuk_sertifikasi_nilai`
  ADD PRIMARY KEY (`id_user`,`id_sertifikasi`),
  ADD KEY `user_id` (`id_user`),
  ADD KEY `sertifikasi_id` (`id_sertifikasi`);

--
-- Indexes for table `tuk_sertifikasi_participants`
--
ALTER TABLE `tuk_sertifikasi_participants`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`user_id`),
  ADD KEY `sertifikasi_id` (`sertifikasi_id`),
  ADD KEY `invoice_id` (`invoice_id`);

--
-- Indexes for table `tuk_skema`
--
ALTER TABLE `tuk_skema`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tuk_tempat`
--
ALTER TABLE `tuk_tempat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_uindex` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `organizations`
--
ALTER TABLE `organizations`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT for table `participants`
--
ALTER TABLE `participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `payment`
--
ALTER TABLE `payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `skema_tuk`
--
ALTER TABLE `skema_tuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `trainings`
--
ALTER TABLE `trainings`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `training_images`
--
ALTER TABLE `training_images`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `training_update`
--
ALTER TABLE `training_update`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tuk`
--
ALTER TABLE `tuk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tuk_participants`
--
ALTER TABLE `tuk_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tuk_peralatan`
--
ALTER TABLE `tuk_peralatan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tuk_sertifikasi`
--
ALTER TABLE `tuk_sertifikasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=52;
--
-- AUTO_INCREMENT for table `tuk_sertifikasi_image`
--
ALTER TABLE `tuk_sertifikasi_image`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tuk_sertifikasi_participants`
--
ALTER TABLE `tuk_sertifikasi_participants`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tuk_skema`
--
ALTER TABLE `tuk_skema`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `tuk_tempat`
--
ALTER TABLE `tuk_tempat`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `participants`
--
ALTER TABLE `participants`
  ADD CONSTRAINT `participants_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `participants_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `trainings`
--
ALTER TABLE `trainings`
  ADD CONSTRAINT `trainings_ibfk_1` FOREIGN KEY (`organization_id`) REFERENCES `organizations` (`id`);

--
-- Constraints for table `training_images`
--
ALTER TABLE `training_images`
  ADD CONSTRAINT `training_images_ibfk_1` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `training_tags`
--
ALTER TABLE `training_tags`
  ADD CONSTRAINT `training_tags_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `tuk_sertifikasi_nilai`
--
ALTER TABLE `tuk_sertifikasi_nilai`
  ADD CONSTRAINT `tuk_sertifikasi_nilai_ibfk_1` FOREIGN KEY (`id_sertifikasi`) REFERENCES `tuk_sertifikasi` (`id`),
  ADD CONSTRAINT `tuk_sertifikasi_nilai_ibfk_2` FOREIGN KEY (`id_user`) REFERENCES `users` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
