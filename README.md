# REQUIREMENT
1. php > 7.0
2. composer installed

# INSTALATION

1. clone this project `git clone https://gitlab.com/gidicode/uii-training-center.git` in your `htdocs` folder.
2. run `composer install` with your console.
3. copy `.env.example` to `.env`.
4. fill `.env` with your settings.