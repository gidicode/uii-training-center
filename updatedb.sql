-- 22 Februari 2019 18:25
ALTER TABLE `organizations` ADD `description` TEXT NULL AFTER `name`;

-- 24 Februari 2019 13:36
ALTER TABLE organizations MODIFY status tinyint(3) unsigned NOT NULL DEFAULT 0;

-- 1 Maret 2019 18:47
ALTER TABLE `trainings` ADD `location` TEXT NULL AFTER `description`;
ALTER TABLE `trainings` CHANGE `cost` `price` INT(11) UNSIGNED NULL DEFAULT NULL;

-- 2 Maret 2019 00:00
CREATE TABLE `tags` (
  `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

CREATE TABLE `training_tags` (
  `training_id` int(11) UNSIGNED NOT NULL,
  `tag_id` int(11) UNSIGNED NOT NULL,
    PRIMARY KEY (`training_id`, `tag_id`),
	KEY `training_tags_ibfk_1` (`tag_id`),
  	KEY `training_tags_ibfk_2` (`training_id`),
    CONSTRAINT `training_tags_ibfk_1` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
    CONSTRAINT `training_tags_ibfk_2` FOREIGN KEY (`training_id`) REFERENCES `trainings` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- 2 Maret 2019 22:10
ALTER TABLE `users` 
  ADD `address` TEXT NULL AFTER `phone`,
  ADD `institusi` VARCHAR(100) NULL AFTER `address`,
  ADD `pekerjaan` VARCHAR(100) NULL AFTER `institusi`;